<div class="page">
	<div class="padder">
		<div class="bat-content d-table" section="infobat">
            <div class="d-table-row h-100">
                <div class="sidebar d-table-cell">
                    <div class="bat-header">
                        <div class="title-header">MAQUETTE</div>
                    </div>
                    <div class="container-bat-info">
                        <div class="container-info">
                            <div class="info reference_container">
                                    <span class="label">Textile</span>
                                    <span class="product-title" sp-edit="product_name" sp-category="infobat" sp-label="Nom du produit"
                                         sp-type="text" sp-permission="0" sp-hiddable sp-order="0"></span>
                                </span>

                                <div class="info reference_container">
                                    <span class="label">Référence</span>
                                    <span class="value" sp-edit="reference" sp-category="infobat" sp-label="Référence du BAT"
                                          sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                                </div>
                                <div class="info marquage_container">
                                    <span class="label">Marquage</span>
                                    <div class="commande_container contenaire_emplacements">
                                        <div class="marquage" sp-type="emplacements" sp-repeat="1" noemp="" container>
                                            <div class="marquage-namebox box" sp-etype="user_img">
                                                 <span class="emplacement_marquage"
                                                       sp-edit="emplacement"></span>
                                            </div>
                                            <span sp-edit="des_nommarquage"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="info reference_container">
                                    <span class="label">Coloris</span>
                                    <span class="value" sp-style="model_product"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="product d-table-cell">
                    <div class="product-pictures contenaire_produit" layout="4-p">
                        <div class="picture" sp-type="produit" sp-repeat="4" sp-stackable="produit">
                            <div class="picture-item">
                                <img src="" class="imgpdt" sp-type="photo_produit">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="logo d-table-cell">
                    <img src="http://bat.sandbox.local/templates/quodao/images/logo-black.png" alt="">
                </div>
            </div>
		</div>
        <div class="footer">
            <div class="company-contacts">
                <div class="logo-footer">
                    <img src="http://bat.sandbox.local/templates/quodao/images/logo-black.png" alt="">
                </div>
                <div class="contact-item">
                    7 rue Edgar Brandt Bat B 72000 LE MANS
                </div>
                <div class="contact-item">
                    Tel. 02 43 61 18 93
                </div>
                <div class="contact-item">
                    contact@quodao.fr
                </div>
                <div class="contact-item">
                    www.quodao.fr
                </div>
            </div>
            <div class="product-notice">
                Le positionnement des logos sur le produit est purement indicatif et peut être modifié selon les contraintes techniques.
            </div>
        </div>
    </div>
</div>
