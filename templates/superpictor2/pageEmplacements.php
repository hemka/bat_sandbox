<div class="page">
    <div class="padder" id="pageEmplacements">
        <div class="bat-header row">
            <div spBranding no-branding class="company vertical-align user block-header f-r">
                <img src="" class="logo_header" sp-edit="user_logo" sp-category="inforevendeur" sp-img sp-hiddable
                     sp-label="Votre logo" sp-type="file" sp-permission="500" sp-order="4">
                <span sp-category="inforevendeur" sp-edit="company" class="uppercase">{nom_entreprise}</span>
            </div>
            <div spBranding high-branding class="company vertical-align block-header f-r">
                <img src="https://superpictor.com/designer/templateSP/templateNewsuper/images/logo_sp.png" class="logo_header">
            </div>
        </div>

        <div class="bat-content">
            <div class="text-header">
                <img src="http://bat.sandbox.local/templates/superpictor2/images/text-header.png"
                     class="logo_header">
            </div>
            <div class="marquages product" section="infologo">
                <div class="contenaire_emplacements">
                    <div class="marquage" sp-type="emplacements" sp-repeat="2" noemp="" container>
                        <div class="logo-image">
                            <div class="header-logo">
                                <div class="marquage-namebox box" sp-etype="user_img">
                                    <span class="emplacement_marquage" sp-edit="emplacement"></span>
                                    <span class="ref_container">
                                            (<span sp-category="infologo" sp-edit="ref" sp-contained sp-label="Référence" sp-hiddable
                                                   sp-type="text" sp-order="1"></span>)
                                            </span>
                                </div>
                                <div class="marquage-size">
                                    <span sp-edit="des_nommarquage"></span> -
                                    <span sp-category="infologo" sp-edit="hauteurtotale" sp-type="text"
                                          sp-label="Hauteur (en mm)" sp-order="3">0</span> mm x
                                    <span sp-category="infologo" sp-edit="largeurtotale" sp-type="text"
                                          sp-label="Largeur (en mm)" sp-order="4">0</span> mm
                                </div>
                            </div>
                            <div class="container-logo-img">
                                <div class="marquage-image" sp-style="background" style>
                                    <img src="{marquage_url}" sp-img sp-static sp-edit="miniature_file" sp-type="file"
                                         class="marquage_img" sp-category="infologo" sp-order="0">
                                </div>
                            </div>
                        </div>
                        <div class="box-logo-color">
                                <div class="marquage-details">
                                    <div class="marquage-comment" sp-category="infologo" sp-edit="comment"
                                         sp-label="Commentaire" sp-hiddable sp-type="textarea" sp-order="2"></div>
                                </div>
                                <div class="infos_container">
                                    <div class="fonts_container">
                                        <div sp-label="Polices" type_display="1" sp-static sp-edit="fonts" sp-type="fonts" sp-category="infologo" sp-contained sp-hiddable class="text_font_container"></div>
                                    </div>
                                    <div class="colors_container">
                                        <div sp-label="Couleurs" type_display="1" sp-static sp-edit="colors"
                                             sp-type="colors"
                                             sp-category="infologo" sp-contained sp-hiddable
                                             class="box_squares_color"></div>
                                    </div>
                                </div>

                            </div>

                    </div>
                </div>
                <div class="sidebar"  section="infobat">
                        <div class="bat-infos">
                            <div class="client" sp-edit="name" sp-category="client" sp-label="Nom du client" sp-hiddable
                                 sp-type="text" sp-permission="0" sp-order="1">
                                {CLIENT}
                            </div>

                            <div class="info-sidebar">
                                <div class="info reference_container">
                                    <span class="label">Réf. BAT</span>
                                    <span class="value" sp-edit="reference" sp-category="infobat"
                                          sp-label="Référence du BAT"
                                          sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                                </div>
                                <div class="info commande_container">
                                    <span class="label">Réf. commande</span>
                                    <span class="value" sp-edit="commande" sp-category="infobat"
                                          sp-label="Référence de la commande"
                                          sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                                </div>
                                <div class="info refproduit_container">
                                    <span class="label">Réf. produit</span>
                                    <span class="value" sp-edit="refproduit" sp-category="infobat"
                                          sp-label="Référence du produit"
                                          sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                                </div>
                                <div class="info commercial_container" section="contact">
                                    <span class="label">Affaire suivie par</span>
                                    <span class="value" sp-edit="commercial" sp-category="contact"
                                          sp-label="Affaire suivie par"
                                          sp-type="select" sp-permission="0" sp-order="0" sp-contained></span>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        <div class="footer">
            <div class="box"></div>
            <div class="text-note">
                Le positionnement des logos sur le produit est purement indicatif et peut être modifié selon les
                contraintes
                techniques.
            </div>
            <div class="pagination">{pactive}/{pagetotal}</div>
        </div>
    </div>
</div>
