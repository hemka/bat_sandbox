<div id="fiche_produit" class="page">
	<div class="padder">
        <div class="fiche_produit_header" section="infoclient">
            <img src="http://bat.sandbox.local/templates/batelier/images/logo.png"
                 class="logo_header">
            <span class="client_company" sp-edit="client_company" sp-category="infoclient"></span>
            <span class="client_name" sp-edit="client_name" sp-category="infoclient"></span>
        </div>
		<div class="fiche_produit_content" section="infoproduct">
            <div class="product">
                <div class="product_pictures contenaire_produit" layout="2-p">
                    <div class="picture" sp-type="produit" sp-repeat="6" sp-stackable="produit">
                        <img src="" class="imgpdt" sp-type="photo_produit">
						<div class="picture_label" sp-type="picture-label"></div>
					</div>
				</div>
                <div class="product_infos">
                    <span class="product_current_color" sp-edit="product_current_color" sp-category="infoproduct" sp-label="Couleur actuelle" sp-type="text" sp-hiddable></span>
                    <p class="product_desc" sp-edit="product_desc" sp-category="infoproduct" sp-label="Description" sp-type="textarea" sp-hiddable></p>
                    <div class="product_brand product_brand_container m-3">
                        <p class="info_title">Marque :</p>
                        <p class="info_text" sp-edit="product_brand" sp-category="infoproduct" sp-label="Marque" sp-type="text" sp-hiddable sp-contained></p>
                    </div>
                    <div class="product_size product_size_container m-3">
                        <p class="info_title">Taille(s) :</p>
                        <p class="info_text" sp-edit="product_size" sp-category="infoproduct" sp-type="text" sp-hiddable sp-contained sp-label="Taille(s)"></p>
                    </div>
                    <div class="product_color product_color_container m-3">
                        <p class="info_title">Couleur(s) :</p>
                        <p class="info_text" sp-edit="product_color" sp-category="infoproduct" sp-type="textarea" sp-hiddable sp-contained sp-label="Couleur(s)"></p>
                    </div>
                    <div class="product_category product_category_container m-3">
                        <p class="info_title">Information complémentaire :</p>
                        <p class="info_text" sp-edit="product_category" sp-category="infoproduct" sp-type="text" sp-hiddable sp-contained sp-label="Information complémentaire"></p>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
