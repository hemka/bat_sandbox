<div class="page">
    <div class="padder">
        <div class="bat-header">
            <div class="company vertical-align">
                <div spBranding no-branding class="company vertical-align user">
                    <img src="http://bat.sandbox.local/templates/batelier/images/logo.png"
                         class="logo_header">
                </div>
                <div spBranding high-branding class="company vertical-align">
                    <img src="http://bat.sandbox.local/templates/batelier/images/logo.png"
                         class="logo_header">
                </div>
            </div>
            <div class="uppercase vertical-align">bon-à-tirer</div>
        </div>
        <div class="bat-content" section="infobat">
            <div class="sidebar">
                <div class="bat-infos">
                    <div  section="contact">
                        <div class="client" sp-edit="client" sp-category="contact" sp-label="Nom du client" sp-hiddable
                             sp-type="text" sp-permission="0" sp-order="1">{CLIENT}
                        </div>
                    </div>
                    <div class="info reference_container">
                        <span class="label">Réf. BAT</span>
                        <span class="value" sp-edit="reference" sp-category="infobat" sp-label="Référence du BAT"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                    </div>
                    <div class="info commande_container">
                        <span class="label">Réf Dossier</span>
                        <span class="value" sp-edit="commande" sp-category="infobat" sp-label="Réf Dossier"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                    </div>
                    <div class="info commercial_container" section="contact">
                        <span class="label">ID</span>
                        <span class="value" sp-edit="commercial" sp-category="contact" sp-label="ID" sp-type="select"
                              sp-permission="0" sp-order="0" sp-contained></span>
                    </div>
                    <div class="info commercial_container" section="contact">
                        <span class="label">Date de création</span>
                        <span class="value" sp-edit="date" sp-category="infobat" sp-label="Date de création" sp-type="text"
                              sp-permission="0" sp-order="0" sp-contained></span>
                    </div>
                </div>
                <div class="validation">
                    <div class="comment" sp-edit="commentaire" sp-category="infobat" sp-label="Commentaire"
                         sp-type="textarea" sp-permission="0" sp-hiddable sp-order="1">
                        La validation du BAT engage <b>votre responsabilité.</b> Merci de vous assurer que le marquage est
                        conforme, en vérifiant le design, l’orthographe, les couleurs ainsi que la taille du logo.
                        <b>Nous ne pourrons accepter de réclamation après accord du bon à tirer. Pour rappel, le
                            délai de production de votre commande ne débute qu’après la validation du BAT</b>. Attention,
                            le BAT papier ou écran peut être légèrement différent de l’impression sur le produit.
                    </div>
                    <div class="accord">
                        <div class="accord-header">
                            <span class="accord-title uppercase">bon pour accord</span>
                            <span class="accord-extra">Merci de dater et de signer</span>
                        </div>
                    </div>
                </div>
                <div class="branding">
                    <div spBranding low-branding class="madeSp">
                        by Superpictor with
                    </div>
                    <img spBranding low-branding class="madeSpPicto"
                         src="http://dev.superpictor.com/designer/templateSP/templateNewsuper/images/picto_coeur.png">
                </div>

                <div class="pagination">{pactive} / {pagetotal}</div>
            </div>
            <div class="product">
                <div class="product-pictures contenaire_produit" layout="2-p">
                    <div class="picture" sp-type="produit" sp-repeat="6" sp-stackable="produit">
                        <img src="" class="imgpdt" sp-type="photo_produit">
                        <div class="picture-label" sp-type="picture-label"></div>
                    </div>
                </div>
                <div class="product-notice">Le positionnement des logos sur le produit est purement indicatif et peut
                    être modifié selon les contraintes techniques.
                </div>
            </div>
        </div>
        <!-- <h3 class="bat_name" sp-edit="nom" sp-category="infobat" sp-label="Nom du BAT" sp-type="text" sp-permission="0" sp-hiddable sp-order="0">{NomBAT}</h3>

        <img src="" class="imgpdt" sp-type="photo_produit">
        <img src="" class="imgpdt" sp-type="photo_produit">

        <p sp-edit="commentaire" sp-category="infobat" sp-label="Commentaire" sp-type="textarea" sp-permission="0" sp-hiddable sp-order="3">
        // commentaire de validation //
        </p>

        <div class="refvalue" sp-edit="reference" sp-category="infobat" sp-label="Référence du BAT" sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained>{refBat}</div>

        <div sp-category="inforevendeur" sp-edit="address_1">{adresse}</div>
        <span sp-category="inforevendeur" sp-edit="email" style="display: block;">{email}</span>

        <span sp-edit="Siteweb" sp-category="inforevendeur" sp-permission="450" sp-type="text" sp-order="4" sp-label="Site web">{Siteweb}</span>
        <span class="page">{pactive}/{pagetotal}</span>
        <span sp-category="inforevendeur" sp-edit="email">{email}</span> -->
    </div>
</div>
