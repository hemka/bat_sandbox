<div class="page">
    <div class="padder">
        <div class="header">
            <div class="header-text">
                <div class="text">
                     <div class=" value-info" sp-edit="email" sp-category="infobat"
                           sp-label="Email" sp-type="text" sp-permission="0" sp-hiddable sp-order="0">
                         contact@comandgifts.eu
                     </div>
                    <div class=" value-info" sp-edit="website" sp-category="infobat"
                         sp-label="Website" sp-type="text" sp-permission="0" sp-hiddable sp-order="0">
                        www.comandgifts.eu
                    </div>
                </div>
               <div class="line-header"></div>
            </div>
            <div class="header-logo"  class="company vertical-align">
                <img src="http://bat.sandbox.local/templates/comgift/images/logo.png" class="logo_header">
            </div>

        </div>
        <div class="bat-content" section="infobat">
            <div class="content">
                <div class="contenaire_emplacements" section="infologo">
                    <div sp-type="emplacements" class="emplacements" sp-repeat="4" noemp="" container>
                        <div class="logo-section">
                            <div class="container-logo">
                                <div class="marquage-image" sp-style="background" style>
                                    <img src="{marquage_url}" sp-img sp-static sp-edit="miniature_file" sp-type="file"
                                         class="marquage_img" sp-category="infologo" sp-order="0"/>
                                </div>
                            </div>

                        </div>

                        <div class="marquage-details">
                            <div class="container-marquage-details">
                                <div class="marquage-name marquage-item" sp-etype="user_img">
<!--                                    <span class="emplacement_marquage marquage-value" sp-edit="emplacement"></span>-->
                                    <span sp-category="infologo" sp-edit="ref" sp-contained sp-label="Référence" sp-hiddable
                                          sp-type="text" sp-order="1"></span>
                                </div>
                                <div class="marquage-size marquage-item">
                                    <span sp-edit="des_nommarquage"></span> -
                                <span class="marquage-value">
                                    <span sp-category="infologo" sp-edit="hauteurtotale" sp-type="text"
                                          sp-label="Hauteur (en mm)" sp-order="3">0</span>x<span sp-category="infologo" sp-edit="largeurtotale" sp-type="text" sp-label="Largeur (en mm)"
                                                                                                 sp-order="4">0</span>mm
                                </span>
                                </div>
                                <div class="marquage-color marquage-item" sp-label="Couleurs" type_display="1" sp-static sp-edit="colors"
                                     sp-type="colors" sp-category="infologo" sp-contained sp-hiddable class="box_squares_color"></div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="products">
                    <div class="product-pictures contenaire_produit" layout="2-p">
                        <div class="picture" sp-type="produit" sp-repeat="6" sp-stackable="produit">
                            <div class="picture-container" >
                                <img src="" class="imgpdt" sp-type="photo_produit">
                                <!--                    <div class="picture-label" sp-type="picture-label"></div>-->
                            </div>
                        </div>
                    </div>
                </div>

            </div>


        </div>
        <div class="footer">
            <div class="footer-block-item footer-block-1">
                <ul>
                    <li>
                        <b>Ce Bon A Tirer a pour objet la validation des textes et de la mise en page de votre maquette.</b> Elle a été réalisée à
                        partir des éléments en notre possession et/ou selon vos indications. Les couleurs d'impression ne pouvant être fidèles
                        à 100%, une marge d'erreur de 10% environ est à prendre en considération.
                    </li>
                    <li>
                        <b>Merci de vérifier attentivement toutes les données</b> (coordonnées, faute de frappe, orthographe...), <b>et de valider
                            cette maquette</b> en nous retournant ce document dûment complété pour Bon à Tirer.
                    </li>
                    <li>
                        <b>En cas de modifications,</b> inscrivez-les et retournez-nous ce document, afin d'obtenir un nouveau Bon A Tirer.
                    </li>
                </ul>
                <div>
                    <b>VOTRE VALIDATION EST ATTENDUE DANS LES MEILLEURS DÉLAIS. ELLE EST INDISPENSABLE POUR LANCER LA MISE
                        EN PRODUCTION DE VOTRE COMMANDE, APRÈS QUOI AUCUNE MODIFICATION ULTÉRIEURE NE SERA POSSIBLE.</b>
                </div>
            </div>
            <div class="footer-block-item footer-block-2">
                <div class="item">
                    <div class="box"></div>
                    <div class="text">
                        <div class="header-footer">VALIDATION DU BAT</div>
                    </div>

                </div>
                <div class="item">
                    <div class="box"></div>
                    <div class="text">
                        <div class="header-footer">VALIDATION DU BAT</div>
                        <div class="text-note">avec demande de modification simples</div>
                    </div>
                </div>
                <div class="item">
                    <div class="box"></div>
                    <div class="text">
                        <div class="header-footer">NOUVEAU BAT ATTENDUE</div>
                        <div class="text-note">suite aux modifications demandées</div>
                    </div>
                </div>
            </div>
            <div class="footer-block-item footer-block-3">
                <div class="footer-block-header">
                    Signature et tampon:
                </div>
                <div class="note-block">
                    (avec mention manuscrite «Pour BON A TIRER»)
                </div>
            </div>
        </div>

    </div>
</div>
