<div class="page">
	<div class="padder">
		<div class="border"></div>
		<div class="bat-header">
			<div class="bat-content" section="infobat">
				<img src="" class="logo_header" sp-edit="user_logo" sp-category="inforevendeur" sp-img sp-hiddable sp-label="Votre logo" sp-type="file" sp-permission="500" sp-order="4">
				<div class="infosHeader">
					<div sp-category="infobat" sp-type="text" sp-label="N° Client" sp-edit="client">CLUB</div><div sp-category="infobat" sp-type="text" sp-label="Nom du club" sp-edit="club" id="headerClub">NOM</div><div sp-category="infobat" sp-type="text" sp-label="Pays" sp-edit="pays">PAYS</div>
				</div>
			</div>
		</div>

		<!--<div>
			<img class="noContract" src="https://dev.superpictor.com/designer/templateSP/templateMoreau/images/leftText.png"></img>
			<img class="scale" src="https://dev.superpictor.com/designer/templateSP/templateMoreau/images/rightText.png"></img>
		</div>-->

		<div class="product">
			<div class="product-pictures contenaire_produit" layout="2-p">
				<div class="picture" sp-type="produit" sp-repeat="6" sp-stackable="produit">
					<img src="" class="imgpdt" sp-type="photo_produit">
				</div>
			</div>
		</div>

		<div class="bat-content" section="infobat">
			<div class="rightInfos">
				<div sp-category="infobat" sp-type="text" sp-label="Strass" sp-edit="strass">STRASS = </div>
				<div sp-category="infobat" sp-type="text" sp-label="CDE" sp-edit="cde">CDE D.....</div>
			</div>

			<div class="bottomInfos" section="infobat">
				<div sp-category="infobat" sp-type="text" sp-label="Date" sp-edit="date">PROPOSITION_P....._DATE</div>
				<div sp-category="infobat" sp-type="text" sp-label="Code en 7" sp-edit="cde7">Code en 7</div>
			</div>
		</div>

		<div class="footer">
			<p>Property of Christian Moreau SAS - All rights reserved Christian-Moreau SAS - Z.I Les Bretteaux - 42410 Saint-Michel-Sur-Rhone.</p>
			<p>Propriété de Christian Moreau SAS - Les créations restent la proptiété de Christian-Moreau SAS - Z.I Les Bretteaux - 42410 Saint-Michel-Sur-Rhone.</p>
		</div>
	</div>
</div>
