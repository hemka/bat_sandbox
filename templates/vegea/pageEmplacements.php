<div class="page">
    <div class="padder">
        <div class="bat-content pageEmplacements" section="infobat">
            <div class="header">
                <div class="header-text uppercase">
                    bon a tirer
                </div>
                <div class="product-info">
                    <div>
                        <span class="label-info">Nom du client: </span>
                        <span class="value-info" sp-edit="name" sp-category="client" sp-label="Nom du client"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1">{CLIENT}</span>
                    </div>
                    <div>
                        <span class="label-info">Ref commande: </span>
                        <span class="value" sp-edit="commande" sp-category="infobat" sp-label="Référence de la commande"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                    </div>
                    <div>
                        <span class="label-info">Ref article: </span>
                        <span class="value" sp-edit="article" sp-category="infobat" sp-label="Ref article"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                    </div>
                    <div>
                        <span class="label-info">Quantité: </span>
                        <span class="value" sp-edit="quantity" sp-category="infobat" sp-label="Quantité"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained>0</span> pièces
                    </div>

                </div>
                <div class="header-logo"  class="company vertical-align">
                    <img src="http://bat.sandbox.local/templates/vegea/images/logo.png" class="logo_header">
                </div>
            </div>
            <div class="content">
                <div class="logo-section">
                    <div class="contenaire_emplacements"  section="infologo">
                        <div sp-type="emplacements" class="emplacements" sp-repeat="2" noemp="" container>

                            <div class="border">
                                <div class="marquage-image" sp-style="background" style>
                                    <div class="container-img">
                                        <img src="{marquage_url}" sp-img sp-static sp-edit="miniature_file" sp-type="file"
                                             class="marquage_img" sp-category="infologo" sp-order="0"/>
                                    </div>
                                </div>
                                <div class="logo-info marquage-details">
                                    <div class="col">
                                        <div>
                                            <span class="marquage-label">Dimensions:</span>
                                            <span class="marquage-value">
                                            <span sp-category="infologo" sp-edit="hauteurtotale" sp-type="text"
                                                  sp-label="Hauteur (en mm)" sp-order="3">0</span> mm x
                                            <span sp-category="infologo" sp-edit="largeurtotale" sp-type="text" sp-label="Largeur (en mm)"
                                                  sp-order="4">0</span> mm
                                        </span>
                                        </div>
                                        <div>
                                            <span class="marquage-label">Position:</span>
                                            <span class="marquage-value">
                                                 <span class="emplacement_marquage" sp-edit="emplacement"></span>
                                                 (<span sp-category="infologo" sp-edit="ref" sp-contained sp-label="Référence" sp-hiddable
                                                       sp-type="text" sp-order="1"></span>)
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div>
                                            <span class="marquage-label">Méthode:</span>
                                            <span class="marquage-value">
                                             <span class="des_nommarquage-text" sp-edit="des_nommarquage"></span>
                                        </span>
                                        </div>
                                        <div>
                                            <span class="marquage-label">Couleurs:</span>
                                            <span class="marquage-value">
                                              <div sp-label="Couleurs" type_display="1" sp-static sp-edit="colors" sp-type="colors" sp-category="infologo" sp-contained sp-hiddable class="box_squares_color"></div>
                                        </span>
                                        </div>
                                    </div>
                                    <div class="marquage-size marquage-item">


                                    </div>
                                </div>
                            </div>


                        </div>


                    </div>

                </div>
            </div>

            <div class="footer">
                <div>Il est de votre responsabilité de vous assurer que ce qui est décrit plus haut correspond exactement à ce que vous souhaitez voir apparaître sur le produit. </div>
                <div>Ceci est la dernière étape avant l'envoi en production. Aucune modification ne sera possible après sa validation. Merci de noter que le délai de livraison est calculé à partir de l'acceptation du bon à tirer. </div>
                <div>Selon la nature et la couleur du produit, le bon à tirer peut légèrement différer du résultat. </div>
            </div>
        </div>
    </div>
</div>
