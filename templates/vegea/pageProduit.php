<div class="page">
    <div class="padder">
        <div class="bat-content" section="infobat">
            <div class="header">
                <div class="header-text uppercase">
                    bon a tirer
                </div>
                <div class="product-info">
                    <div>
                        <span class="label-info">Nom du client: </span>
                        <span class="value-info" sp-edit="name" sp-category="client" sp-label="Nom du client"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1">{CLIENT}</span>
                    </div>
                    <div>
                        <span class="label-info">Ref commande: </span>
                        <span class="value" sp-edit="commande" sp-category="infobat" sp-label="Référence de la commande"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                    </div>
                    <div>
                        <span class="label-info">Ref article: </span>
                        <span class="value" sp-edit="article" sp-category="infobat" sp-label="Ref article"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                    </div>
                    <div>
                        <span class="label-info">Quantité: </span>
                        <span class="value" sp-edit="quantity" sp-category="infobat" sp-label="Quantité"
                              sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained>0</span> pièces
                    </div>

                </div>
                <div class="header-logo"  class="company vertical-align">
                    <img src="http://bat.sandbox.local/templates/vegea/images/logo.png" class="logo_header">
                </div>
            </div>
            <div class="content">
                <div class="product">
                    <div class="product-pictures contenaire_produit" layout="1-p">
                        <div class="picture" sp-type="produit" sp-repeat="3" sp-stackable="produit">
                            <img src="" class="imgpdt" sp-type="photo_produit">
                            <!--                    <div class="picture-label" sp-type="picture-label"></div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="date-signature">
                <span class="label-date">Date et signature</span>
                <span class="value" sp-edit="date" sp-category="infobat" sp-label="Date et signature" sp-type="text"
                      sp-permission="0" sp-order="0" sp-contained></span>
            </div>
            <div class="footer">
                <div>Il est de votre responsabilité de vous assurer que ce qui est décrit plus haut correspond exactement à ce que vous souhaitez voir apparaître sur le produit. </div>
                <div>Ceci est la dernière étape avant l'envoi en production. Aucune modification ne sera possible après sa validation. Merci de noter que le délai de livraison est calculé à partir de l'acceptation du bon à tirer. </div>
                <div>Selon la nature et la couleur du produit, le bon à tirer peut légèrement différer du résultat. </div>
            </div>
        </div>
    </div>
</div>
