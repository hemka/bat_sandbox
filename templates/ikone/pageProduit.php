<div class="page">
	<div class="padder">

		<div class="bat-content d-table" section="infobat">
            <div class="d-table-row header">
                <div class="header-container">
                    <div class="d-table">
                        <div class="d-table-cell middle">
                            <img src="http://bat.sandbox.local/templates/ikone/images/logo.png" class="logo_header">
                        </div>
                        <div class="d-table-cell">
                            <div class="bat-infos">
                                <div class="item-info">
                                    <span class="label-info">Date</span>
                                    <span class=" value-info" sp-edit="date" sp-category="infobat"
                                          sp-label="Date" sp-type="text" sp-permission="0" sp-hiddable sp-order="0"></span>
                                </div>
                                <div class="item-info commercial_container" section="contact">
                                    <span class="label-info">Contact</span>
                                    <span class="value-info" sp-edit="commercial" sp-category="contact"
                                          sp-label="Contact"
                                          sp-type="select" sp-permission="0" sp-order="0" sp-contained></span>
                                </div>
                                <div class="item-info">
                                    <span class="label-info">Dossier</span>
                                    <span class="value-info" sp-edit="name" sp-category="client" sp-label="Nom du client" sp-hiddable
                                          sp-type="text" sp-permission="0" sp-order="1">
                                        {CLIENT}
                                    </span>
                                </div>
                                <div class="item-info">
                                    <span class="label-info">Série</span>
                                    <span class=" value-info" sp-edit="serie" sp-category="infobat"
                                          sp-label="Série" sp-type="text" sp-permission="0" sp-hiddable sp-order="0"></span>
                                </div>
                                <div class="item-info version-info">
                                    <span class="label-info">Version</span>
                                    <span class=" value-info" sp-edit="version" sp-category="infobat"
                                          sp-label="Version" sp-type="text" sp-permission="0" sp-hiddable sp-order="0"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product d-table">
                <div class="d-table-row">
                    <div class="line-above"></div>
                </div>
                <div class="d-table-row h-100 product-content">
                    <div class="title-page"></div>
                    <div class="container-product">
                        <div class="product-pictures contenaire_produit" layout="2-p">
                            <div class="picture" sp-type="produit" sp-repeat="3" sp-stackable="produit">
                                <div class="picture-item">
                                    <div class="picture-container">
                                        <img src="" class="imgpdt" sp-type="photo_produit">
                                    </div>
                                    <div class="picture-label" sp-type="picture-label"></div>
                                </div>
                            </div>
                        </div>
<!--                        <div class="line-product"></div>-->
                    </div>
                </div>
                <div class="d-table-row">
                    <div class="line-below"></div>
                </div>
                <div class="title-logo d-table-row">
                    <div class="container-title-logo">VOS LOGOS</div>
                </div>
                <div class="d-table-row content-bellow" section="infologo">
                    <div class="container-content-bellow">
                        <div class="d-table">
                            <div class="d-table-row">
                                <div class="product-color">
                                    <div class="color-title">
                                        Coloris textile
                                    </div>
                                    <div class="product-color-name">
                                        <div class="color-round-product" sp-style="background"> </div>
                                        <div class="color-name" sp-style="model_product"> </div>
                                    </div>

                                </div>
                                <div class=" note-bat-container">
                                    <div class="note-bat">
                                        Nous vous prions d’examiner ce document
                                        avec beaucoup de soin, afin de nous
                                        indiquer les éventuelles modifications
                                        à apporter.<br/>
                                        Les BAT sont des réalisations numériques
                                        faites avec amour par des passionnés.
                                        Malgré tout, il peut persister de légères
                                        différences entre la version
                                        numérique et la réalité.
                                        <div class="icon-note"></div>
                                    </div>
                                </div>
                                <div class="d-table emplacements-table">
                                    <div class="emplacements-table-container">
                                        <div class="contenaire_emplacements d-table-row">
                                            <div sp-type="emplacements" class="emplacements" sp-repeat="6" noemp="" container>
                                                <div class="container-emplacements">
                                                    <div class="marquage-namebox box" sp-etype="user_img">
                                        <span class="emplacement_marquage"
                                              sp-edit="emplacement"></span>
                                                    </div>
                                                    <div class="logo-section">
                                                        <div class="des_nommarquage">
                                                            <div class="des_nommarquage-text" sp-edit="des_nommarquage"></div>
                                                        </div>
                                                        <div class="container-marquage-image">
                                                            <div class="marquage-image" sp-style="background" style>
                                                                <img src="{marquage_url}" sp-img sp-static sp-edit="miniature_file" sp-type="file"
                                                                     class="marquage_img" sp-category="infologo" sp-order="0"/>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="marquage-details">
                                                        <div class="infos_container">
                                                            <div class="fonts_container">
                                                                <div sp-label="Polices" type_display="1" sp-static
                                                                     sp-edit="fonts" sp-type="fonts"
                                                                     sp-category="infologo" sp-contained sp-hiddable
                                                                     class="text_font_container"></div>
                                                            </div>
                                                            <div class="colors_container">
                                                                <div sp-label="Couleurs" type_display="1" sp-static
                                                                     sp-edit="colors"
                                                                     sp-type="colors"
                                                                     sp-category="infologo" sp-contained sp-hiddable
                                                                     class="box_squares_color"></div>
                                                            </div>
                                                        </div>
                                                        <div class="marquage-info">

                                                            <div class="marquage-size">
                                                                <div class="dimensions">Dimensions marquage</div>
                                                                <div class="size-item largeur">
                                                                    <span class="text-size">Largeur ≈ </span><span sp-category="infologo" sp-edit="largeurtotale"
                                                                                                                   sp-type="text"
                                                                                                                   sp-label="Largeur (en mm)" sp-order="4">0</span> mm
                                                                </div>
                                                                <div class="size-item">
                                                                    <span class="text-size">Hauteur ≈</span> <span sp-category="infologo" sp-edit="hauteurtotale"
                                                                                                                   sp-type="text"
                                                                                                                   sp-label="Hauteur (en mm)" sp-order="3">0</span> mm
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
		</div>
	</div>
</div>
