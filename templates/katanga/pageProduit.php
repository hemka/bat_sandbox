<div class="page">
    <div class="padder">

        <div class="bat-content" section="infobat">
            <div class="header">
                <div class="header-logo"  class="company vertical-align">
                    <img src="http://bat.sandbox.local/templates/katanga/images/logo.png" class="logo_header">
                </div>
                <div class="header-text uppercase">
                    bon a tirer
                </div>
            </div>
            <hr class="divider-header">
            <div class="content">
                <div class="bat-infos">
                    <div class="info">
                        <div class="left-info">
                            <img src="http://bat.sandbox.local/templates/katanga/images/arrow.png" alt="">
                            <span class="label-info uppercase">DOSSIER: </span>
                            <span class="value-info uppercase" sp-edit="name" sp-category="client" sp-label="Nom du client"
                                  sp-hiddable sp-type="text" sp-permission="0" sp-order="1">{CLIENT}</span>
                        </div>
                        <div class="right-info">
                            <div class="date-info">
                                <span class="label-info uppercase">Date: </span>
                                <span class=" value-info uppercase" sp-edit="date" sp-category="infobat"
                                      sp-label="Date" sp-type="text" sp-permission="0" sp-hiddable sp-order="0"></span>
                            </div>
                            <div class="interne-info">
                                <span class="label-info uppercase">Ref Interne: </span>
                                <span class="value-info uppercase" sp-edit="reference" sp-category="infobat"
                                      sp-label="Référence du BAT" sp-hiddable sp-type="text" sp-permission="0" sp-order="1"
                                      sp-contained></span>
                            </div>
                        </div>
                    </div>
                    <hr class="divider-info">
                    <div class="type-product">
                        <img src="http://bat.sandbox.local/templates/katanga/images/arrow.png" alt="">
                        <span class="label-info uppercase">Type Produit:</span>
                        <span class="product-title value-info uppercase" sp-edit="product_name" sp-category="infobat"
                              sp-label="Nom du
                    produit" sp-type="text" sp-permission="0" sp-hiddable sp-order="0"></span>
                    </div>
                </div>
                <div class="product">
                    <div class="product-pictures product contenaire_produit" layout="2-p">
                    <div class="picture" sp-type="produit" sp-repeat="6" sp-stackable="produit">
                        <img src="" class="imgpdt" sp-type="photo_produit">
                        <!--                    <div class="picture-label" sp-type="picture-label"></div>-->
                    </div>
                </div>
                </div>
            </div>

            <div class="footer">
                    <div class="footer-block-item footer-block-1">
                        <div class="attention">
                            <span class="label-footer">*ATTENTION:</span>
                            <span class="comment comment-footer">
                        Ces dimensions peuvent légèrement diminuer pendant la production en fonction de la matière du
                                textile utilisé et de la technique d'impression.
                    </span>
                        </div>
                        <div class="attention">
                            <span class="label-footer">*ATTENTION:</span>
                            <span class="comment comment-footer">
                        Ces maquettes/montages sont réalisés à titre indicatif, les proportions ne sont pas forcément réalistes.
                    </span>
                        </div>

                    </div>
                    <div class="footer-block-item footer-block-2">
                        <div class="footer-block-header uppercase">RELISEZ ATTENTIVEMENT CE DOCUMENT ET RETOURNEZ-LE AU PLUS VITE
                        </div>
                        <div class="footer-block-content">
                            <div>AVEC LA MENTION &lt;&lt;BON POUR IMPRESSION&gt;&gt; (cachet obligatoire)</div>
                            <div>ATTENTION: votre signature vous engage:</div>
                            <div class="text">
                                <div>- elle annule les instructions et les épreuves papier antérieures</div>
                                <div>- elle dégage notre responsabilité même pour les erreurs non corrigées</div>
                            </div>
                            <div class="more-info">
                                <div class="text-date">
                                    Date: ...../...../.....
                                </div>
                                <div class="right-more">
                                    <div class="item">SANS MODIFICATION</div>
                                    <div class="item">AVEC MODIFICATION</div>
                                    <div class="note-more">(dans ce cas, un nouveau BAT vous sera envoyé dans les plus
                                        brefs délais)</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="footer-block-item footer-block-3">
                        <div class="footer-block-header uppercase">VALIDATION RÉSERVÉE À L'ATELIER
                        </div>
                        <div class="footer-block-content">
                            Validation 1er exemplaire.
                        </div>
                    </div>
                </div>

        </div>

        <div class="bat-sidebar contenaire_emplacements" section="infologo">
            <div sp-type="emplacements" class="emplacements" sp-repeat="4" noemp="" container>
                <div class="logo-section">
                    <div class="des_nommarquage">
                        <div class="des_nommarquage-text" sp-edit="des_nommarquage"></div>
                    </div>
                    <div class="marquage-image" sp-style="background" style>
                        <img src="{marquage_url}" sp-img sp-static sp-edit="miniature_file" sp-type="file"
                             class="marquage_img" sp-category="infologo" sp-order="0"/>
                    </div>
                </div>

                <div class="marquage-details">
                    <div class="marquage-name marquage-item" sp-etype="user_img">
                        <span class="marquage-label">Position:</span>
                        <span class="emplacement_marquage marquage-value" sp-edit="emplacement"></span>
                        <span sp-category="infologo" sp-edit="ref" sp-contained sp-label="Référence" sp-hiddable
                              sp-type="text" sp-order="1"></span>
                    </div>
                    <div class="marquage-size marquage-item">
                        <span class="marquage-label">Taille:</span>
                        <span class="marquage-value">
                            <span sp-category="infologo" sp-edit="hauteurtotale" sp-type="text"
                                  sp-label="Hauteur (en mm)" sp-order="3">0</span> mm x
                        <span sp-category="infologo" sp-edit="largeurtotale" sp-type="text" sp-label="Largeur (en mm)"
                              sp-order="4">0</span> mm
                        </span>

                    </div>
                    <div class="marquage-color marquage-item" sp-label="Couleurs" type_display="1" sp-static sp-edit="colors"
                              sp-type="colors" sp-category="infologo" sp-contained sp-hiddable class="box_squares_color"></div>
                </div>
            </div>

        </div>
    </div>
</div>
