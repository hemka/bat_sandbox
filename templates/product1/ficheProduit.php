<div class="page fiche_produit">
    <div class="padder">
        <div class=" d-table" section="infoproduct">
            <div class="header d-table-row">
                <div class="logo">
                    <img class="client_logo" sp-edit="client_logo1" sp-category="infoclient"
                         src="https://dev.superpictor.com/designer/templateSP/brandingImg/logo_sp.png" alt="">
                </div>
                <div class="client_name" sp-edit="client_name1" sp-category="infoclient">{CLIENT}</div>
            </div>
            <div class="d-table-row">
                <div class="product-title-container">
                      <div class="product_title_text" sp-edit="product_name" sp-category="infoproduct" sp-label="Nom"
                            sp-type="text" sp-hiddable></div>
                </div>

            </div>
            <div class="d-table-row product-pictures">
                <div class="info refproduit_container">
                    <span class="label">Reference: </span>
                    <span class="value" sp-edit="refproduit" sp-category="infobat"
                          sp-label="Référence du produit"
                          sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                </div>
                <div class="contenaire_produit products_preview" sp-type="produit">
                    <div class="product-picture-item" sp-repeat="6" sp-stackable="produit">
                        <div class="product-img">
                            <img src="" class="imgpdt" sp-type="photo_produit">
                        </div>
                        <div class="picture-label" sp-type="picture-label"></div>
                    </div>
                </div>
            </div>
            <div class="d-table-row description">
                <div class="description-label">Description :</div>
                <div class="info_text value" sp-edit="product_description" sp-category="infoproduct" sp-type="textarea"
                     sp-hiddable sp-contained sp-label="Description">
                    la parka frigoriste cs10 de longueur 3/4 offre une protection incomparable. La
                    fermeture a glissiere double curseur robuste permet une flexibilité. Maximale de
                    l'utilisateur et les poignets tricotés extra longs avec des trous de pouce offrent
                    une chaleur supplémentaire
                </div>
            </div>

            <div class="d-table-row product-info-container ">
                <div class="product-info">
                    <div class="product-item-info">
                        <div class="label">Marque:</div>
                        <div class="info_text value" sp-edit="product_brand" sp-category="infoproduct" sp-label="Marque"
                             sp-type="text" sp-hiddable sp-contained sp-order="1"></div>
                    </div>
                    <div class="product-item-info">
                        <div class="label">Genre:</div>
                        <div class="info_text value" sp-edit="product_genre" sp-category="infoproduct" sp-label="Genre"
                             sp-type="text" sp-hiddable sp-contained sp-order="2"></div>
                    </div>
                    <div class="product-item-info">
                        <div class="label">Manches:</div>
                        <div class="value" sp-edit="manches" sp-category="infoproduct"
                             sp-contained sp-label="Manches" sp-hiddable sp-type="text" sp-permission="0"
                             sp-order="3"></div>
                    </div>
                    <div class="product-item-info">
                        <div class="label">Tailles:</div>
                        <p class="info_text value" sp-edit="product_size" sp-category="infoproduct" sp-type="text"
                           sp-hiddable sp-contained sp-label="Taille(s)" sp-order="4"></p>
                    </div>
                    <div class="product-item-info">
                        <div class="label">Col:</div>
                        <div class="value" sp-edit="col" sp-category="infoproduct"
                             sp-contained sp-label="Col" sp-hiddable sp-type="text" sp-permission="0" sp-order="5"></div>
                    </div>
                    <div class="product-item-info">
                        <div class="label">Grammage:</div>
                        <div class="value" sp-edit="grammage" sp-category="infoproduct"
                             sp-contained sp-label="Grammage" sp-hiddable sp-type="text" sp-permission="0"
                             sp-order="6"></div>
                    </div>
                </div>

                <div class="product-item-info product-color">
                    <div class="label">Couleurs:</div>
                    <p class="info_text value" sp-edit="product_color" sp-category="infoproduct" sp-type="textarea"
                       sp-hiddable sp-contained sp-label="Couleur(s)"></p>
                </div>

            </div>

            <div class="branding">
                <div spBranding low-branding class="madeSp">
                    by Superpictor with
                </div>
                <img spBranding low-branding class="madeSpPicto"
                     src="https://dev.superpictor.com/designer/templateSP/templateNewsuper/images/picto_coeur.png">
            </div>
        </div>
    </div>
</div>

