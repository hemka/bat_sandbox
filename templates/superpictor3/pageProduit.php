<div class="page" id="page-product">
    <div class="padder">
        <div class="bat-header">
            <div spBranding no-branding class="company vertical-align user block-header f-r">
                <img src="" class="logo_header" sp-edit="user_logo" sp-category="inforevendeur" sp-img sp-hiddable
                     sp-label="Votre logo" sp-type="file" sp-permission="500" sp-order="4">
                <span sp-category="inforevendeur" sp-edit="company" class="uppercase">{nom_entreprise}</span>
            </div>
            <div spBranding high-branding class="company vertical-align block-header f-r">
                <img src="https://superpictor.com/designer/templateSP/templateNewsuper/images/logo_sp.png" class="logo_header">
            </div>
            <div class="title-header">
                BON-À-TIRER
            </div>
        </div>
        <div class="bat-content" section="infobat">
            <div class="bat-infos">
                <div class="bat-infos-box">
                    <div class="client" sp-edit="name" sp-category="client" sp-label="Nom du client" sp-hiddable
                         sp-type="text" sp-permission="0" sp-order="1">
                        {CLIENT}
                    </div>

                    <div class="info-sidebar">
                        <div class="info reference_container">
                            <span class="label">Réf. BAT</span>
                            <span class="value" sp-edit="reference" sp-category="infobat"
                                  sp-label="Référence du BAT"
                                  sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                        </div>
                        <div class="info commande_container">
                            <span class="label">Réf. commande</span>
                            <span class="value" sp-edit="commande" sp-category="infobat"
                                  sp-label="Référence de la commande"
                                  sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                        </div>
                        <div class="info refproduit_container">
                            <span class="label">Réf. produit</span>
                            <span class="value" sp-edit="refproduit" sp-category="infobat"
                                  sp-label="Référence du produit"
                                  sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                        </div>
                        <div class="info commercial_container" section="contact">
                            <span class="label">Affaire suivie par</span>
                            <span class="value" sp-edit="commercial" sp-category="contact"
                                  sp-label="Affaire suivie par"
                                  sp-type="select" sp-permission="0" sp-order="0" sp-contained></span>
                        </div>
                    </div>
                </div>
                <div class="product-title" sp-edit="product_name" sp-category="infobat"
                     sp-label="Nom du produit"
                     sp-type="text" sp-permission="0" sp-hiddable sp-order="0">
                </div>

            </div>
            <div class="container-product">
                <div class="product-pictures contenaire_produit" layout="2-p">
                    <div class="picture" sp-type="produit" sp-repeat="6" sp-stackable="produit">
                        <div class="picture-item">
                            <div class="picture-container">
                                <img src="" class="imgpdt" sp-type="photo_produit">
                            </div>
                            <div class="picture-label block-radius-top-left" sp-type="picture-label"></div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="validation">
                <div class="comment">
                    <div class="comment-content" sp-edit="commentaire" sp-category="infobat" sp-label="Commentaire"
                         sp-type="textarea" sp-permission="0" sp-hiddable sp-order="1">
                        Selon les usages professionnels, il est de votre responsabilité de vous assurer que
                        le
                        marquage est conforme, en vérifiant le design, l’orthographe, les couleurs ainsi que la
                        taille
                        du logo.<br>
                        Nous ne pourrons accepter de réclamation après accord du bon à tirer.
                        Pour rappel, le délai de production de votre commande ne débute qu’après la validation du
                            BAT.
                        Attention, le BAT papier ou écran peut être légèrement différent de l’impression sur le
                        produit.
                    </div>
                </div>

                <div class="accord">
                    <div class="accord-header">
                        <div class="accord-header">
                            <span class="accord-title uppercase">bon pour accord</span>
                            <span class="accord-extra">Merci de dater et de signer</span>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="branding">
            <div spBranding low-branding class="madeSp">
                by Superpictor with
            </div>
            <img spBranding low-branding class="madeSpPicto" src="http://dev.superpictor.com/designer/templateSP/templateNewsuper/images/picto_coeur.png">
        </div>
        <div class="pagination">{pactive}/{pagetotal}</div>
        <div class="footer">
            Le positionnement des logos sur le produit est purement indicatif et peut être modifié selon les contraintes techniques.
        </div>
    </div>
</div>
