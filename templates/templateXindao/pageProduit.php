<div class="page infos_layout" nb_product_img="" nb_logo="">
	<div class="padder">
		<div class="bat-header">
			<img src="https://www.superpictor.com/designer/templateSP/templateXindao/images/top.png" alt="">
		</div>
		<div class="bat-content" section="infobat">
			<h2 class="_bat">BAT Digital</h2>
			<span class="line"></span>
			<div class="infos">
				<div class="command_client_container">
					<span class="static">Commande client :</span>
					<span class="value" sp-edit="command_client" sp-category="infobat" sp-label="Commande client" sp-hiddable sp-contained sp-type="text" sp-permission="0"></span>
				</div>
				<div class="ref_article_container">
					<span class="static">Réf. article :</span>
					<span class="value" sp-edit="ref_article" sp-category="infobat" sp-label="Réf. article" sp-hiddable sp-contained sp-type="text" sp-permission="0"></span>
				</div>
				<div class="command_bat_phy_container">
					<span class="static">Commande BAT physique :</span>
					<span class="value" sp-edit="command_bat_phy" sp-category="infobat" sp-label="Commande BAT physique" sp-hiddable sp-contained sp-type="text" sp-permission="0"></span>
				</div>
				<div class="quantite_container">
					<span class="static">Quantité :</span>
					<span class="value" sp-edit="quantite" sp-category="infobat" sp-label="Quantité" sp-hiddable sp-contained sp-type="text" sp-permission="0"></span>
				</div>
				<div class="ref_bat_dig_container">
					<span class="static">Ref. BAT digital :</span>
					<span class="value" sp-edit="ref_bat_dig" sp-category="infobat" sp-label="Ref. BAT digital" sp-hiddable sp-contained sp-type="text" sp-permission="0"></span>
				</div>
				<div class="quantite_bat_phy_container">
					<span class="static">Quant. BAT physique :</span>
					<span class="value" sp-edit="quantite_bat_phy" sp-category="infobat" sp-label="Quant. BAT physique" sp-hiddable sp-contained sp-type="text" sp-permission="0"></span>
				</div>
				<div class="ref_container">
					<span class="static">Votre référence :</span>
					<span class="value" sp-edit="ref" sp-category="infobat" sp-label="Votre référence" sp-hiddable sp-contained sp-type="text" sp-permission="0"></span>
				</div>				
			</div>
			
			<div class="marquages" section="infologo">
				<div class="contenaire_emplacements" layout="">
					<div class="marquage" sp-type="emplacements" sp-repeat="7" container>
					<h2 class="_seri" sp-edit="des_nommarquage"></h2>
						<div class="details">
								<div>
									<span class="static">Logo : </span>
									<span class="value" sp-category="infologo" sp-edit="logo" sp-type="text" sp-label="Logo"></span>
								</div>

								<div class="marquage-size">
									<span class="static">Taille de marquage (mm) : </span>
									<span class="value">
										<span class="" sp-category="infologo" sp-edit="hauteurtotale" sp-type="text" sp-label="Hauteur (en mm)" sp-order="3">0</span> <span class="">x</span>
										<span class="" sp-category="infologo" sp-edit="largeurtotale" sp-type="text" sp-label="Largeur (en mm)" sp-order="4">0</span> <span class=""></span>
									</span>
								</div>
							<div>
								<span class="static">Position : </span>
								<span class="value emplacement_marquage" sp-edit="emplacement"></span>
							</div>
							<div>
								<span class="static">Nombre de côté : </span>
								<span class="value" sp-category="infologo" sp-edit="nb_sides" sp-type="text" sp-label="Nombre de côté"></span>
							</div>
							<div>
								<span class="static">Nombre de couleurs : </span>
								<span class="value" sp-type="nb_colors"></span>
							</div>
							<div>
								<span class="static">Status : </span>
								<span class="value" sp-edit="status" sp-type="text" sp-label="Status" sp-category="infologo"></span>
							</div>
						</div>
						<div class="colors_container">
							<span class="static">Couleur(s) :</span>
							<div sp-label="Couleurs" type_display="1" sp-static sp-edit="colors" sp-type="colors" sp-category="infologo" sp-contained sp-hiddable class="box_squares_color"></div>
						</div>
						<div class="marquage-image">
							<img src="{marquage_url}" sp-img sp-static sp-edit="miniature_file" sp-type="file" class="marquage_img" sp-category="infologo" sp-order="0">
							<span class="forcenter"></span>
						</div>
					</div>
				</div>
			</div>
			<div class="product">
				<div class="product-pictures contenaire_produit" layout="">
					<div class="picture" sp-type="produit" sp-repeat="4">
						<span class="forcenter"></span>
						<img src="" class="imgpdt" sp-type="photo_produit">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
