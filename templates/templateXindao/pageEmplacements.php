<div class="page pageEmplacement">
   <div class="marquages" section="infologo">
      <div class="contenaire_emplacements" layout="">
         <div class="marquage" sp-type="emplacements" sp-repeat="7" container>
            <div class="marquage-image">
               <img src="{marquage_url}" sp-img sp-static sp-edit="miniature_file" sp-type="file" class="marquage_img" sp-category="infologo" sp-order="0">
               <span class="forcenter"></span>
            </div>

            <div class="details">
               <div class="marquage-details">
                  <div>
                     <span class="title">Nom : </span>
                     <span class="logoInfoVal" sp-category="infologo" sp-edit="nom" sp-type="text" sp-label="Nom"></span>
                  </div>

                  <div class="marquage-size">
                     <span class="title">Taille : </span>
                     <span class="logoInfoVal" sp-category="infologo" sp-edit="hauteurtotale" sp-type="text" sp-label="Hauteur (en mm)" sp-order="3">0</span> <span class="logoInfoVal">mm x</span>
                     <span class="logoInfoVal" sp-category="infologo" sp-edit="largeurtotale" sp-type="text" sp-label="Largeur (en mm)" sp-order="4">0</span> <span class="logoInfoVal">mm</span>
                  </div>
               </div>
               <div>
                  <span class="title">Emplacement : </span>
                  <span class="emplacement_marquage logoInfoVal" sp-edit="emplacement"></span>
               </div>
               <div>
                  <span class="title">Technique : </span>
                  <span class="logoInfoVal" sp-edit="des_nommarquage"></span>
               </div>
               <div class="colors_container">
                  <div class="box_color">
                     <p class="title">Couleur : </p>
                     <div sp-label="Couleurs" sp-static sp-edit="colors" type_display=0 sp-type="colors" sp-category="infologo" sp-contained sp-hiddable class="box_squares_color"></div>
                  </div>
               </div>
            </div>
         </div>

         <div class="approveMessage">
            <span class="line" id="lineApprove"></span>
            <p>« Lu et approuvé » + signature et cachet de l’entreprise :</p>
            <p><span>Fait à</span><span>le</span></p>
         </div>
      </div>
   </div>
</div>