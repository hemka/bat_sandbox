<div class="page">
    <div class="padder">
        <div class="bat-header row">
            <div class="col-6 ">
                <div spBranding no-branding class="company vertical-align user block-header">
                    <img src="" class="logo_header" sp-edit="user_logo" sp-category="inforevendeur" sp-img sp-hiddable
                         sp-label="Votre logo" sp-type="file" sp-permission="500" sp-order="4">
                    <span sp-category="inforevendeur" sp-edit="company" class="uppercase">{nom_entreprise}</span>
                </div>
                <div spBranding high-branding class="company vertical-align block-header">
                    <img src="https://superpictor.com/designer/templateSP/templateNewsuper/images/logo_sp.png" class="logo_header">
                </div>

            </div>
            <div class="col-6 align-r">
                <div class="uppercase vertical-align title-header block-header">bon-à-tirer</div>
            </div>
        </div>
        <div class="bat-content" section="infobat">
            <div class="product">
                <div class="title">
                    <div class="box-title">
                        <img src="http://bat.sandbox.local/templates/superpictor1/images/plus_bg.png" alt="">
                    </div>
                    <div class="under-line"></div>
                    <div class="product-title" sp-edit="product_name" sp-category="infobat" sp-label="Nom du produit"
                         sp-type="text" sp-permission="0" sp-hiddable sp-order="0">
                    </div>
                </div>
                <div class="row-product">
                    <div class="product-pictures contenaire_produit" layout="2-p">
                        <div class="picture" sp-type="produit" sp-repeat="6" sp-stackable="produit">
                            <div class="picture-item">
                                <div class="container-product">
                                    <img src="" class="imgpdt" sp-type="photo_produit">
                                    <div class="picture-label">
                                        <div class="box"></div>
                                        <div  sp-type="picture-label"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="sidebar">
                <div class="bat-row">
                    <div class="bat-infos">
                        <div class="plus-icon">
                            <img src="http://bat.sandbox.local/templates/superpictor1/images/plus.png" alt="">
                        </div>
                        <div class="client" sp-edit="name" sp-category="client" sp-label="Nom du client" sp-hiddable
                             sp-type="text" sp-permission="0" sp-order="1">{CLIENT}
                        </div>
                        <div class="info reference_container">
                            <span class="label">Réf. BAT</span>
                            <span class="value" sp-edit="reference" sp-category="infobat" sp-label="Référence du BAT"
                                  sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                        </div>
                        <div class="info commande_container">
                            <span class="label">Réf. commande</span>
                            <span class="value" sp-edit="commande" sp-category="infobat"
                                  sp-label="Référence de la commande"
                                  sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                        </div>
                        <div class="info refproduit_container">
                            <span class="label">Réf. produit</span>
                            <span class="value" sp-edit="refproduit" sp-category="infobat"
                                  sp-label="Référence du produit"
                                  sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained></span>
                        </div>
                        <div class="info commercial_container" section="contact">
                            <span class="label">Affaire suivie par</span>
                            <span class="value" sp-edit="commercial" sp-category="contact" sp-label="Affaire suivie par"
                                  sp-type="select" sp-permission="0" sp-order="0" sp-contained></span>
                        </div>
                    </div>
                </div>
                <div class="validation">
                    <div class="comment" sp-edit="commentaire" sp-category="infobat" sp-label="Commentaire"
                         sp-type="textarea" sp-permission="0" sp-hiddable sp-order="1">
                        Selon les usages professionnels, il est de <b>votre responsabilité</b> de vous assurer que
                        le
                        marquage est conforme, en vérifiant le design, l’orthographe, les couleurs ainsi que la
                        taille
                        du logo.<br>
                        <b>Nous ne pourrons accepter de réclamation après accord du bon à tirer.</b>
                        <b>Pour rappel, le délai de production de votre commande ne débute qu’après la validation du
                            BAT.</b>
                        Attention, le BAT papier ou écran peut être légèrement différent de l’impression sur le
                        produit.
                    </div>

                </div>

                <div class=" row-accord">
                    <div class="accord">
                        <div class="box f-l"></div>
                        <div class="accord-header f-l">
                            <div class="accord-title uppercase">bon pour accord</div>
                            <div class="accord-extra">Merci de dater et de signer</div>
                        </div>
                    </div>
                </div>
                <div class="branding">
                    <div spBranding low-branding class="madeSp">
                        by Superpictor with
                    </div>
                    <img spBranding low-branding class="madeSpPicto" src="http://dev.superpictor.com/designer/templateSP/templateNewsuper/images/picto_coeur.png">
                </div>

            </div>
        </div>
        <div class="footer">
            <div class="box"></div>
            <div class="text-note">
                Le positionnement des logos sur le produit est purement indicatif et peut être modifié selon les contraintes
                techniques.
            </div>
            <div class="pagination">{pactive}/{pagetotal}</div>
        </div>
        <!-- <h3 class="bat_name" sp-edit="nom" sp-category="infobat" sp-label="Nom du BAT" sp-type="text" sp-permission="0" sp-hiddable sp-order="0">{NomBAT}</h3>

        <img src="" class="imgpdt" sp-type="photo_produit">
        <img src="" class="imgpdt" sp-type="photo_produit">

        <p sp-edit="commentaire" sp-category="infobat" sp-label="Commentaire" sp-type="textarea" sp-permission="0" sp-hiddable sp-order="3">
        // commentaire de validation //
        </p>

        <div class="refvalue" sp-edit="reference" sp-category="infobat" sp-label="Référence du BAT" sp-hiddable sp-type="text" sp-permission="0" sp-order="1" sp-contained>{refBat}</div>

        <div sp-category="inforevendeur" sp-edit="address_1">{adresse}</div>
        <span sp-category="inforevendeur" sp-edit="email" style="display: block;">{email}</span>

        <span sp-edit="Siteweb" sp-category="inforevendeur" sp-permission="450" sp-type="text" sp-order="4" sp-label="Site web">{Siteweb}</span>
        <span class="page">{pactive}/{pagetotal}</span>
        <span sp-category="inforevendeur" sp-edit="email">{email}</span> -->
    </div>
</div>
