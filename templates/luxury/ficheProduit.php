<div id="fiche_produit" class="rendu_template">
   <div class="backgroundColor"></div>
   <div class="padder" section="infoproduct">
      <div class="header">
         <div class="logo">
            <img spBranding high-branding src="https://dev.superpictor.com/designer/templateSP/brandingImg/logo_sp.png" alt="Votre logo" class="logo_header" sp-permission="500" sp-order="4">
         </div>
      </div>
      <div class="body_template border">
         <h1 class="product_name" sp-edit="product_name" sp-category="infoproduct" sp-label="Nom du produit" sp-type="text" sp-permission="0" sp-hiddable sp-order="0"></h1>
         <div class="contenaire_produit products_preview" sp-type="produit">
            <div class="" sp-repeat="6" sp-stackable="produit">
                <img src="" class="imgpdt" sp-type="photo_produit">
            </div>
         </div>
         <div class="consigne align-absolute">
            <p class="info_title">Informations complémentaires:</p>
            <p class="info_text" sp-edit="product_category" sp-category="infoproduct" sp-type="text" sp-hiddable sp-permission="0" sp-label="Informations complémentaires" sp-order="1">
               Renseignez des informations au besoin 
            </p>
         </div>
      </div>

      <div class="branding">
         <div spBranding low-branding class="madeSp">
            by Superpictor with
         </div>
         <img spBranding low-branding class="madeSpPicto" src="https://dev.superpictor.com/designer/templateSP/templateNewsuper/images/picto_coeur.png">
      </div>

      <div class="reference_container border">
         <div class="reftext">Marque</div>
         <div class="align-absolute">
            <div class="refvalue" sp-edit="product_brand" sp-category="infoproduct" sp-contained sp-label="Marque" sp-hiddable sp-type="text" sp-permission="0" sp-order="2"></div>  
         </div>
      </div>
   </div>
   <div class="footer">
   </div>
   <div class="poweredBy"></div>
</div>

