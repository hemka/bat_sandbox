<div class="page">
    <div class="padder">
        <div class="bat-header">
            <div class="product-title" sp-edit="product_name" sp-category="infobat" sp-label="Nom du produit" sp-type="text" sp-permission="0" sp-hiddable sp-order="0"></div>
        </div>

        <div class="bat-content" section="infobat">
            <div class="contentCol leftContent">
                <div class="description">
                    <p class="title">DESCRIPTIF</p>
                    <p class="value" sp-edit="descriptif" sp-category="infobat" sp-label="Descriptif" sp-hiddable sp-type="textarea" sp-permission="0" sp-order="1">.....</p>
                </div>

                <div class="product">
                    <div class="product-pictures contenaire_produit" layout="2-p">
                        <div class="picture" sp-type="produit" sp-repeat="6" sp-stackable="produit">
                            <img src="" class="imgpdt" sp-type="photo_produit">
                        </div>
                    </div>
                </div>
            </div>

            <div id="border"></div>

            <div class="contentCol rightContent">
                <div class="delais">
                    <p class="title">DELAI</p>
                    <p class="value" sp-edit="delai" sp-category="infobat" sp-label="Delai" sp-hiddable sp-type="text" sp-permission="0" sp-order="1">.....</p>
                </div>

                <div class="budget">
                    <p class="title">BUDGET (Prix Unitaire HT)</p>
                    <p class="value" sp-edit="budget" sp-category="infobat" sp-label="Budget" sp-hiddable sp-type="textarea" sp-permission="0" sp-order="1">.....</p>
                </div>
            </div>
        </div>

        <div class="bat-footer" section="infobat">
            <img src="http://sandbox-bat.local/templates/mCom/images/mComLogo.png">
            <div id="separator">|</div>
            <div class="client" sp-edit="name" sp-category="client" sp-label="Nom du client" sp-hiddable sp-type="text" sp-permission="0" sp-order="1"></div>
            <div class="footer-contact">
                <p>M COM</p>
                <p>60 avenue Tony Garnier - 69007 LYON</p>
                <p>+33 (0)4 78 78 10 06 - www.m-com.com</p>
            </div>
        </div>
    </div>
</div>