<div class="page">
    <div class="bat-header">
        <img src="http://sandbox-bat.local/templates/mCom/images/mComLogo.png">
    </div>

    <div class="bat-content">
        <div class="marquages" section="infologo">
         <div class="contenaire_emplacements">
            <div class="marquage" sp-type="emplacements" sp-repeat="4" noemp="" container>
               <div class="marquage-image" sp-style="background" style>
                  <img src="{marquage_url}" sp-img sp-static sp-edit="miniature_file" sp-type="file" class="marquage_img" sp-category="infologo" sp-order="0">
               </div>

               <div class="marquage-details">
                  <div class="marquage-name" sp-etype="user_img">
                     <span class="emplacement_marquage" sp-edit="emplacement"></span>
                     <span class="ref_container">
                        (<span sp-category="infologo" sp-edit="ref" sp-contained sp-label="Référence" sp-hiddable sp-type="text" sp-order="1"></span>)
                     </span>
                  </div>
                  <div class="marquage-size">
                     <span sp-edit="des_nommarquage"></span> -
                     <span sp-category="infologo" sp-edit="hauteurtotale" sp-type="text" sp-label="Hauteur (en mm)" sp-order="3">0</span> mm x
                     <span sp-category="infologo" sp-edit="largeurtotale" sp-type="text" sp-label="Largeur (en mm)" sp-order="4">0</span> mm
                  </div>  
                  <div class="marquage-comment" sp-category="infologo" sp-edit="comment" sp-label="Commentaire" sp-hiddable sp-type="textarea" sp-order="2"></div>
               </div>
               <div class="colors_container">
                  <div sp-label="Couleurs" type_display="1" sp-static sp-edit="colors" sp-type="colors" sp-category="infologo" sp-contained sp-hiddable class="box_squares_color"></div>
               </div>
            </div>
         </div>
      </div>
    </div>
</div>

