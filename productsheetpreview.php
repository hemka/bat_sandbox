
<?php

/* ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1); */

require_once $_SERVER['DOCUMENT_ROOT'].'/src/libraries/SuperEntity.php';

// $user   = JFactory::getUser();
$model = new SuperSource();
$idDoc = $model->decrypt($_GET["id"], true);
$product_sheet =  SuperEntity::get("document", $idDoc);
$product_sheet->setnbpages();
$product_sheet->io_load();
if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['client']['identifier'])) {
	$identifier = $product_sheet->decrypt($_POST['client']['identifier'], true);	
	$product_sheet->dxupsert('1_element_element', array(
		'source'    => ['value' => $product_sheet->type, 'key' => true],
		'source_id' => ['value' => $product_sheet->id, 'key' => true],
		'target'    => ['value' => 'client', 'key' => true],
		'target_id' => ['value' => $identifier]
	));
}
if (isset($_GET["templateSlug"])) {
	$returnUrl = $_SERVER['PHP_SELF']."?id=".$_GET['id'];
	$template = $product_sheet->select("SELECT * FROM `0_document_templates` WHERE slug = " . $product_sheet->quote($_GET["templateSlug"]));
	if ($template) {
		$product_sheet->updateTemplate(intval($template->id));
		header('location: '.$returnUrl);
	}	
	
}
// get editable fields de template html to display in menu "modifier les infos"
$editables = $product_sheet->getTemplateEditableFields();
foreach ($editables as $category => $editable)
	$$category = $editable; //? creation de variables dynamiques en fonction du nom de la category
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$exit = false;
	if (!empty($_POST['infoproduct'])) {
		$tmp = new stdClass();

		foreach ($_POST['infoproduct'] as $name => $value) {
			if ($name != 'update' && $name != 'trig' && $name != 'i_b') {
				$hidden = isset($_POST["hide-infoproduct"][$name]);
				$value = htmlspecialchars($value);

				$tmp->$name = compact('value', 'hidden');
			}
		}

		$exit = true;
		$product_sheet->updateInfoFP($tmp);
	}
	if ($exit) exit;
}
	
function buildEditableForm($editables, $infos) {
	$forSection = $forContainer = null;
	if (!empty($editables['forSection'])) {
	   $forSection = $editables['forSection'];
	   unset($editables['forSection']);
	}

	if (!empty($editables['forContainer'])) {
		$forContainer = $editables['forContainer'];
	   unset($editables['forContainer']);
	}

	$form = '<div class="form-container"' . (($forContainer) ? ' data-container="' . $forContainer . '"' : '') . (($forSection) ? ' data-section="' . $forSection . '"' : '') . '>';
	foreach ($editables as $edit => $field) {
		$field->value = (isset($infos->$edit->value)) ? $infos->$edit->value : $field->value;
		$field->hidden = (isset($infos->$edit->hidden)) ? $infos->$edit->hidden : null;

		   $form .= "<div class='form-group' data-field='$edit'>";

		$inputName = strtolower($edit);
		if ($forSection)
			$inputName = "$forSection" . (($forContainer) ? "[$forContainer]" : '') . "[$inputName]";

		if ($field->type == 'textarea') {
			$form .= "<textarea id='$edit' name=\"$inputName\" autocomplete='off'>$field->value</textarea>";
		}
		else if ($field->type == 'select') {
			if (!empty($field->selectOptions)) {
				$form .= "<select class='spSelect' id='$edit' name=\"$inputName\" autocomplete='off'><option value=''></option>";

				foreach ($field->selectOptions as $opt) {
					$form .= "<option value='" . $opt['value'] . "'";
					$form .= ((strtolower($field->value) == strtolower($opt['text'])) ? ' selected' : '') . ">";
					$form .= $opt['text'] . "</option>";
				}

				$form .= "</select>";
			}
		}
		else {
			if ($field->isImg && $field->value)
				$form .= "<input type='hidden' name=\"$inputName\" value='" . $field->value . "'>";

			$imgSrc = ($field->isImg && $field->value) ? $field->value : 'images/upload.png';

			$form .= "<input id='$edit' static='" . ($field->isStatic ? "true" : "false") . "' type='$field->type' " . (($field->isImg && $field->value) ? 'uped="uped"' : '') . "name=\"$inputName\" " . (($field->type != 'file') ? "value='$field->value'" : "data-src='$imgSrc'") . " autocomplete='off'>";
		}

		if ($field->label != '') {
			$form .= "<div class='flex-label'>";
			$form .= "<label for='$edit'>$field->label</label>";

			if ($field->hiddable) {
				$hidden = $field->hidden;
				$form .= "<label for=\"hide-$inputName\" class='hiddable " . (($hidden) ? 'hidden' : 'visible') . "'>";
				$form .= "<input id=\"hide-$inputName\" type='checkbox' name=\"hide-$inputName\"" . (($hidden) ? ' checked' : '') . " autocomplete='off'>";
				$form .= "<i class='material-icons'></i>";
			}

			$form .= '</div>';
		}

		   $form .= '</div>';
	}

	$form .= '</div>';
	return $form;
}
?>
<html lang="en">
<head>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://www.cdnsfront.com/ajax/libs/sp-select/sp-select.css?<?= time() ?>">
	<link rel="stylesheet" href="/src/style/template.css?<?= time() ?>">
	<link rel="stylesheet" href="/src/style/incstyle.php?st=supertemplate&<?= time() ?>">

	<script src="/src/scripts/jquery/jquery-3.2.1.min.js"></script>
	<script src="https://www.cdnsfront.com/ajax/libs/sp-select/sp-select.js?<?= time() ?>"></script>

	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sandbox | Preview</title>
</head>
<body>
<link rel="stylesheet" href="<?php echo $product_sheet->getStyleSheetPath();?>">
<link href="/src/style/incstyle.php?st=document" rel="stylesheet">



<link rel="stylesheet" href="/src/style/template.css?<?= time() ?>">
<script src="https://www.cdnsfront.com/ajax/libs/sp-alert/sp-alert.js?<?= time() ?>"></script>
<script src="/src/scripts/sp-panel.js"></script>
<a id="backPdt" href="/"><span class="material-icons">arrow_back</span>Products</a>

<form id="chooseSlug" action="" method="GET">
	<input type="hidden" name="id" value=" <?= $_GET['id'] ?>">
	<input type="text" name="templateSlug" id="templateSlug" placeholder="Enter your template slug">
	<input type="submit" value="Go">
</form>
<div class="preview-bat">
	<div id="fullscreen">
		<div class="pages"> 
            <?php 
			$product_sheet->setTemplateEditableFields();
            $product_sheet->displayPage(); 			
            ?>
        </div>
		<div id="edit-bat" class="panel">
			<div class="panel-header">
				<button type="button" class="close-panel"><i class="material-icons">clear</i></button>
			</div>

			<div class="panel-content">
				<form>
					<div class="infobat-form form">
						<span class="form-title-container">
							<span class="form-title">Informations de la fiche produit</span>
							<div class="collapse-form-arrow">
								<span class="arrow-branch"></span><span class="arrow-branch"></span>
							</div>
						</span>
						<div class="form-collapse">
							<?php 
								$infosFP = $product_sheet->getInfosFP(); 
								$infoproduct['forSection'] = 'infoproduct'; //le nom de variable est le meme nom de sp-category dans template html
								echo buildEditableForm($infoproduct, json_decode($infosFP->fields));
							?>
							
						</div>
					</div>
					<input type="hidden" name="i_b" value="<?= $idDoc; ?>">
				</form>
			</div>
			<div class="panel-footer">
				<button type="submit" class="spbtnblue" name="update">Sauvegarder</button>
			</div>
		</div>
		
    </div>
    <div id="bat-menu" class="bat-menu panel panel-standalone">
		<div class="panel-opener"><span class="material-icons">more_vert</span></div>
		<div class="panel-content">
			<div class="flex-content">
				<!-- <div class="flex-content-item">
					<button class="bat-menu-item" popin="#clientPopin">
						<img src="/images/picto/svg/user.svg" class="sp-icon">
						<span>Affecter un client</span>
					</button>
				</div> -->
				<!-- <div class="flex-content-item">
					<button class="bat-menu-item" id="previous">
						<img src="/images/picto/svg/left-arrow.svg" class="sp-icon">
						<span>Précedent</span>
					</button>
				</div> -->
				<div class="flex-content-item">
					<button class="bat-menu-item" panel="edit-bat">
						<img src="/src/style/images/document.svg" class="sp-icon">
						<span>Modifier les infos</span>
					</button>
					
					<?php 
						$idbat = intval($product_sheet->getInfoBat()->rowid);
						$idbatCrypt = $model->DanXEncrypt($idbat); 
					?>
					
				</div>
				<div class="flex-content-item">
					<!-- <button  class="bat-menu-item" popin="#ordersLogos">
						<img src="/images/picto/svg/play-button.svg" class="sp-icon">
						<span>Commander les logos</span>
					</button>
					<button class="bat-menu-item" id="send" popin="#sendMailAsk">
						<img src="/images/picto/svg/envelope.svg" class="sp-icon">
						<span>Envoyer par mail</span>
					</button> -->
					<?php 
					$fichePdf =  SuperEntity::get("document", $idDoc);
					$fichePdf->setContext('server');
					$fichePdf->setnbpages(); 
					$fichePdf->io_load(); 
					$fichePdf->setTemplateEditableFields();
					?>
					<a class="bat-menu-item" download="fiche_produit" href="<?php echo $fichePdf->sortiePDF() ?>">
						<img src="/src/style/images/pdf.svg" class="sp-icon">
						<span>Télécharger</span>
					</a>
				</div>				
			</div>
		</div>
	</div>   

</div>
<script>
	function replaceClass(elmt, classToRemove, classToAdd) {
		if (typeof elmt == 'string') elmt = $(elmt);

		elmt.removeClass(classToRemove);
		elmt.addClass(classToAdd);
	}

	function getEditableTarget(elmt) {
		let field = ($(elmt).is('.form-group')) ? $(elmt).data('field') : elmt.id;
		let forContainer = $(elmt).parents('.form-container').attr('data-container');
		let forSection = $(elmt).parents('.form-container').attr('data-section');
		let target = '';

		for (let [attr, forWhat] of new Map([['section', forSection], ['container', forContainer]]).entries()) {
			if (forWhat !== undefined) {
				if (forWhat.indexOf(' ') !== -1)
					forWhat = forWhat.replace(' ', '+');

				target += `[${attr}="${forWhat}"] `;
			}
		}

		target = $(`${target}[sp-edit="${field}"]`);

		return target;
	}
	$(function() {
		// gerer affectation client
		window.addEventListener('message', function(e) {
			var data = JSON.parse(e.data);

			if (data.identifier !== undefined) {
				let buttons = $('#clientPopin .popin-body .button-group');

				buttons.css('visibility', 'visible').fadeTo(200, 1);
				buttons.find('[name="client[identifier]"]').val(data.identifier);
			}
		});
//  gerer les modifier les infor
		$('.spSelect').spSelect();

		$('.panel-opener').on('click', function(e) {
			let panel = $(this).closest('.panel');

			if (panel.hasClass('panel-opened'))
				closePanel(panel, e);
			else
				openPanel(panel, e);
		});

		$('form .form-group, form .form-group input, form .form-group textarea').on('mouseenter mouseleave focusin focusout', function(e) {
			var target = getEditableTarget(this);

			if ($(this).is('.form-group')) {
				if (e.type == 'mouseenter')
					target.removeClass('persist').addClass('active');
				else if (e.type == 'mouseleave')
					target.removeClass('active');
			} else {
				if (e.type == 'focusin')
					target.removeClass('active').addClass('persist');
				else if (e.type == 'focusout')
					target.removeClass('persist active');
			}
		});

		$(document).on('keyup change', '.form input[type="text"], .form textarea, .form select', function() {
			var target = getEditableTarget(this);

			if (target) {
				let value = this.value;

				if ($(this).is('select'))
					value = $(this).find(`option[value="${value}"]`).text();

				target.text(value);
			}
		});

		$(document).on('change', 'label.hiddable input[type="checkbox"]', function() {
			var target = getEditableTarget($(this).closest('.form-group'));
			if (target.attr('sp-contained') !== undefined) {
				let targetName = target.attr('sp-edit');
				target = target.closest(`.${targetName}_container`);
			}

			if (target) {
				if (this.checked)
					target.attr('hidden-element', '');
				else
					target.removeAttr('hidden-element');
			}
		});

		$('.form label.hiddable').on('click', function(e) {
			if ($(this).hasClass('visible')) {
				replaceClass($(this), 'visible', 'hidden');
			} else if ($(this).hasClass('hidden')) {
				replaceClass($(this), 'hidden', 'visible');
			}
		});

		$(document).on('submit', '#edit-bat form', function(e) {
			e.preventDefault();
		});

		$(document).on('click', '[name="update"]', function() {
			$('#edit-bat').prepend($('<div class="loading"><div>'));
			let button_text = $(this).text();
			var that = this;
			
			var formData = new FormData(document.querySelector('#edit-bat form'));
			$(this).text('Sauvegarde en cours');

			$.ajax({
				type: "post",
				url: "",
				data: formData,
				processData: false,
				contentType: false
			})
			.done(function() {
				spAlert('Les modifications ont été sauvegardées', {type: 'success'});
				$('#edit-bat .loading').remove();
				$(that).text(button_text);
				document.location.reload();
			});
		});

		$(document).on('click', '.alert-container .close-alert', function() {
			$(this).closest('.alert-container').animate({
				opacity: 0,
				top: '10px'
			}, 200, function() {
				$(this).remove();
			});
		});

		$('.form input[type="file"]').each(function() {
			var input = $(this);
			var wrapper = $(
				'<div class="main-wrapper">' +
				'<div class="filewrapper wrapped-' + input.attr('id') + '">' +
					'<img src="' + input.data('src') + '">' +
				'</div>' +
				'</div>'
			);

			if (input.attr('static') == 'false') {
				var img = wrapper.find('img');
				img.attr('src', (img.attr('src') + '?' + $.now()));

				$('<div class="overlay">' +
					'<p class="overlay-text">Télécharger une photo</p>' +
				'</div>').insertAfter(img);

				if (input.attr('uped') == 'uped')
				wrapper.append($('<div class="actions"><span class="logo-action edit">Modifier</span><span class="logo-action remove">Supprimer</span></div>'));
			} else
				wrapper.addClass('static');

			input.addClass('gtfo').attr('accept', '.png, .jpg, .jpeg');
			input.after(wrapper);

			if (input.attr('static') == 'true') {
				input.prev('input[type="hidden"]').remove();
				input.remove();
			}
		});

		$(document).on('click', '.filewrapper', function(e) {
			e.stopPropagation();
			$(this).closest('.form-group').find('input[type="file"]').trigger('click');
		});

		$(document).on('click', '.form-title-container', function() {
			let arrow = $(this).find('.collapse-form-arrow');

			if (arrow.hasClass('active')) {
				arrow.removeClass('active');
				arrow.closest('.form').find('.form-collapse').slideUp(200);
			}
			else {
				arrow.addClass('active');
				arrow.closest('.form').find('.form-collapse').slideDown(200);
			}
		});

		//? Remove l'input pour les statics

		$('.form input[static="true"]').each(function() {
			$('.form input[static="true"]').attr('type', 'hidden');
		});

	});

	//? Bouton Previous
	<?php $id = ($product_sheet->source == 'product') ? $product_sheet->source_id : $model->encrypt($product_sheet->source_id, true);?>
	let is_pdt = '<?= $product_sheet->source; ?>';
	$('#previous').on('click', function() {
		var url_string = window.location.href; //window.location.href
		var url = new URL(url_string);
		if(is_pdt == 'product')
			window.location.assign("./singleproduct.html?code=<?= $id; ?>");
		else
			window.location.assign("./superpictor.html?view=mockup&id=<?= $id; ?>");
	});

</script>
</body>
</html>