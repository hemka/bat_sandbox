<title>Sandbox | Superpictor</title>

<body>
    <div class="logo"><img src="src/style/images/stickersplogo.png" alt=""> <p>Superpictor</p></div>

    <a href="/preview.php?id=ODIwMzI4ZDQxNDQ1MzhjNTA2MzM2NjAzNThjMzc5ZDdKZ0RiNnc9PQ==">1 logo</a>
    <a href="/preview.php?id=OGY3Y2E3M2QxMDYzZjY5YTNhM2Y5M2UzMTIyMGMzN2Y4bmk4QWc9PQ==">2 logos</a>
    <a href="/preview.php?id=MGNhZDhmMDQxMGQyNDIwZjQ0NzI3MGNhMjZhZTU3NDc0ckUyNEE9PQ==">3 logos</a>
    <a href="/preview.php?id=MGI4OTJkOTY0MWYxZjdjMjhhYjhlZjcxZWVkYTU5ZGUwR0xESVE9PQ==">4 logos</a>
    <a href="/preview.php?id=YzQ3MTU1MzI0YTdkZDA3ZjEwMDA2OWViMTBhZGM3OTk0aWdQZnlVPQ==">Texts</a>
    <a href="/preview.php?id=ZjMxYmRlNWY2MWE2NDU3YjgwZjczZmJhNjYzZWY0ZTdnMXNjVFVRPQ==">3 views products</a>
    <a href="/productsheetpreview.php?id=ZmNmZDYxNzBjOWE4NmE4NGZlMmVmNGU1NTE0NDA1ZmZNcU42">view product sheet</a>
</body>

<style>
    body { 
        background: #f6f5fb; 
        font-family: sans-serif;
    }
    .logo {
        display: flex;
        justify-content: center;
        align-items: center;
    }
    .logo img{
        width: 100px;
    }
    .logo p {
        margin-left: 20px;
        font-size: 50px;
        font-weight: bold;
    }
    a {
        display: block;
        background: #003c9d;
        color: white;
        padding: 20px 10px;
        width: 100px;
        text-align: center;
        border-radius: 10px;
        margin: 20px auto;
        text-decoration: none;
    }
</style>