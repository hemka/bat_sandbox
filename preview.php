 <?php
	// ini_set('display_errors', 1);
	// ini_set('display_startup_errors', 1);
	// error_reporting(E_ALL);

	// session_start();
	// var_dump($_SESSION['lastVisitedPage']);
    //error_reporting(E_ERROR | E_PARSE);
	header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
	header("Cache-Control: post-check=0, pre-check=0", false);
	header("Pragma: no-cache");

	require $_SERVER['DOCUMENT_ROOT'] . '/src/libraries/helpers.php';
	require $_SERVER['DOCUMENT_ROOT'] . '/src/libraries/SuperBAT.php';

	$smodel = new SuperSource();
	if (isset($_GET['idbat']) && !empty($_GET['idbat'])){
		$idbatCrypt = ($_GET['idbat']);
		$idbat = $smodel->DanXDecrypt($_GET['idbat']);
	} else if (isset($_GET["id"])) {
		$idbat = $smodel->decrypt($_GET["id"], true);
		$idbatCrypt = $smodel->DanXEncrypt($idbat);
	}

	$numpage = 1;
	if (isset($_GET['no']) && !empty($_GET['no']))
		$numpage = $_GET['no'];

	$bat = new SuperBAT($idbat);
	// var_dump($bat);
	if ($_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['client']['identifier'])) {
		$identifier = $bat->decrypt($_POST['client']['identifier'], true);

		/* $bat->dxupsert('1_element_element', array(
			'source'    => ['value' => 'bat', 'key' => true],
			'source_id' => ['value' => $idbat, 'key' => true],
			'target'    => ['value' => 'client', 'key' => true],
			'target_id' => ['value' => $identifier]
		)); */
		$bat->dxupsert('1_element_element', array(
			'source'    => ['value' => 'mockup', 'key' => true],
			'source_id' => ['value' => $bat->designed->rowid, 'key' => true],
			'target'    => ['value' => 'client', 'key' => true],
			'target_id' => ['value' => $identifier]
		));
	}

	$infos = $bat->getInfos();
	$bat->initMaquetor(0);

	if (isset($_GET["externalid"]))
		$bat->maquetor->initExternal(intval($_GET["externalid"]));

	if (isset($_GET["ref"]))
		$bat->maquetor->addioVariables("infobat", "reference", $_GET["ref"]);


	if (isset($_GET["templateSlug"])) {
		$tmpl = $bat->select("SELECT * FROM `1_bat_templates` WHERE slug = " . $bat->quote($_GET["templateSlug"]));

		if ($tmpl) {
			$bat->maquetor->templateSlug = $tmpl->slug;
			$bat->maquetor->templateOrientation = $tmpl->orientation;
			$bat->maquetor->templateId = $tmpl->id;
		}
	}

	$editables = $bat->maquetor->getTemplateEditableFields();
	foreach ($editables as $category => $editable)
		$$category = $editable; //? creation de variables dynamiques en fonction du nom de la category

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$exit = false;

		if (!empty($_POST['infobat'])) {
			$tmp = new stdClass();

			foreach ($_POST['infobat'] as $name => $value) {
				if ($name != 'update' && $name != 'trig' && $name != 'i_b') {
					$hidden = isset($_POST["hide-infobat"][$name]);
					$value = htmlspecialchars($value);

					$tmp->$name = compact('value', 'hidden');
				}
			}

			$exit = true;
			$bat->updateInfoBat($tmp);
		}

		if (!empty($_POST['infologo'])) {
			$hidden = array();

			foreach ($_POST['infologo'] as $container => $values) {
				foreach ($values as $name => $value) {
					if (isset($_POST['hide-infologo'][$container][$name])) {
						$hidden[$container][] = $name;
					}
				}
			}

			$exit = true;
			$bat->setInfoLogo($_POST['infologo'], $hidden);
		}

		if (!empty($_POST['contact']['commercial'])) {
			$bat->dxupsert('1_element_element', array(
				'source'    => ['value' => 'user', 'key' => true],
				'source_id' => ['value' => $_POST['contact']['commercial']],
				'target'    => ['value' => 'bat', 'key' => true],
				'target_id' => ['value' => $idbat, 'key' => true]
			));

			$exit = true;
		}



		if ($exit) exit;
	}

	function buildEditableForm($editables, $infos) {
		$forSection = $forContainer = null;

		if (!empty($editables['forSection'])) {
		   $forSection = $editables['forSection'];
		   unset($editables['forSection']);
		}

		if (!empty($editables['forContainer'])) {
			$forContainer = $editables['forContainer'];
		   unset($editables['forContainer']);
		}

		$form = '<div class="form-container"' . (($forContainer) ? ' data-container="' . $forContainer . '"' : '') . (($forSection) ? ' data-section="' . $forSection . '"' : '') . '>';

        foreach ($editables as $edit => $field) {
			$field->value = (isset($infos->$edit->value)) ? $infos->$edit->value : $field->value;
			$field->hidden = (isset($infos->$edit->hidden)) ? $infos->$edit->hidden : null;

		   	$form .= "<div class='form-group' data-field='$edit'>";

			$inputName = strtolower($edit);
			if ($forSection)
				$inputName = "$forSection" . (($forContainer) ? "[$forContainer]" : '') . "[$inputName]";

			if ($field->type == 'textarea') {
				$form .= "<textarea id='$edit' name=\"$inputName\" autocomplete='off'>$field->value</textarea>";
			}
			else if ($field->type == 'select') {
				if (!empty($field->selectOptions)) {
					$form .= "<select class='spSelect' id='$edit' name=\"$inputName\" autocomplete='off'><option value=''></option>";

					foreach ($field->selectOptions as $opt) {
						$form .= "<option value='" . $opt['value'] . "'";
						$form .= ((strtolower($field->value) == strtolower($opt['text'])) ? ' selected' : '') . ">";
						$form .= $opt['text'] . "</option>";
					}

					$form .= "</select>";
				}
			}
			else {
				if ($field->isImg && $field->value)
					$form .= "<input type='hidden' name=\"$inputName\" value='" . $field->value . "'>";

				$imgSrc = ($field->isImg && $field->value) ? $field->value : 'images/upload.png';

				if($field->isStatic && $field->type === "colors") {
					$form .= "<input id='$edit' static='" . ($field->isStatic ? "true" : "false") . "' type='$field->type'" . "name=\"$inputName\"" . "autocomplete='off'>";
				} else {
					$form .= "<input id='$edit' static='" . ($field->isStatic ? "true" : "false") . "' type='$field->type' " . (($field->isImg && $field->value) ? 'uped="uped"' : '') . "name=\"$inputName\" " . (($field->type != 'file') ? "value='$field->value'" : "data-src='$imgSrc'") . " autocomplete='off'>";
				}


			}

			if ($field->label != '') {
				$form .= "<div class='flex-label'>";
				$form .= "<label for='$edit'>$field->label</label>";

				if ($field->hiddable) {
					$hidden = $field->hidden;
					$form .= "<label for=\"hide-$inputName\" class='hiddable " . (($hidden) ? 'hidden' : 'visible') . "'>";
					$form .= "<input id=\"hide-$inputName\" type='checkbox' name=\"hide-$inputName\"" . (($hidden) ? ' checked' : '') . " autocomplete='off'>";
					$form .= "<i class='material-icons'></i>";
				}

				$form .= '</div>';
			}

		   	$form .= '</div>';
		}

		$form .= '</div>';
		return $form;
	}
?>

<?php if (!defined('COMPONENT')) { ?>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://www.cdnsfront.com/ajax/libs/sp-select/sp-select.css?<?= time() ?>">
	<link rel="stylesheet" href="/src/style/template.css?<?= time() ?>">
	<link rel="stylesheet" href="/src/style/incstyle.php?st=supertemplate&<?= time() ?>">

	<script src="/src/scripts/jquery/jquery-3.2.1.min.js"></script>
	<script src="https://www.cdnsfront.com/ajax/libs/sp-select/sp-select.js?<?= time() ?>"></script>

<?php }


?>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Sandbox | Preview</title>
</head>
<body>

<link rel="stylesheet" href="https://www.cdnsfront.com/ajax/libs/sp-alert/sp-alert.css?<?= time() ?>">
<link rel="stylesheet" href="<?php $bat->maquetor->setContext('server'); echo $bat->maquetor->getStyleSheetPath() ?>?d=<?= time() ?>">
<link href="/src/style/incstyle.php?st=previewBat" rel="stylesheet">

<script src="https://www.cdnsfront.com/ajax/libs/sp-alert/sp-alert.js?<?= time() ?>"></script>
<script src="/src/scripts/sp-panel.js"></script>

<a id="backPdt" href="/"><span class="material-icons">arrow_back</span>Products</a>

<form id="chooseSlug" action="" method="GET">
	<input type="hidden" name="id" value=" <?= $_GET['id'] ?>">
	<input type="text" name="templateSlug" id="templateSlug" placeholder="Enter your template slug">
	<input type="submit" value="Go">
</form>

<div class="preview-bat">
	<div id="fullscreen">
		<div class="pages">
			<?php
				$bat->maquetor->setContext('client');
				$bat->maquetor->loadTemplate();
				$countPage = $bat->maquetor->numOfPage();

				for ($i = 0; $i <= $countPage - 1; $i++) {
					echo $bat->maquetor->displayPage($numpage);
					$numpage ++;
				}
			?>
		</div>
		<div id="edit-bat" class="panel">
			<div class="panel-header">
				<button type="button" class="close-panel"><i class="material-icons">clear</i></button>
			</div>

			<div class="panel-content">
				<form>
					<div class="infobat-form form">
						<span class="form-title-container">
							<span class="form-title">Informations de contact</span>
							<div class="collapse-form-arrow">
								<span class="arrow-branch"></span><span class="arrow-branch"></span>
							</div>
						</span>
						<div class="form-collapse">
							<?php
								$contact_fields = new stdClass();

								$user_q = $bat->execute(
									"SELECT ui.virtuemart_user_id AS id, ui.first_name, ui.last_name
									FROM `1_element_element` AS ee
									INNER JOIN `digime_virtuemart_userinfos` AS ui 
									ON ee.source_id = ui.virtuemart_user_id
									WHERE `source` = 'user' AND `target` = 'bat' AND `target_id` = $idbat"
								);
								if ($user = $user_q->fetch(PDO::FETCH_OBJ)) {
									$contact_fields = new stdClass();
									$contact_fields->commercial = new stdClass();
									$contact_fields->commercial->value = "$user->first_name $user->last_name";
								}

								$contact['forSection'] = 'contact';
								echo buildEditableForm($contact, $contact_fields);
							?>
						</div>
					</div>
					<div class="infobat-form form">
						<span class="form-title-container">
							<span class="form-title">Informations du BAT</span>
							<div class="collapse-form-arrow">
								<span class="arrow-branch"></span><span class="arrow-branch"></span>
							</div>
						</span>
						<div class="form-collapse">
							<?php
								$infosBat = $bat->getInfosBat();
								$infobat['forSection'] = 'infobat';

								echo buildEditableForm($infobat, json_decode($infosBat->fields));
							?>
						</div>
					</div>
					<div class="infobat-form form">
						<span class="form-title-container">
							<span class="form-title">Informations des logos</span>
							<div class="collapse-form-arrow">
								<span class="arrow-branch"></span><span class="arrow-branch"></span>
							</div>
						</span>
						<div class="form-collapse">
							<?php
								$logos = $bat->getLogos();
								$infologo['forSection'] = 'infologo';

								foreach ($logos['zones'] as $zone => $values) {
									$valueFields = new stdClass();

									foreach ($values as $key => &$value) {
										if ($key == 'ref' && isset($values->spref)) {
											$order = $bat->getOrderByRef($values->spref);
											$p_attribute = json_decode($order->product_attribute, true);

											if (isset($p_attribute['33']))
												foreach ($p_attribute['33'] as $ref)
													$value = $ref['comment'];

											if (empty($value))
												$value = $values->spref;
										}


										if (isset($infologo[$key])) {
											$valueFields->$key = new stdClass();
											$valueFields->$key->value = $value;


											if (strpos($key, "hidden") === 0) {
												$pr = substr($key, 7);
												$valueFields->$pr->hidden = $values->$key;
											}
										}
									}

									$infologo['forContainer'] = $zone;
									echo buildEditableForm($infologo, $valueFields);
								}
							?>
						</div>
					</div>
					<input type="hidden" name="i_b" value="<?= $idbat ?>">
				</form>
			</div>
			<div class="panel-footer">
				<button type="submit" class="spbtnblue" name="update">Sauvegarder</button>
			</div>
		</div>
	</div>
	<div id="bat-menu" class="bat-menu panel panel-standalone">
		<div class="panel-opener"><span class="material-icons">more_vert</span></div>
		<div class="panel-content">
			<div class="flex-content">
				<div class="flex-content-item">
					<button class="bat-menu-item" panel="edit-bat">
						<img src="/src/style/images/document.svg" class="sp-icon">
						<span>Modifier les infos</span>
					</button>
				</div>
				<div class="flex-content-item">
					<a class="bat-menu-item" download="<?= preg_replace('/\s+/', '_', trim($infos->nom)) ?>_BAT.pdf" href="<?php echo $bat->maquetor->sortiePDF().'?'.time() ?>">
						<img src="/src/style/images/pdf.svg" class="sp-icon">
						<span>Télécharger</span>
					</a>
				</div>
			</div>
		</div>
	</div>

	<div class="preview_tolbar">
		<button alt="Zoom avant" class="zoom_bat" zoom='avant'><i class="material-icons">zoom_in</i></button>
		<span>100%</span>
		<button alt="Zoom arrière" class="zoom_bat" zoom='arrière'><i class="material-icons">zoom_out</i></button>
		<!-- <button alt="Page précédente" class="move_page" page='moins'><i class="material-icons">arrow_drop_up</i></button> -->
		<!-- <button alt="Page suivante" class="move_page" page='plus'><i class="material-icons">arrow_drop_down</i></button> -->
		<button alt="Plein écran" id="fullscreen-button"><i class="material-icons">fullscreen</i></button>
		<button alt="Quitter le plein écran" id="exit-fullscreen-button" style="display:none"> <i class="material-icons">fullscreen_exit</i></button>
	</div>
</div>

<?php // Gestion form edit ?>
<script>
	function replaceClass(elmt, classToRemove, classToAdd) {
		if (typeof elmt == 'string') elmt = $(elmt);

		elmt.removeClass(classToRemove);
		elmt.addClass(classToAdd);
	}

	function getEditableTarget(elmt) {
		let field = ($(elmt).is('.form-group')) ? $(elmt).data('field') : elmt.id;
		let forContainer = $(elmt).parents('.form-container').attr('data-container');
		let forSection = $(elmt).parents('.form-container').attr('data-section');
		let target = '';

		for (let [attr, forWhat] of new Map([['section', forSection], ['container', forContainer]]).entries()) {
			if (forWhat !== undefined) {
				if (forWhat.indexOf(' ') !== -1)
					forWhat = forWhat.replace(' ', '+');

				target += `[${attr}="${forWhat}"] `;
			}
		}

		target = $(`${target}[sp-edit="${field}"]`);

		return target;
	}

	$(function() {
		$('.spSelect').spSelect();

		$('.panel-opener').on('click', function(e) {
			let panel = $(this).closest('.panel');

			if (panel.hasClass('panel-opened'))
				closePanel(panel, e);
			else
				openPanel(panel, e);
		});

		$('form .form-group, form .form-group input, form .form-group textarea').on('mouseenter mouseleave focusin focusout', function(e) {
			var target = getEditableTarget(this);

			if ($(this).is('.form-group')) {
				if (e.type == 'mouseenter')
					target.removeClass('persist').addClass('active');
				else if (e.type == 'mouseleave')
					target.removeClass('active');
			} else {
				if (e.type == 'focusin')
					target.removeClass('active').addClass('persist');
				else if (e.type == 'focusout')
					target.removeClass('persist active');
			}
		});

		$(document).on('keyup change', '.form input[type="text"], .form textarea, .form select', function() {
			var target = getEditableTarget(this);

			if (target) {
				let value = this.value;

				if ($(this).is('select'))
					value = $(this).find(`option[value="${value}"]`).text();

				target.html(value.replace(/(\r\n|\r|\n)/g, '<br>'));
			}
		});

		$(document).on('change', 'label.hiddable input[type="checkbox"]', function() {
			var target = getEditableTarget($(this).closest('.form-group'));
			if (target.attr('sp-contained') !== undefined) {
				let targetName = target.attr('sp-edit');
				target = target.closest(`.${targetName}_container`);
			}

			if (target) {
				if (this.checked)
					target.attr('hidden-element', '');
				else
					target.removeAttr('hidden-element');
			}
		});

		$('.form label.hiddable').on('click', function(e) {
			if ($(this).hasClass('visible')) {
				replaceClass($(this), 'visible', 'hidden');
			} else if ($(this).hasClass('hidden')) {
				replaceClass($(this), 'hidden', 'visible');
			}
		});

		$(document).on('submit', '#edit-bat form', function(e) {
			e.preventDefault();
		});

		$(document).on('click', '[name="update"]', function() {
			$('#edit-bat').prepend($('<div class="loading"><div>'));
			let button_text = $(this).text();
			var that = this;

			var formData = new FormData(document.querySelector('#edit-bat form'));
			$(this).text('Sauvegarde en cours');

			$.ajax({
				type: "post",
				url: "",
				data: formData,
				processData: false,
				contentType: false
			})
			.done(function() {
				spAlert('Les modifications ont été sauvegardées', {type: 'success'});
				$('#edit-bat .loading').remove();
				$(that).text(button_text);
				document.location.reload();
			});
		});

		$(document).on('click', '.alert-container .close-alert', function() {
			$(this).closest('.alert-container').animate({
				opacity: 0,
				top: '10px'
			}, 200, function() {
				$(this).remove();
			});
		});

		//? Remove l'input pour le type color

		$('.form input[type="colors"][static="true"]').each(function() {
			$('.form input[type="colors"][static="true"]').attr('type', 'hidden');
		});

		$('.form input[type="file"]').each(function() {
			var input = $(this);
			var wrapper = $(
				'<div class="main-wrapper">' +
				'<div class="filewrapper wrapped-' + input.attr('id') + '">' +
					'<img src="' + input.data('src') + '">' +
				'</div>' +
				'</div>'
			);

			if (input.attr('static') == 'false') {
				var img = wrapper.find('img');
				img.attr('src', (img.attr('src') + '?' + $.now()));

				$('<div class="overlay">' +
					'<p class="overlay-text">Télécharger une photo</p>' +
				'</div>').insertAfter(img);

				if (input.attr('uped') == 'uped')
				wrapper.append($('<div class="actions"><span class="logo-action edit">Modifier</span><span class="logo-action remove">Supprimer</span></div>'));
			} else
				wrapper.addClass('static');

			input.addClass('gtfo').attr('accept', '.png, .jpg, .jpeg');
			input.after(wrapper);

			if (input.attr('static') == 'true') {
				input.prev('input[type="hidden"]').remove();
				input.remove();
			}
		});

		$(document).on('click', '.filewrapper', function(e) {
			e.stopPropagation();
			$(this).closest('.form-group').find('input[type="file"]').trigger('click');
		});

		<?php
			//! A verifier plus tard quand on aura besoin de changer la source d'un logo
		?>
		// $('input[type="file"]').on('change', function(e) {
		// 	var that = $(this);
		// 	var formContainer = $(this).parents('.form-animate');
		// 	var file = $(this)[0].files[0];
		// 	var formData = new FormData();

		// 	formData.append('file', file);
		// 	formData.append('field_name', that.attr('name'));
		// 	formData.append('i_b', $('input[type="hidden"][name="i_b"]').val());
		// 	formContainer.find('form .loader').fadeIn(200);

		// 	$.ajax({
		// 		url: '/components/com_bat/views/edition/tmpl/uploadImg.php',
		// 		method: 'POST',
		// 		contentType: false,
		// 		processData: false,
		// 		data: formData,
		// 		dataType: 'json',
		// 		success: function(data) {
		// 			if (data.response == 'success') {
		// 				that.after($('<input type=hidden name="' + that.attr('name') + '" value="' + data.dir + '">'));
		// 				formContainer.find('form').append($('<input type=hidden name="trig">'));
		// 				formContainer.find('form button[type="submit"][name="update"]').trigger('click');
		// 			}
		// 		},
		// 		error: function(jqXHR, textStatus, errorThrown) {
		// 			console.log('ERRORS: ' + errorThrown);
		// 		}
		// 	});
		// });

		$(document).on('click', '.form-title-container', function() {
			let arrow = $(this).find('.collapse-form-arrow');

			if (arrow.hasClass('active')) {
				arrow.removeClass('active');
				arrow.closest('.form').find('.form-collapse').slideUp(200);
			}
			else {
				arrow.addClass('active');
				arrow.closest('.form').find('.form-collapse').slideDown(200);
			}
		});
	})
</script>

<?php //Envoie de mail ?>
<script>
	$(function() {
		$('#sendMailAsk .popin-body button').on('click', function(e) {
			e.preventDefault();

			if ($(this).hasClass('sendToClient')) {
				closePopIn($(this).closest('.popin'));
				openPopIn($('#sendMail'), e);
			} else {
				$('#sendMailAsk .popin-container').append($('<div class="loading"></div>'));

				$.ajax({
					url: '/scripts/sendBAT.php',
					method: 'POST',
					data: {
						task: 'mail',
						subject: "Votre BAT SuperPictor",
						content: "Bonjour,\n\nVeuillez trouver ci-joint votre BAT SuperPictor.\n\nCordialement.",
						idc: $('#sendMailAsk input[name="idc"]').val(),
						idb: $('#sendMailAsk input[name="idb"]').val()
					},
					dataType: 'text',
					success: function(data) {
						if (data == 'true')
							spAlert('Le mail a bien été envoyé', {type: 'success'});
						else
							spAlert("Une erreur est survenue, veuillez réessayer. Si l'erreur persiste, veuillez nous contacter.", {type: 'error'});
					},
					error: function(jqXHR, textStatus, errorThrown) {
						spAlert("Une erreur est survenue, veuillez réessayer. Si l'erreur persiste, veuillez nous contacter.", {type: 'error'});
					},
					complete: function() {
						$('#sendMailAsk .loading').remove();
						closePopIn($('#sendMailAsk'));
					}
				});
			}
		});

		$('#sendMail form button[submit]').on('click', function(e) {
			e.preventDefault();
			var values = [];
			var formCompleted = true;

			values['mailToWho']  = $('#sendMail form input#mailToWho').val();
			values['mailSubject'] = $('#sendMail form input#mailSubject').val();
			values['mailContent'] = $('#sendMail form textarea#mailContent').val();

			for (var key in values) {
				if ($.trim(values[key]).length == 0) {
					formCompleted = false;
					$('#sendMail form #' + key).addClass('iSaidRequired');
				}
			}

			if (!formCompleted)
				$('#sendMail form p.message')
					.addClass('warning')
					.text("Vous devez remplir tous les champs")
					.fadeIn()
				;
			else {
				$('#sendMail .popin-container').append($('<div class="loading"></div>'));

				$.ajax({
					url: '/scripts/sendBAT.php',
					method: 'POST',
					data: {
						task: 'mail',
						who: values['mailToWho'],
						subject: values['mailSubject'],
						content: values['mailContent'],
						idc: $('#sendMail form input[name="idc"]').val(),
						idb: $('#sendMail form input[name="idb"]').val()
					},
					dataType: 'text',
					success: function(data) {
						if (data == 'true')
							spAlert('Le mail a bien été envoyé', {type: 'success'});
						else
							spAlert("Une erreur est survenue, veuillez réessayer. Si l'erreur persiste, veuillez nous contacter.", {type: 'error'});
					},
					error: function(jqXHR, textStatus, errorThrown) {
						spAlert("Une erreur est survenue, veuillez réessayer. Si l'erreur persiste, veuillez nous contacter.", {type: 'error'});
					},
					complete: function() {
						$('#sendMail .loading').remove();
						closePopIn($('#sendMail'));
					}
				});
			}
		});

		$('#sendMail form input:not([type="hidden"]), #sendMail form textarea').on('input', function() {
			if ($(this).hasClass('iSaidRequired'))
				$(this).removeClass('iSaidRequired');
		});

		window.addEventListener('message', function(e) {
			var data = JSON.parse(e.data);

			if (data.identifier !== undefined) {
				let buttons = $('#clientPopin .popin-body .button-group');

				buttons.css('visibility', 'visible').fadeTo(200, 1);
				buttons.find('[name="client[identifier]"]').val(data.identifier);
			}
		});
	});
</script>

<script>
	var pageactive = <?php echo $numpage ?>;
	var pagemax = <?php echo $bat->maquetor->nbpages ?>;
	if (pageactive == pagemax){
		$(".move_page[page='plus'] i").css({opacity:'0.4',cursor:'default'});
	}
	else if (pageactive == 1){
		$(".move_page[page='moins'] i").css({opacity:'0.4',cursor:'default'});
	}

	var fullscreenButton = document.getElementById("fullscreen-button");
	var exitfullscreenButton = document.getElementById("exit-fullscreen-button");
	var fullscreenDiv    = document.getElementById("fullscreen");
	var fullscreenFunc   = fullscreenDiv.requestFullscreen;
	//var exitfullscreenFunc   = fullscreenDiv.exitFullscreen;
	if (!fullscreenFunc) {
		['mozRequestFullScreen',
		'msRequestFullscreen',
		'webkitRequestFullScreen'].forEach(function (req) {
			fullscreenFunc = fullscreenFunc || fullscreenDiv[req];
		});
	}

	function enterFullscreen() {
		fullscreenFunc.call(fullscreenDiv);
	}

	fullscreenButton.addEventListener('click', enterFullscreen);

	$('#fullscreen-button').click(function(event) {
		$(this).hide();
		$('#exit-fullscreen-button').show();
	});

	$('#exit-fullscreen-button').click(function(event) {
		$(this).hide();
		$('#fullscreen-button').show();
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.webkitCancelFullScreen) {
			document.webkitCancelFullScreen();
		}
	});

	$('.zoom_bat').click(function(event) {
		var elmt = $('.rendu_template');
		if (!elmt.length)
			elmt = $('.page');

		var scaleini = elmt.css("transform");
		var widthPreview;
		var heightPreview;

		if (scaleini == 'none' || scaleini == 'undefinded')
			scaleini = 1;
		else{
			scaleini = elmt.css("transform").split(",");
			scaleini = parseFloat(scaleini[3]);
		}
		if (scaleini == 0.55)
			$(".zoom_bat[zoom='arrière'] i").css({opacity:'0.4',cursor:'default'});
		else
			$(".zoom_bat[zoom='arrière'] i").css({opacity:'1',cursor:'pointer'});

		if (scaleini == 2.05)
			$(".zoom_bat[zoom='avant'] i").css({opacity:'0.4',cursor:'default'});
		else
			$(".zoom_bat[zoom='avant'] i").css({opacity:'1',cursor:'pointer'});

		var boundingRect = elmt.get(0).getBoundingClientRect();
		if ($(this).attr('zoom') =='avant'){
			if (scaleini != 2.05 ){
				widthPreview = boundingRect.width;
				heightPreview = boundingRect.height;
				var diffHeightMarg = ((heightPreview - elmt.height()) / 2);
				diffHeightMarg += 100;
				elmt.css("transform","scale("+(parseFloat(scaleini+0.15))+")");
				elmt.css('margin-top', +diffHeightMarg+'px');
				elmt.css('margin-bottom', +diffHeightMarg+'px');
				var zoomPercent = Math.round((scaleini+0.15)*100);
				$(".preview_tolbar span").text(zoomPercent+"%");
				if (widthPreview >= 990 || heightPreview >= 1200) {
					$('#fullscreen').css({flexDirection : 'column'});
				}
			}
		}
		else if ($(this).attr('zoom') =='arrière'){
			if (scaleini != 0.55 ){
				widthPreview = boundingRect.width;
				heightPreview = boundingRect.height;
				var diffHeightMarg = ((heightPreview - elmt.height()) / 2);
				diffHeightMarg -= 50;
				elmt.css("transform","scale("+(parseFloat(scaleini-0.15))+")");
				elmt.css('margin-top', +diffHeightMarg+'px');
				elmt.css('margin-bottom', +diffHeightMarg+'px');
				var zoomPercent = Math.round((scaleini-0.15)*100);
				$(".preview_tolbar span").text(zoomPercent+"%");
				if (widthPreview < 990 || heightPreview < 1200) {
					$('#fullscreen').css({flexDirection : 'row'});
				}
			}
		}
	});

	// if (document.addEventListener)
	// {
	//     document.addEventListener('webkitfullscreenchange', exitHandler, false);
	//     document.addEventListener('mozfullscreenchange', exitHandler, false);
	//     document.addEventListener('fullscreenchange', exitHandler, false);
	//     document.addEventListener('MSFullscreenChange', exitHandler, false);
	// }

	// function exitHandler()
	// {
	//     if (document.webkitIsFullScreen || document.mozFullScreen || document.msFullscreenElement !== null)
	//     {
	//         $('.rendu_template').css("transform","scale(1)")
	//     }
	//     else{
	//     	$('.rendu_template').css("transform","scale(1.5)")
	//     }
	// }

	$('.move_page').click(function(event) {
		if ($(this).attr('page') =='plus'){
			if (pageactive != pagemax)
				window.location.href = '/designer/preview.php?idbat=<?php echo $idbatCrypt?>&no='+(parseFloat(<?php echo intval($numpage) ?>)+1);
		}
		else if ($(this).attr('page') =='moins'){
			if (pageactive != 1)
				window.location.href = '/designer/preview.php?idbat=<?php echo $idbatCrypt?>&no='+(parseFloat(<?php echo intval($numpage) ?>)-1);
		}
	});

	//? Bouton Previous
	<?php $id = $smodel->encrypt($bat->designed->rowid, true); ?>
	$('#previous').on('click', function() {
		var url_string = window.location.href; //window.location.href
		var url = new URL(url_string);
		window.location.assign("./superpictor.html?view=mockup&id=<?= $id; ?>");
	});
</script>

</body>
</html>
