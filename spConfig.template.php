<?php

abstract class spConfig {

    static private $sp_HOST   = "mysqldb";
    static private $sp_PORT   = "3306";
    static private $sp_DBNAME = "bat_sandbox";
    static private $sp_USER   = "root";
    static private $sp_PWD    = "bat_sandbox";

    static public function getHost($base_code)      {return self::${$base_code . '_HOST'};  }
    static public function getDbname($base_code)    {return self::${$base_code . '_DBNAME'};}
    static public function getPort($base_code)      {return self::${$base_code . '_PORT'};  }
    static public function getUser($base_code)      {return self::${$base_code . '_USER'};  }
    static public function getPwd($base_code)       {return self::${$base_code . '_PWD'};   }
}

