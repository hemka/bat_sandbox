<?php

require_once ("SuperSource.php");

class SuperSourceBat extends SuperSource {

    public function __construct($source, $source_id) {
        parent::__construct($source, $source_id);
   
        $sql = "SELECT * FROM `1_gestion_bat` as b
                JOIN `1_designed` as d ON b.designed =  d.rowid
                WHERE b.rowid = ".intval($source_id);
        $mission = $this->select($sql);
       
        // Common attributes
        $this->ref          = $mission->reference;
        $this->name         = $mission->nom;
        $this->link_single  = "/bat.html?view=single&idbat=".$this->DanXEncrypt($source_id)."";
        $this->link_single_parent  = "/superpictor.html?view=mockup&id=".$this->encrypt($mission->designed, true);
        $this->link_single  = $this->link_single_parent; // ? temporaire en attente de routing complet
        $this->link_title_parent  = "Mockup";
        $this->created_on   = DateTime::createFromFormat('Y-m-d H:i:s', $mission->date_creation);
        $this->status_code  = $mission->statut;
        // $this->status       = $this->getHtmlStatus($this->status_code, "label");
        // $this->status_class = $this->getHtmlStatus($this->status_code, "class");
        $this->status       = "";
        $this->status_class = "";
        $this->type         = "BAT";
        $design             = json_decode($mission->zonesjson);
        $this->file_url     = $design->views[0];
        $this->file_url_absolute     = (strpos(  $this->file_url, "http") !== false) ? $this->file_url : "https://www.superpictor.com/designer/".$this->file_url;
        $this->getOwnerInfo($mission->idclient);
        $this->admin_infos[] = "Revendeur:<br> <span>" . $this->client . "</span>";
        $this->setUser($mission->idclient);
    }


    
    public function getHtmlStatus($statuscode, $type = "label") {
        if (intval($statuscode) == 0)
            return ($type == "label") ? "Brouillon" : "warngray" ;
        if (intval($statuscode) == 1)
            return ($type == "label") ? "Enregistré" : "warnblue" ;
        if (intval($statuscode) == 2)
            return ($type == "label") ? "Validé" : "warngreen" ;
        if (intval($statuscode) == 10)
            return ($type == "label") ? "Supprimé" : "warnred" ;
        return "Aucun";
      }
}
