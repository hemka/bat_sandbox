<?php

abstract class ProductModel {
    protected $code;
    protected $parent;
    protected $name;
    protected $default_picture;
    protected $published;

    protected function __construct($code, $parent, $name, $default_picture, $published) {
        $this->code      = $code;
        $this->parent    = $parent;
        $this->name      = $name;
        $this->default_picture  = $default_picture;
        $this->published = $published;
    }

    public abstract function getCode();
    public abstract function getName();
    public abstract function getParent();
    public abstract function getDefaultPicture();
}
?>