<?php 
if(!class_exists('ProductModel'))
    require_once 'ProductModel.php';

if(!class_exists('SuperProduct'))
    require_once 'SuperProduct.php';

if(!class_exists('SuperFinder'))
    require_once 'SuperFinder.php';

class SuperChild extends ProductModel {
    //? Specific attributes
    public $finder;
    public $size;
    public $color;

    //? Relational attributes
    public $prices;
    public function __construct($code) {
        if(!$code) return null;

        $this->finder = new SuperFinder();
        $infos = $this->finder->getChildInfos($code);

        // Set de toutes les images
        $pictures = [];
        $pics_cols = ['veg_nom_pic_face','veg_nom_pic_dos','veg_nom_pic_cote','veg_pic_packshot_4','veg_pic_packshot_5','veg_pic_packshot_6'];
        foreach($pics_cols as $pics)
            if(!empty($infos->{$pics}))
                $pictures[$pics] = $infos->{$pics};

        $parent = new SuperProduct($infos->parent, false);
        parent::__construct($infos->code, $parent, $infos->name, $pictures, 1);

        $this->size = $infos->veg_taille;

        $this->color = $infos->veg_couleur;
        $this->size = $infos->veg_taille;

        // Set des infos distrib
        $size_array = array();
        $infos_distrib = $this->finder->getPdtDistribInfos($this->code);
        $distrib_cols = ['PAHT_2020-Crafters-EUR', 'veg_SKU_distributeur', 'veg_distributeur'];
        foreach($infos_distrib as $distrib) {
            $temp = array();

            foreach($distrib_cols as $cols)
                $temp[$cols] = $distrib->{$cols};

            array_push($size_array, $temp);

            $this->prices = $size_array;
        }
    }

    public function getCode()     { return $this->code; }
    public function getName()     { return $this->name; }
    public function getParent()   { return $this->parent; }
    public function getDefaultPicture() { return $this->pictures; }
}
?>