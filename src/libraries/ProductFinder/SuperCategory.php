<?php 
if(!class_exists('SuperFinder'))
    require_once 'SuperFinder.php';

class SuperCategory extends SuperClass {

    private $id;
    private $id_parent;
    private $code;
    private $search_handler;
    private $date;
    private $ordering;

    public function __construct($code = NULL)
    {
        if(!empty($code)){
            $this->code = $code;
            $this->finder = new SuperFinder();
            $infos = $this->finder->getCategoryInfo($this->code);
            foreach($infos as $key => $val)
                if(property_exists($this, $key))
                    $this->{$key} = $val;
            return;
        }     
    }

    public function getId(){ return $this->id; }
    public function getIdParent(){ return $this->id_parent; }
    public function getCode(){ return $this->code; }
    public function getSearchHandler(){ return $this->search_handler; }
    public function getDate(){ return $this->date; }
    public function getOrdering(){ return $this->ordering; }

    public function addCategory($id_parent, $name, $jsonRepresentation, $ordering){
        $this->autoAuth('products');
        $code = str_replace(['"', '"', ",", '-', ' '], '_', strtolower($name));
        $id_parent = ($id_parent != 0 ? $id_parent : "NULL");
        var_dump($id_parent);
        $rqt = "INSERT INTO `products_db`.`PF_categories` (id_parent, code, search_handler, ordering, `label-fr_FR`) VALUES ($id_parent, '$code', '$jsonRepresentation', '$ordering', '$name')";
        $this->execute($rqt);
        return $this->lastInsertId();
    }
        
}
?>