<?php 
const LS_ALL     = 32767;
const LS_PERSO   = 16;
const LS_PUBLIC  = 8;
const LS_MINE    = 6;
const LS_CATALOG = 4;
const LS_QUOTE   = 2;
const LS_SHARED  = 1;

if(!class_exists('SuperClass'))
    require_once $_SERVER['DOCUMENT_ROOT'].'/libraries/SuperClass.php';

if(!class_exists('SuperClient'))
    require_once $_SERVER['DOCUMENT_ROOT'].'/libraries/SuperClient.php';

if(!class_exists('SuperProduct'))
    require_once $_SERVER['DOCUMENT_ROOT'].'/src/libraries/ProductFinder/SuperProduct.php';

if(!class_exists('SuperList'))
    require_once $_SERVER['DOCUMENT_ROOT'].'/src/libraries/ProductFinder/SuperList.php';   

if(!class_exists('SuperCategory'))
    require_once $_SERVER['DOCUMENT_ROOT'].'/src/libraries/ProductFinder/SuperCategory.php';
    
class SuperFinder extends SuperClass {

    public function __construct($base = 'products') { $this->autoAuth($base); }

    public function getProductInfos($code, $with_children = true, $allInfos = true, $default_color = []) {
        $parent = $this->select("SELECT * FROM `superpicache` WHERE code = '$code' AND (parent IS NULL OR parent = '');");
        
        if(empty($parent)) {
            $code_parent = $this->select("SELECT parent FROM `superpicache` WHERE code = '$code'");
            $code = $code_parent->parent;
        }

        $infos = $this->select("SELECT *, `veg_product_description-fr_fr` as description FROM `superpicache` WHERE code = '$code' AND (parent = '' OR parent IS NULL)");
        
        $infos->default_picture = false;
        $infos->colors_pictures = [];

        $colors_pictures = $this->selectAll("SELECT veg_couleur, color_groups, veg_couleur, veg_nom_pic_face as pic0, veg_nom_pic_dos as pic1, veg_nom_pic_cote as pic2 FROM `superpicache` WHERE parent = ".$this->quote($infos->code)." AND veg_nom_pic_face != '' GROUP BY veg_couleur ORDER BY veg_nom_pic_face DESC");
        if (!empty($colors_pictures)) {

            $infos->default_picture = $colors_pictures[0]->pic0;

            foreach ($colors_pictures as $color_picture) {
                $cond = (!empty($default_color) && array_search($color_picture->color_groups, $default_color) !== false) || (!empty($default_color) && array_search($color_picture->veg_couleur, $default_color) !== false);
                if ($cond) $infos->default_picture = $color_picture->pic0;
                $infos->colors_pictures[$color_picture->veg_couleur] = [];

                for($i=0; $i < count((array)$color_picture)-1; $i++) {
                    if(!empty($color_picture->{'pic' . $i}))
                        $infos->colors_pictures[$color_picture->veg_couleur][] = $color_picture->{'pic' . $i};
                }
            }
        }
        // Get children infos
        if($with_children) {
            $children = $this->selectALL("SELECT code FROM `superpicache` WHERE parent = ".$this->quote($infos->code));
            if($children)
                $infos->children = $children;
        }

        if($allInfos) {
            $infos->matrice_colors_size_distrib = $this->getMatriceColorSizeDistrib($code);
            $infos->distribs = $this->getDataByProduct('distrib', $code);
            $infos->colors   = $this->getDataByProduct('color', $code);
            $infos->sizes    = $this->getDataByProduct('size', $code);
        }

        return $infos;
    }


    public function findDistribInMatrice($matrice, $color, $size){
        return (isset($matrice['color']['size']) ? $matrice['color']['size'] : false);
    }

    public function getMatriceColorSizeDistrib($code)
    {   
        $children = $this->selectALL("SELECT veg_couleur as color, veg_nom_pic_face as image, veg_brand as brand FROM `superpicache` WHERE parent = '$code' AND NOT parent IS NULL AND NOT parent = '';");
        
        $arrayC= [];
        foreach($children as $v){
            $arrayC[$v->color] = new stdClass();
            $arrayC[$v->color]->code = $v->color;
            $arrayC[$v->color]->name = $v->color;// TRANSLATE CODE
            $arrayC[$v->color]->image = $v->image;
            $arrayC[$v->color]->brand = $v->brand;

            $sizes = $this->selectALL("SELECT veg_taille as size, veg_brand as brand FROM `superpicache` WHERE veg_couleur = '$v->color' AND parent = '$code' AND NOT parent IS NULL AND NOT parent = '';");
            $arrayC[$v->color]->sizes = [];
            foreach ($sizes as $size) {
                $size = $size->size !== '' ? $size->size : 'MA_UNIQUE';

                $o = new stdClass;
                $o->code = $size;
                $o->name = $size;

                $distribs = $this->select("SELECT veg_distributeurs as distrib FROM `superpicache` WHERE veg_couleur = '$v->color' AND veg_taille = '$size' AND parent = '$code' AND NOT parent IS NULL AND NOT parent = '';");
                if($distribs !== false) {
                    $distribs = explode(',',$distribs->distrib);

                    foreach ($distribs as &$d) 
                        $d = new SuperDistributor($d);

                    $o->distribs = $distribs;
                } else
                    $o->distribs = [];
                    
                $arrayC[$v->color]->sizes[$size] = $o;
            }

        }
        return $arrayC;
    }

    public function getDataByProduct($dataWanted, $code){
        if(!class_exists('SuperLang'))
            require_once $_SERVER['DOCUMENT_ROOT'].'/libraries/SuperLang.php';

        $lang = new SuperLang();
        
        switch($dataWanted) {
            case 'color' :
                $text = 'veg_couleur';
                $prefix = 'co_';
                break;

            case 'distrib' :
                $text = 'veg_distributeurs';
                $prefix = 'di_';
                break;

            case 'size' :
                $text = 'veg_taille';
                $prefix = 'sz_';
                break;

            default :
                return null;
                break;
        }
        
        $result = $this->select("SELECT $text FROM `superpicache` WHERE code = '$code' AND (parent = '' OR parent IS NULL);");
        $a = [];

        foreach(array_unique(explode(',',$result->{$text})) as $res) {
            if($res === '')
                return [];
            $a[] = ['code' => $res, 'name' => $lang->l($prefix.$res)];
        }
        return $a;
    }

    public function getChildInfos($code) {
        return $this->select("SELECT * FROM `superpicache` WHERE code = '$code'");
    }

    public function getPdtDistribInfos($code) {
        return $this->selectALL("SELECT * FROM `00_PIM_distrib` WHERE parent = '$code'");
    }

    public function getDistribInfos($code) {
        return $this->select("SELECT * FROM `distribs` WHERE code = '$code'");
    }

    public function getCatInfos($code) {
        return $this->select("SELECT * FROM `MA_virtuemart_categories_fr_fr` WHERE virtuemart_category_id = $code");
    }

    public function getOption($code, $attribute, $lang = "fr_FR") {
        $ret = $this->select(sprintf("SELECT * FROM `00_PIM_option` WHERE code = %s AND attribute = %s", $this->quote($code), $this->quote($attribute)));
        return (!empty($ret) && isset($ret->{"label-".$lang})) ? $ret->{"label-".$lang} : false;
    }

    public function getCategories() {
		return $this->selectAll("SELECT * FROM PF_categories");
	}

    public function getCategory($id_parent = NULL) {
		return $this->selectAll("SELECT id,code FROM `PF_categories` WHERE id_parent ". (( $id_parent = intval($id_parent) ) ? "= ".$id_parent : "IS NULL"). " ORDER BY ordering DESC");
	}

    public function getCategoryTree($id = 0, $level = 0){
        foreach($children = $this->getCategory($id) as $child){
            $child->children = $this->getCategoryTree($child->id, $level +1);
            $child->level    = $level;
        }
        return $children;
    }

    public function getCategoryInfo($code) 
    {
        return $this->select("SELECT * FROM `PF_categories` WHERE code = '$code'");
    }


    public function getAttributesCode($search_handler, $verbose = false) {
        $sh_where = $this->buildQuery($search_handler, false, false);
        $sh_where = (!empty($sh_where)) ? "AND $sh_where" : $sh_where;
        $attributes = $this->selectAll("SELECT attributes FROM `superpicache` as cache WHERE parent = '' $sh_where");

        $attrs = array();
        foreach($attributes as $attr) {
            if(!empty($attr->attributes)) {
                $temp = (strpos($attr->attributes, ',') !== false) ? explode(',', $attr->attributes) : [$attr->attributes];
                foreach($temp as $val)
                    if(!empty($val))
                        array_push($attrs, $val);
            }
        }
        $attrs = implode(',', array_unique($attrs));
        $where = (!empty($attrs)) ? "WHERE importance != -1 AND value_code IN ($attrs)" : "WHERE importance != -1";

        if($verbose) {
            var_dump("SELECT attributes FROM `superpicache` WHERE parent = '' $sh_where");
            var_dump("SELECT property_code, GROUP_CONCAT(value_code) as values_code FROM `PF_attributes` $where GROUP BY property_code");
            var_dump((!empty($sh_where) && empty($where)) ? "WILL return NULL" : "WILL return SOMETHING");    
        }

        if(!empty($sh_where) && empty($where))
            return null;

        $result = $this->selectAll(
            "SELECT property_code, GROUP_CONCAT(value_code) as values_code
            FROM `PF_attributes` $where GROUP BY property_code"
        );
        
        return $result;
    }

    public function createOrdering(SearchHandler $search_handler) {
        $col = "";
        $type = "";
        $order_type = explode('_', $search_handler->getOrderBy()[0]);

        switch($order_type[0]) {
            case "alp" :
                $col = "name";
                break;
            case "brd" :
                $col = "veg_brand";
                break;
            default:
                $col = "boconfig_id ".(!empty($search_handler->getUser()) ? 'DESC, mu.favorite DESC, du.favorite' : '');
                $order_type = ['', 'd'];
                if(!empty($search_handler->getUser())){
                    $userid = $search_handler->getUser();
                    $this->query_jointure = "LEFT JOIN `digitizingBD`.`0_marques_users` mu ON mu.marque_code = cache.`veg_brand` AND mu.user_id = ".$this->quote($userid)." LEFT JOIN `digitizingBD`.`0_distributeurs_users` du ON du.distrib_code = cache.`veg_distributeurs` AND du.user_id = ".$this->quote($userid);
                }
                break;
        }

        if(!empty($order_type[1]))
            $type = ($order_type[1] == 'd') ? " DESC" : " ASC" ;

        return $col.$type;
    }

    //! A Refactor - Clément
    public function buildQuery(SearchHandler $search_handler, $dump = false, $force = true, $legacy = false) {
        if (!$force && !empty($this->buildQuery)) return $this->buildQuery;

        $cols = ['cache.`code`', 'cache.`name`', '`veg_brand`', '`veg_product_description-fr_fr`', '`veg_couleur`', '`sku_parent_marque`'];
        $list = ['sku_enfant_marque', 'veg_SKU_distributeurs'];
        $clauses = array(" 1 "); // ? ajout de 1 car pas de verification sur les querys derriere - Dan

        // Create Where clause for alias
        if(!$legacy) {
            if(!empty($search_handler->getAliasGroup())) {
                $tmp_clauses = array();
                foreach($search_handler->getAliasGroup() as $alias)
                    array_push($tmp_clauses, "FIND_IN_SET('$alias', code_alias)");

                array_push($clauses, $tmp_clauses);
            }
        }

        // Create Where clause for keywords
        $keywords_func = ($legacy) ? ['key' => 'getKeywords', 'as' => 'getAliasGroup'] : ['key' => 'getKeywords'];
        foreach($keywords_func as $key => $func) {
            if(!empty($search_handler->{$func}(true))) {
                $tmp_clauses = array();
                if($key === 'as') {
                    $words = [];
                    foreach($search_handler->{$func}(true) as $code)
                        $words = array_merge($words, $this->getAllAliasByCode($code)); 

                } else
                    $words = $search_handler->{$func}(true);
                    
                foreach ($words as $wrd) {
                    $col_clauses = array();
                    $lst_clauses = array();
                    if(!empty($wrd)) {
                        foreach($cols as $val)
                            array_push($col_clauses, "$val LIKE '%$wrd%'");

                        foreach($list as $val)
                            array_push($lst_clauses, "FIND_IN_SET('$wrd', $val)");
                    }

                    $key_clauses = array_merge($col_clauses, $lst_clauses);
                    if($key_clauses != [])
                        array_push($tmp_clauses, '('.implode(' OR ', $key_clauses).')');
                }
                
                $operator = ($key === 'as') ? 'OR' : 'AND';
                if($tmp_clauses)
                    array_push($clauses, '('.implode(" $operator ", $tmp_clauses).')');
            }
        }

        // Create Where clause for configurable
        if($search_handler->getIsConfigurable()) {
            $operator = ($search_handler->getIsConfigurable() == 2) ? 'IS' : 'IS NOT';
            array_push($clauses, "boconfig_id $operator NULL");
        }

        // Create Where clause for Product Type
        if($search_handler->getTypeProduct())
            array_push($clauses, "type_product IN (".implode(",", $search_handler->getTypeProduct()).")");

        // Create Where clause for brands
        if(!empty($search_handler->getBrands(true))) {
            $brd_clauses = array();
            foreach ($search_handler->getBrands(true) as $brd)
                if(!empty($brd))
                    array_push($brd_clauses, "veg_brand = '$brd'");
            array_push($clauses, "(".implode(' OR ', $brd_clauses).")");
        }

        // Create Where clause for colors / brand_color
        $props = [['func' => 'getColors', 'attr' => 'color_groups'], ['func' => 'getBrandColors', 'attr' => 'veg_couleur']];
        $concat_clauses = array();
        foreach($props as $prop) {
            if(!empty($search_handler->{$prop['func']}(true))) {
                $col_clauses = array();
                foreach($search_handler->{$prop['func']}(true) as $val)
                    if(!empty($val)) {
                        $col = $prop['attr'];
                        array_push($col_clauses, "FIND_IN_SET('$val', `$col`)");
                    }
                $operator = ($prop['attr'] == 'color_groups') ? 'AND' : 'OR';
                array_push($concat_clauses, "(".implode(" $operator ", $col_clauses).")");
            }
        }
        if(!empty($concat_clauses))
            array_push($clauses, "(".implode(' AND ', $concat_clauses).")");

        // Create Where clause for distributors
        if(!empty($search_handler->getDistributors(true))) {
            $dis_clauses = array();
            foreach ($search_handler->getDistributors(true) as $dis)
                if(!empty($dis))
                    array_push($dis_clauses, "FIND_IN_SET('$dis', veg_distributeurs)");
            array_push($clauses, "(".implode(' OR ', $dis_clauses).")");
        }

        // Create Where clause for attributes
        if(!empty($search_handler->getAttributes())) {
            $attr_array   = explode(';', $search_handler->getAttributes());
            $attr_clauses = array();
            foreach($attr_array as $group) {
                $child_attr   = array();
                if(!empty($group)) {
                    $split_group = explode(',', $group);
                    unset($split_group[0]); // Representing Group ID we don't need it
                    foreach($split_group as $attr)
                        array_push($child_attr, "FIND_IN_SET($attr, attributes)");

                    array_push($attr_clauses, "(".implode(' OR ',$child_attr).")");
                }
            }

            array_push($clauses, implode(' AND ', $attr_clauses));
        }

        if (!empty($search_handler->getCatalogs())) {
            $catalogs_array = $search_handler->getCatalogs();
            $catalogs_clauses = array();
            foreach ($catalogs_array as $cat) 
                if (!empty($cat)) 
                    array_push($catalogs_clauses, "code IN (SELECT product_code as code FROM `products_db`.`products_in_list` WHERE list_id = $cat)");
                
            array_push($clauses, "(".implode(' OR ', $catalogs_clauses).")");
        }
        $query = (!empty($clauses)) ? implode(' AND ', $clauses) : "";
        $this->buildQuery = $query;
        return $this->buildQuery;
    }

    public function find(SearchHandler $search_handler, $verbose = false, $force_build = true) {
        $GLOBALS["spec_color"] = (!empty($search_handler->getColors(true))) ? $search_handler->getColors(true) : [];
        $GLOBALS["spec_color"] = array_merge($GLOBALS["spec_color"], $search_handler->getBrandColors(true));

        $where = $this->buildQuery($search_handler, $verbose, $force_build);
        $where = (!empty($where)) ? "AND $where" : $where;
        $order = (!empty($this->createOrdering($search_handler))) ? "ORDER BY veg_nom_pic_face DESC, power DESC, ".$this->createOrdering($search_handler) : "ORDER BY veg_nom_pic_face DESC, power DESC";

        $join = (isset($this->query_jointure) && $this->query_jointure != "" ? $this->query_jointure : '');
        $rqt       = "SELECT * FROM `products_db`.`superpicache` as cache $join WHERE parent = '' $where GROUP BY code $order LIMIT 0, ".$search_handler->getLimit().";";
        $rqt_count = "SELECT COUNT(code) as total FROM `products_db`.`superpicache` as cache $join WHERE parent = '' $where";
        $search_handler->setRequete($rqt);

        $this->autoAuth('full');
        $result = $this->selectAll($rqt);
        $result_count = $this->select($rqt_count);
        if($verbose) print_r($rqt);
        
        $result = array_map(function($e) {
            return new SuperProduct($e->code, false, false, $GLOBALS["spec_color"]);
        }, $result);
        
        $results = new stdClass();
        $results->rows = $result;
        $results->sql = $rqt;
        $results->total = (isset($result_count->total) ? $result_count->total: 0);
        $search_handler->setResult($results);
        $this->autoAuth('products');
    }

    public function getBrands(SearchHandler $sh, $verbose = false) {
        if ($verbose) $mt_s = microtime(true);

        $hidden_temp_brand = $sh->getProps('hidden_brands');
        $temp_brand = $sh->getProps('brands');
        $brands = array_unique(array_merge($temp_brand, $hidden_temp_brand));
        $sh->setBrands(null);
        $initial_condition = $this->buildQuery($sh);

        $sql_brands = "SELECT veg_brand as code, `label-fr_FR` as name, count(*) as total
        FROM `superpicache` as cache 
        INNER JOIN `00_PIM_option` as po ON po.code = cache.veg_brand AND po.attribute = 'veg_brand'
        WHERE cache.parent = '' AND $initial_condition GROUP BY veg_brand ORDER BY name";
        $sql_brands = $this->selectAll($sql_brands);
        $sql_brands_code = array_map(function($element) {return $element->code;}, $sql_brands );

        $selected = "SELECT code, `label-fr_FR` as name
        FROM `00_PIM_option` as po
        WHERE po.code IN ('". implode("','", $brands)."') AND attribute = 'veg_brand'";
        $selected = $this->selectAll($selected);
        $selected_code = array_map(function($element) {return $element->code;}, $selected );

        foreach ($selected_code as $key=>$s) {
            if (array_search($s, $sql_brands_code) === false) {
                $selected[$key]->total = '0';
                array_push($sql_brands, $selected[$key]);
            }
        }

        if ($verbose) {
            echo $sql_brands;
            printf("<br>\n GetBrands Executée en %d <br>\n",  (microtime(true) - $mt_s));
        }

        $sh->setBrands($hidden_temp_brand, true);
        $sh->setBrands($temp_brand);

        return $sql_brands;
    }

    public function getBrandColors(SearchHandler $sh, $verbose = false) {
        $hidden_temp_colors = $sh->getProps('hidden_brand_colors');
        $temp_colors = $sh->getProps('brand_colors');

        $sh->setBrandColors(null, false);

        $sh_where = $this->buildQuery($sh);
        $sh_where = (!empty($sh_where)) ? "AND $sh_where" : $sh_where;
        $colors_cache = $this->selectAll("SELECT `veg_couleur` as colors FROM `superpicache` as cache WHERE parent != '' $sh_where GROUP BY veg_couleur");
        $colors = array();
        foreach($colors_cache as $color) {
            if(!empty($color->colors)) {
                $temp = (strpos($color->colors, ',') !== false) ? explode(',', $color->colors) : [$color->colors];
                foreach($temp as $val)
                    if(!empty($val))
                        array_push($colors, $val);
            }
        }
        $colors = implode("','", array_unique($colors));

        $sh->setBrandColors($hidden_temp_colors, true, false);
        $sh->setBrandColors($temp_colors, false, false);


        $condition = (!empty($colors)) ? "AND code IN ('$colors')" : "AND 0";
        return $this->selectAll("SELECT GROUP_CONCAT(code) as code, `label-fr_FR` as name FROM `00_PIM_option` WHERE `attribute` = 'veg_couleur' AND `label-fr_FR` != '' $condition GROUP BY LOWER(`label-fr_FR`) ORDER BY `label-fr_FR` ASC");
    }

    public function getDistributors($search_handler, $verbose = false) {
        $hidden_temp_distrib = $search_handler->getProps('hidden_distributors');
        $temp_distrib = $search_handler->getProps('distributors');
        $selected_distribs = array_unique(array_merge($hidden_temp_distrib, $temp_distrib));
        $selected_distribs = implode(',', $selected_distribs);

        $search_handler->setDistributors(null);

        $sh_where = $this->buildQuery($search_handler);
        $sh_where = (!empty($sh_where)) ? "AND $sh_where" : $sh_where;
        $distributors = $this->selectAll("SELECT `veg_distributeurs` as distribs FROM `superpicache` as cache WHERE parent = '' $sh_where");

        $distribs = array();
        foreach($distributors as $dsitrib) {
            if(!empty($dsitrib->distribs)) {
                $temp = (strpos($dsitrib->distribs, ',') !== false) ? explode(',', $dsitrib->distribs) : [$dsitrib->distribs];
                foreach($temp as $val)
                    if(!empty($val))
                        array_push($distribs, $val);
            }
        }
        $distribs = implode(',', array_unique($distribs));

        $condition = (!empty($distribs)) ? "WHERE code IN ($distribs)" : "";
        $condition .= ($selected_distribs != '') ? " OR code IN ($selected_distribs)": "";
        $sql_distribs = "SELECT code, name FROM `distribs` $condition ORDER BY name";
        
        if ($verbose) echo $sql_distribs;

        $search_handler->setDistributors($hidden_temp_distrib, true);
        $search_handler->setDistributors($temp_distrib);
        return $this->selectAll($sql_distribs);
    }

    public function getColors($search_handler) {
        $hidden_temp_colors = $search_handler->getProps('hidden_colors');
        $temp_colors = $search_handler->getProps('colors');

        $search_handler->setColors(null);

        $sh_where = $this->buildQuery($search_handler);
        $sh_where = (!empty($sh_where)) ? "AND $sh_where" : $sh_where;
        $colors_cache = $this->selectAll("SELECT `color_groups` as colors FROM `superpicache` as cache WHERE parent = '' $sh_where GROUP BY color_groups");

        $colors = array();
        foreach($colors_cache as $color) {
            if(!empty($color->colors)) {
                $temp = (strpos($color->colors, ',') !== false) ? explode(',', $color->colors) : [$color->colors];
                foreach($temp as $val)
                    if(!empty($val))
                        array_push($colors, $val);
            }
        }
        $colors = implode("','", array_unique($colors));

        $search_handler->setColors($hidden_temp_colors, true);
        $search_handler->setColors($temp_colors);

        $condition = (!empty($colors)) ? "AND Color_Groupe IN ('$colors')" : "";
        return $this->selectAll("SELECT * FROM `PF_colors` WHERE `Color_Groupe` != 'NA' $condition GROUP BY Color_Groupe");
    }

    public function getDesignerDeclinaisonColor($color_code, $color_name) {
        $parent = $this->select("SELECT * FROM `superpicache` WHERE code = '$color_code' AND (parent IS NULL || parent = '');");
        
        if(empty($parent)) {
            $code_parent = $this->select("SELECT parent FROM `superpicache` WHERE code = '$color_code'");
            $color_code = $code_parent->parent;
        }
        $result = $this->select("SELECT id_product_virtuemart as designer_id, boconfig_id FROM `superpicache` WHERE parent = ". $this->quote($color_code) ."  AND veg_couleur = ". $this->quote($color_name) ." GROUP BY veg_couleur");
        return $result;
    }
    public function getAllBrands($indexByLength = false) {
        $results = $this->selectAll("SELECT * FROM `brands`");
        if ($indexByLength)
            return $this->indexArrayByWordsCounts($results, "name_fr");
        return $results;
    }

    public function indexArrayByWordsCounts ($list, $prop) {
        $indexedResult = array();
        foreach($list as $r) {
            if (!isset($indexedResult[$l = sizeof(explode(" ", ($r->{$prop})))]))
                $indexedResult[$l] = array();
            $indexedResult[$l][] = $r;
        }
        return $indexedResult;
    }

    public function getCodeColor($colors_name) {
        $name = strtolower($colors_name);
        $result = $this->selectAll("SELECT code FROM `00_PIM_option` WHERE attribute = 'veg_couleur' AND LOWER(`label-fr_FR`) = '$name'");
        return array_map(function($e) { return $e->code; }, $result);
    }

    public function getAllColors($indexByLength = false) {
        $results = $this->selectAll("SELECT * FROM `PF_colors` WHERE `Color_Groupe` != 'NA' GROUP BY Color_Groupe");
        if ($indexByLength)
            return $this->indexArrayByWordsCounts($results, "Color_Groupe");
        return $results;
    }

    public function getBrandInfo($code) {
        return $this->select("SELECT * FROM `brands` WHERE code = '$code'");
    }
    public function getDistribInfo($code) {
        return $this->select("SELECT * FROM `distribs` WHERE code = '$code'");
    }
    public function getAllDistributors($indexByLength = false) {
        $results = $this->selectAll("SELECT * FROM `distribs`");
        if ($indexByLength)
            return $this->indexArrayByWordsCounts($results, "name");
        return $results;
    }

    public function getAllAlias() {
        return $this->selectAll("SELECT *, CONCAT(type, ',', code) as concat FROM `PF_alias`");
    }

    public function getAliasGroup($text, $type = 'text') {
        $text = strtolower($text);
        $result = $this->select("SELECT code FROM `PF_alias` WHERE LOWER(alias) = '$text' AND type = '$type'");
        return (empty($result)) ? false : $result->code;
    }

    public function getAllAliasByCode($code) {
        $result = $this->selectAll("SELECT alias FROM `PF_alias` WHERE code = '$code'");
        return array_map(function($e) { return $e->alias; }, $result);
    }

    public function getTypeProductByAlias($code) {
        $all = $this->getAllAliasByCode($code);
        $all = array_map(function($e) { return $this->quote(strtolower($e)); }, $all);
        $all = implode(',', $all);

        return $this->select("SELECT id FROM `type_product` WHERE LOWER(name) IN ($all)");
    }

    public function getAllTypesProduct($indexByLength = false) {
        $results = $this->selectAll("SELECT * FROM `type_product`");
        if ($indexByLength)
            return $this->indexArrayByWordsCounts($results, "name");
        return $results;
    }

    public function getTypeProduct($sh) {
        $hidden_temp_type_product = $sh->getProps('hidden_type_product');
        $temp_type_product = $sh->getProps('type_product');

        $sh->setTypeProduct(null);

        $initial_condition = $this->buildQuery($sh);
        $sql_brands = "SELECT tp.id as code, tp.name FROM `superpicache` as cache 
        INNER JOIN `type_product` as tp ON tp.id = cache.type_product
        WHERE cache.parent = '' AND $initial_condition GROUP BY type_product ORDER BY tp.name";
        $sh->setTypeProduct($hidden_temp_type_product, true);
        $sh->setTypeProduct($temp_type_product);
        return $this->selectAll($sql_brands);
    }
}
?>