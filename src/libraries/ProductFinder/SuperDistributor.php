<?php 

if(!class_exists('SuperFinder'))
    require_once 'SuperFinder.php';

class SuperDistributor {
    
    public $finder;
    public $image;

    private $code;
    private $name;
    private $description;
    private $address;
    private $zip;
    private $pays;
    private $mailto;

    public function __construct($code = null) {
        if(!$code) return null;

        $this->finder = new SuperFinder();

        $infos = $this->finder->getDistribInfos($code);

        $this->code        = (isset($infos->code) ? $infos->code : '');
        $this->name        = (isset($infos->name) ? $infos->name : '');
        $this->description = (isset($infos->description) ? $infos->description : '');
        $this->image       = (isset($infos->image) ? $infos->image : '');
        $this->address     = (isset($infos->address) ? $infos->address : '');
        $this->zip         = (isset($infos->zip) ? $infos->zip : '');
        $this->pays        = (isset($infos->pays) ? $infos->pays : '');
        $this->mailto      = (isset($infos->mailto) ? $infos->mailto : '');
        
    }

    public function getCode()           { return $this->code; }
    public function getName()           { return $this->name; }
    public function getDescription()    { return $this->description; }
    public function getAddress()        { return $this->address; }
    public function getZip()            { return $this->zip; }
    public function getPays()           { return $this->pays; }
    public function getMailto()         { return $this->mailto; }
    public function getImage()          { return $this->image; }
}
?>