<?php 
class SuperBrand extends SuperClass implements JsonSerializable{
    
    //? Specific attributes
    public $code;
    public $name;
    public $comment;

    public function __construct($code, &$finder = null) {
        if (!$finder)
            $finder = new SuperFinder();

        $this->code = $code;
        $this->name = $finder->getOption($code, "veg_brand");
    }

    public function getCode()     { return $this->code; }
    public function getName()     { return (!empty($this->name)) ? $this->name : $this->code ; }

    public function __toString()
    {
        return $this->getCode();
    }

    public function jsonSerialize() {
        return [
            "code" => $this->getCode(), 
            "name" => $this->getName()
        ];
    }
}
?>