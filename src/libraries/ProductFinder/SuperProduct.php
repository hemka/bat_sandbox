<?php 
if(!class_exists('ProductModel'))
    require_once 'ProductModel.php';

if(!class_exists('SuperChild'))
    require_once 'SuperChild.php';

if(!class_exists('SuperFinder'))
    require_once 'SuperFinder.php';

if(!class_exists('SuperBrand'))
    require_once 'SuperBrand.php';

if(!class_exists('SuperDistributor'))
    require_once 'SuperDistributor.php';


    const SPRO_NONE  = 0;
    const SPRO_SHARED  = 1;
    const SPRO_DEVIS   = 2;
    const SPRO_CATALOG = 4;
    const SPRO_MINE    = 6;
    const SPRO_WITHCHILDREN  = 8;
    const SPRO_ALL     = 32767;

class SuperProduct extends SuperClass  implements JsonSerializable {
    //? Specific attributes
    public $finder;
    public $designer_id;
    public $technos;
    public $hashtags;
    public $distributeurs;
    public $single_link;
    public $sizes;
    public $distribs_colors_quantity;

    private $description;
    private $code;
    private $parent;
    private $name;
    private $default_picture;

    //? Relational attributes
    public $brand;       // SuperBrand OBJ
    public $categories;  // List of SuperCategory OBJ
    private $children;    // List of SuperProduct OBJ & List of SuperDistrib OBJ by Child

    public function __construct($code, $flag = SPRO_NONE, $all_infos = true, $default_color = []) {
        if(!$code) return null;
        
        $this->finder = new SuperFinder();
        $infos = $this->finder->getProductInfos($code, $flag & SPRO_WITHCHILDREN, $all_infos, $default_color);
        $this->code             = $infos->code;
        $this->designer_id      = $infos->id_product_virtuemart;
        $this->boconfig_id      = $infos->boconfig_id;
        $this->single_link      = "index.php?option=com_superpictor&view=singleproduct&code=".$this->code;
        $this->name             = $infos->name;
        $this->description      = $infos->description;
        $this->default_picture  = $infos->default_picture;

        if ($all_infos) {
            $this->colors           = $infos->colors;
            $this->distribs         = $infos->distribs;
            $this->sizes            = $infos->sizes;
            $this->attributes       = $infos->attributes;
            $this->matrice_colors_size_distrib = $infos->matrice_colors_size_distrib;
        }

        if ($infos->colors_pictures) {
            $this->colors_pictures  = new stdClass();
            $this->colors_pictures->total  = sizeof($infos->colors_pictures);
            $this->colors_pictures->pictures  = $infos->colors_pictures;
        }

        $this->hashtags         = [];
        $this->brand            = new SuperBrand($infos->veg_brand, $this->finder);
        $this->categories       = [];

        $this->distributeurs          = [];

        foreach(explode(',', $infos->veg_distributeurs) as $distributeur)
            $this->distributeurs[] = new SuperDistributor($distributeur);

        if($flag & SPRO_WITHCHILDREN)
            foreach($infos->children as $child)
                $this->children[$child->code] = new SuperChild($child->code);
        else
            unset($this->children);
    }

    //? Setter global
    public function setProps($prop, $value) { $this->{$prop} = $value;    }

    //? Getter global
    public function getProps($prop) { return $this->{$prop}; }

    public function getCode()     { return $this->code; }
    // ! temporaire en attendant les datas - Dan
    public function getBrandRef()     { return str_replace($this->brand->getCode()."_", "", $this->code); }
    public function getName()     { return $this->name; }
    public function getParent()   { return $this->parent; }
    public function getDescription()    { return $this->description; }
    public function getDefaultPicture($buildUrl = false) {
        $default_picture = $this->default_picture;
        if ($buildUrl)
            $default_picture = "/components/com_bat/views/indexproducts/tmpl/dl.php?min&ctx=pf&sourcename=" . $this->encrypt($this->brand . "/" .$this->default_picture, true);    
        return $default_picture;
    }
   
    public function getChildren() { return $this->children; }

    public function jsonSerialize() {
        return [
            "code" => $this->getCode(), 
            "parent" => $this-> getParent(), 
            "default_picture_url" => $this->getDefaultPicture(true), 
            "name" => $this->name, 
            "description" => $this->description, 
            "single_link" => $this->single_link, 
            "colors_pictures" => $this->getColorsPictures(true), 
            "brand_ref" => $this->getBrandRef(), 
            "brand" => $this->brand->getName()
        ];
    }

    public function getDesignerDeclinaisonColor($color_code, $color_name) {
        return $this->finder->getDesignerDeclinaisonColor($color_code, $color_name);
    }
}
?>