<?php 
if(!class_exists('SuperFinder'))
    require_once 'SuperFinder.php';
if(!class_exists('LDAPSP'))
    require_once 'LDAPSP.php';

class SuperList extends SuperClass {
    
    const DEFAULT_TYPE = 'LS_CATALOG';
    // const ListTypes = [
    //     "LS_SHARED"  => "Partagée",
    //     "LS_QUOTE"   => "Devis",
    //     "LS_PERSO"   => "Perso",
    //     "LS_PUBLIC"  => "Public",
    //     "LS_CATALOG" => "Catalogue"
    // ];

    public $finder;
    public $id;
    public $encrypt_id;
    public $name;
    public $owner_id;
    public $share_to;
    public $customers;
    public $type;
    public $products; // List of SuperProduct
    public $created_on;
    public $modified_on;
    public $hashtags;
    public $documents;

    public function __construct($id = null, $user_id = null, $name = null, $type = null, $all_infos = true) {
        $this->autoAuth('full');

        if(empty($id) && (!empty($user_id) && !empty($name)))
            $id = $this->createList($user_id, $name, $type);

        if ($id != null) {
            $this->id = $id;
            $infos = $this->getListInfos($all_infos);
            foreach($infos as $key=>$val)
                if(property_exists($this, $key))
                    $this->{$key} = $val;
        }
        
        $this->encrypt_id = $this->encrypt($id, true);
    }

    public function getProducts() { return $this->products; }
    public function getCustomers() { return $this->customers; }
    public function getHashtags() { return $this->hashtags; }

    //? Gestion des collaborateurs
    public function addCollaborators($users_infos) {
        $this->addListCollaborators($users_infos);
        $this->share_to = $this->getListCollaborators();
    }

    public function removeCollaborators($users) {
        $this->removeListCollaborators($users);
        $this->share_to = $this->getListCollaborators();
    }

    public function updateCollaborators($users_infos) {
        $this->updateListCollaborators($users_infos);
        $this->share_to = $this->getListCollaborators();
    }

    //?Gestion des Hashtags
    public function addHastags($name, $color) {
        $this->addListHashtag($name, $color);
        $this->hashtags = $this->getListHashtags();
    }

    public function removeHastags($hash_id) {
        $this->removeListHashtag($hash_id);
        $this->hashtags = $this->getListHashtags();
    }

    public function createList($owner_id, $name, $type = self::DEFAULT_TYPE, $hashtags = null) {
        $this->execute("INSERT INTO `digitizingBD`.`0_finder_lists` (`name`, `owner_id`, `type`, `published`) VALUES (". $this->quote($name).", ". $this->quote($owner_id).", ". $this->quote($type).", '1');");
        return $this->select("SELECT id FROM `digitizingBD`.`0_finder_lists` WHERE owner_id = $owner_id ORDER BY created_on DESC LIMIT 1")->id;
    }

    public function delete() {
        $this->execute("UPDATE `digitizingBD`.`0_finder_lists` SET published = 0 WHERE id = " . $this->id);
    }

    public function duplicate($name) {
        $old = $this->getListInfos();
        $new = $this->createList($old->owner_id, $name, $old->type, $old->hashtags);
        $this->duplicateRelatedList($old->id, $new);
        return $new;
    }

    public function updateName($name) {
        $this->execute("UPDATE `digitizingBD`.`0_finder_lists` SET name = ".$this->quote($name).", modified_on = ". $this->quote(date('Y-m-d H:i:s')) ." WHERE id = ".$this->quote($this->id));
    }

    // ! Voir duplication des champs products_in_list - Maxence
    public function duplicateRelatedList($old_id, $new_id) {
        $this->execute("INSERT INTO `digitizingBD`.`0_related_to_list` (`list_id`, `target`, `target_code`, `params`)
                        SELECT $new_id, target, target_code, params
                        FROM `digitizingBD`.`0_related_to_list`
                        WHERE list_id = $old_id");
    }

    public function getProductsInList($all_infos = false) {
        $products = $this->selectAll("SELECT product_code as code FROM `products_db`.`products_in_list` WHERE list_id = " . $this->quote($this->id));
        foreach($products as &$p)
            $p = new SuperProduct($p->code, SPRO_NONE, $all_infos);
        return $products;
    }

    public function addProduct($product_code) {
        $this->execute("UPDATE `digitizingBD`.`0_finder_lists` SET modified_on = ". $this->quote(date('Y-m-d H:i:s')) ." WHERE id = ".$this->quote($this->id));
        $this->execute("INSERT INTO `products_db`.`products_in_list` (list_id, product_code) VALUES (". $this->quote($this->id).",". $this->quote($product_code).")");
    }
    
    public function removeProduct($product_code) {
        $this->execute("UPDATE `digitizingBD`.`0_finder_lists` SET modified_on = ". $this->quote(date('Y-m-d H:i:s')) ." WHERE id = ".$this->quote($this->id));
        $this->execute("DELETE FROM `products_db`.`products_in_list` WHERE product_code = ".$this->quote($product_code)." AND list_id = ". $this->quote($this->id));
    }

    public function moveProduct($old_list_id, $product_code){
        $this->execute("UPDATE `products_db`.`products_in_list` SET list_id = ". $this->id ." WHERE list_id = $old_list_id AND product_code = ". $this->quote($product_code));
    }

    public function duplicateProduct($product_code){
        $this->execute("INSERT INTO `digitizingBD`.`0_related_to_list` (`list_id`, `target`, `target_code`, `params`)
                        SELECT ". $this->id .", target, target_code, params
                        FROM `digitizingBD`.`0_related_to_list`
                        WHERE list_id = ". $this->id ." AND target_code = ". $this->quote($product_code));
    }

    public function getUserLists($owner_id, $mode = LS_ALL, $all_infos = true) {
        $array = [];
        if($mode & LS_SHARED) {
            $lists = $this->selectAll("SELECT * FROM `products_db`.`products_in_list` as r LEFT JOIN `digitizingBD`.`0_finder_lists` as f ON r.list_id = f.id WHERE f.owner_id = $owner_id AND f.published = 1 AND f.type = 'LS_SHARED'");
            foreach($lists as $list) 
                $array[] = new SuperList($list->list_id, null, null, null, $all_infos); 
        }

        if($mode & LS_QUOTE) {
            $lists = $this->selectAll("SELECT fl.id FROM `digitizingBD`.`0_finder_lists` as fl WHERE owner_id = $owner_id AND published = 1 AND type = 'LS_QUOTE';");
            foreach($lists as $list) 
                $array[] = new SuperList($list->id, null, null, null, $all_infos);  
        }

        if($mode & LS_PERSO) {
            $lists = $this->selectAll("SELECT fl.id FROM `digitizingBD`.`0_finder_lists` as fl WHERE owner_id = $owner_id AND published = 1 AND type = 'LS_PERSO';");
            foreach($lists as $list) 
                $array[] = new SuperList($list->id, null, null, null, $all_infos);  
        }

        if($mode & LS_CATALOG) {
            $lists = $this->selectAll("SELECT fl.id FROM `digitizingBD`.`0_finder_lists` as fl WHERE owner_id = $owner_id AND published = 1 AND type = 'LS_CATALOG';");
            foreach($lists as $list) 
                $array[] = new SuperList($list->id, null, null, null, $all_infos);  
        }

        if($mode & LS_PUBLIC) {
            $lists = $this->selectAll("SELECT fl.id FROM `digitizingBD`.`0_finder_lists` as fl WHERE owner_id = $owner_id AND published = 1 AND type = 'LS_PUBLIC';");
            foreach($lists as $list) 
                $array[] = new SuperList($list->id, null, null, null, $all_infos);
        }
        return $array;
    }

    public function getListInfos($all_infos = true) {
        $result = $this->select("SELECT * FROM `digitizingBD`.`0_finder_lists` WHERE id = " . $this->quote($this->id));
        if ($all_infos) {
            $result->products  = $this->getProductsInList($all_infos);
            $result->share_to  = $this->getListCollaborators();
            $result->customers = $this->getListCustomers();
            $result->hashtags  = $this->getListHashtags();
            $result->documents = $this->getListDocuments();
        }
        return $result;
    }

    public function getListCollaborators() {
        $results = $this->selectAll("SELECT target_code as user, params as perm_type FROM `digitizingBD`.`0_related_to_list` WHERE target = 'user' AND list_id = " . $this->id);
        foreach($results as &$result)
            $result = new SuperClient($result->user, SP_INFOS);
        return $results;
    }

    public function getCustomProductName($code) {
        $result = $this->select("SELECT custom_name FROM `products_db`.`products_in_list` WHERE list_id = " . $this->quote($this->id). " AND product_code = " . $this->quote($code));
        return $result;
    }

    public function setCustomProductName($code, $custom_name) {
        $this->execute("UPDATE `products_db`.`products_in_list` SET `custom_name` = " . $this->quote($custom_name). " WHERE list_id = " . $this->quote($this->id). " AND product_code = " . $this->quote($code));
    }

    /**
     * Get customer societe link to list
     * @param integer $list_id
     * @param integer $user_id
     * @return mixed 
     */
    public function getlinkCustomer() {
        $r = $this->select("SELECT * FROM `digitizingBD`.`0_related_to_list` WHERE target = 'customer' AND list_id = " . $this->quote($this->id));
        $ldap = new LDAPSP($this->owner_id);
        $result = $ldap->getClientByIdentifier($r->target_code);

        return $result;
    }

     /**
     * Set customer societe link to list
     * @param string $client_id encrypt with base64
     */
    public function setlinkCustomer($client_id) {
        $this->execute("INSERT INTO `digitizingBD`.`0_related_to_list` (`id`, `list_id`, `target`, `target_code`, `params`) VALUES (NULL, " . $this->quote($this->id) . ", 'customer', " . $this->quote($this->decrypt($client_id, true)) . ", NULL)");
    }

    public function updatelinkCustomer($client_id) {
        $this->execute("UPDATE `0_related_to_list` SET `target_code` = " . $this->quote($this->decrypt($client_id, true)) . " WHERE `0_related_to_list`.`target` = 'customer' AND `0_related_to_list`.`list_id` = " . $this->quote($this->id));
    }

    public function addListCollaborators($infos) {
        foreach($infos as $val)
            $this->execute("INSERT `digitizingBD`.`0_related_to_list` (`list_id`, `target`, `target_code`, `params`) VALUES(".$this->id."),'user',".$val['user'].",".$this->quote($val['perm_type']).")");
    }

    public function removeListCollaborators($users) {
        foreach($users as $usr)
            $this->execute("DELETE FROM `digitizingBD`.`0_related_to_list` WHERE list_id =(".$this->id.") AND target = 'user' AND target_code = '$usr'");
    }

    public function updateListCollaborators($infos) {
        foreach($infos as $inf)
            $this->execute("UPDATE `digitizingBD`.`0_related_to_list` SET `params` = ".$this->quote($inf['perm_type'])." WHERE list_id =(".$this->id.") AND target = 'user' AND target_code = ".$this->quote($inf['user']));
    }

    public function getListCustomers() {
        $this->execute("SET SESSION group_concat_max_len = 1000000;");
        $infos = $this->select("SELECT GROUP_CONCAT(target_code) as codes  FROM `digitizingBD`.`0_related_to_list` WHERE list_id = " . $this->quote($this->id) . " AND target = 'customer' GROUP BY list_id");
        if(!$infos)
            return false;
        $useri = $this->select("SELECT * FROM `digitizingBD`.`0_finder_lists` WHERE id = " . $this->quote($this->id));
        
        $this->autoAuth('ldap');
        $cols = '`rowid`, `nom`, `address`, `town`, `zip`';
        $table = $useri->owner_id.'_societe';
        $result = $this->selectAll("SELECT $cols FROM `ldap_superpictor`.`$table` WHERE rowid IN(".$infos->codes.")");
        $this->autoAuth('sp');

        return $result;
    }

    public function getListDocuments() {
        return $this->selectAll("SELECT * FROM `digitizingBD`.`0_related_to_list` as rtl 
                                INNER JOIN `digitizingBD`.`0_document` as doc ON rtl.target_code = doc.id
                                WHERE rtl.list_id = ". $this->quote($this->id) ." AND rtl.target = 'document'");
    }

    public function addCustomer($cus_id) {
        $this->execute("INSERT `digitizingBD`.`0_related_to_list` (`list_id`, `target`, `target_code`) VALUES(". $this->quote($this->id) .", 'customer', $cus_id)");
    }

    public function removeCustomer($cus_id) {
        $this->execute("DELETE FROM `digitizingBD`.`0_related_to_list` WHERE list_id = ". $this->quote($this->id) ." AND target = 'customer' AND target_code = $cus_id");
    }

    public function getListHashtags() {
        $result = $this->select("SELECT hashtags FROM `digitizingBD`.`0_finder_lists` WHERE id = " . $this->quote($this->id));
        if($result)
            return json_decode($result->hashtags);
        return [];
    }

    public function addListHashtag($name_hash, $color) {
        $hashs = ($lst = $this->getListHashtags()) ? $lst : array();
        $temp['name']  = $name_hash;
        $temp['color'] = $color;
        array_push($hashs, $temp);
        $this->execute("UPDATE `digitizingBD`.`0_finder_lists` SET `hashtags` = ".$this->quote(json_encode($hashs))." WHERE id = " .$this->quote($this->id));
    }

    public function editListHashtag($name_hash, $color) {
        $hashs= [];
        $temp['name']  = $name_hash;
        $temp['color'] = $color;
        array_push($hashs, $temp);
        $this->execute("UPDATE `digitizingBD`.`0_finder_lists` SET `hashtags` = ".$this->quote(json_encode($hashs))." WHERE id = " . $this->quote($this->id));
    }

    public function removeListHashtag($hash_id) {
        $hashs = $this->getListHashtags();
        array_splice($hashs, $hash_id, 1);
        $this->execute("UPDATE `digitizingBD`.`0_finder_lists` SET `hashtags` = ".$this->quote(json_encode($hashs))." WHERE id = " . $this->quote($this->id));
    }

    public function isUserList($owner_id, $list_id) {
        $result = $this->execute("SELECT id FROM `digitizingBD`.`0_finder_lists` WHERE id = " . $this->quote($list_id) . " AND owner_id = " . $this->quote($owner_id));
        return (!$result) ?: true;

    }
}