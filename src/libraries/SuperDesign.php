<?php

use Joomla\CMS\Categories\Categories;

require_once ("SuperSource.php");

class SuperDesign extends SuperSource {
    public $documents = [];
    public $product;
    public $idbat;
    public function wordIsBlacklisted($word) {
        // ? Volontairement des mots plus permissif qui shunt d'autres mots. Laissez tel quel.
        $blacklist = [ "crafters", "bschool", "b school", "beschool", "hqtex", "hqtx", "hqt", "paul de la vega", 
                       "paul de", "test", "dropmed", "gladiasport", "gladia", "glad", "bernand", "bernan", "headquartex", 
                       "headq", "quartex", "cft", "gld", "bsh", "bsh", "deluxe","delux", "de luxe", "craft", "star"
                    ];
           foreach ($blacklist as  $blackword)
              if (stripos($word, $blackword) !== false)
                 return 1;
        return 0;
     }

    public function __construct($source, $source_id) {
        if(!class_exists('SuperProduct'))
            require_once $_SERVER['DOCUMENT_ROOT']."/src/libraries/ProductFinder/SuperProduct.php";

        parent::__construct($source, $source_id);
        $this->promotePDO(false);
        $this->execute("USE `templateDB`;");
        
        $this->product = new stdClass();

        $sql    = "SELECT * FROM `1_designed` WHERE rowid = ".intval($source_id);
        $design = $this->select($sql);
        
        if (!empty($design))
            foreach (get_object_vars(json_decode($design->zonesjson)) as $k => $v) 
                $this->$k = $v;

        //? Datas productr for document
        $product_fromCache = new SuperProduct($this->parent_id);

        if(empty($product_fromCache->getCode())) {
            $sql    = "SELECT sc.* FROM `templateDB`.`1_gestion_bat` as gb 
                        INNER JOIN `templateDB`.`1_designed` as des ON des.rowid = gb.designed 
                        LEFT JOIN `products_db`.`MA_supercache` as sc ON gb.product = sc.parent_id 
                        WHERE des.rowid = ".intval($source_id);

            $product_fromCache = $this->select($sql);
            if (!empty($product_fromCache)) {
                foreach (get_object_vars($product_fromCache) as $k => $v)
                    $this->product->$k = ($v === null || $this->wordIsBlacklisted($v)) ? "" : $v;

                $this->product->sizes = $this->getSizesFP();
                $this->product->colors = $this->getColorsFP();
                $this->product->categories = $this->getCategories();
            }
        }

        // temporaire en attente de la class SuperDocument;
        $batres = $this->select("SELECT * FROM `1_gestion_bat` WHERE designed = " . intval($this->source_id) );
        $this->idbat = $batres->rowid;
        $bat = new stdClass();
        $bat->ext = "BAT";
        $bat->type = "document";
        $bat->link_single = "/bat.html?view=single&id=".$this->encrypt($batres->rowid, true);
        $bat->link_title = "Voir et télécharger le BAT";

        $zip = new stdClass();
        $zip->ext = "ZIP";
        $zip->type = "download";
        $zip->link_single = "/components/com_bat/views/overview/tmpl/dlmu.php?id=".$this->encrypt($this->source_id, true);
        $zip->link_title = "Télécharger les visuels";

        $this->documents[] = $bat;
        $this->documents[] = $zip;

        $this->setUser($batres->idclient);
        // Common attributes
        $this->link_single  = "/superpictor.html?view=mockup&id=".$this->encrypt($this->source_id, true);
        $this->css["main_color"]  = "#F0134D";
        $this->css["second_color"] = "#4B3FB4";
        $this->created_on   = DateTime::createFromFormat('Y-m-d H:i:s', $batres->date_creation);
    }

    public function getOrderByRef($spref) {
        if ($spref) {
           $order = $this->select(
              "SELECT voi.virtuemart_order_item_id, vo.virtuemart_order_id, voi.order_status, vo.order_number, vo.order_pass, voi.product_attribute
              FROM `digime_virtuemart_order_items` AS voi
              INNER JOIN `digime_virtuemart_orders` AS vo
              ON voi.virtuemart_order_id = vo.virtuemart_order_id
              WHERE vo.virtuemart_user_id = $this->user_id
              AND voi.product_attribute LIKE '%\"$spref\"%'"
           );
  
           return $order;
        }
  
        return null;
    }
     
    public function getSizesFP() {
        $sizes = [];
        $array_result =  $this->selectAll(
            "SELECT customfield_id_4 FROM `products_db`.`MA_supercache` 
            WHERE parent_id = '" . $this->product->parent_id . "' GROUP BY customfield_id_4"
          );   

        foreach($array_result as $result)
            $sizes[] = $result->customfield_id_4;

        usort($sizes, array("SuperDesign", "sortSize"));

        return implode(' ', $sizes);
    }

    public function getColorsFP() {
        $colors = "";
        $result =  $this->selectAll(
          "SELECT customfield_id_7 FROM `products_db`.`MA_supercache` 
          WHERE parent_sku = '" . $this->product->parent_sku . "' GROUP BY customfield_id_7"
        );   
        
        foreach($result as $color)
            $colors = $colors . $color->customfield_id_7 . '&nbsp;-&nbsp;';
        $colors = substr($colors, 0, -8);
        return $colors;
    }

    public function getCategories() {
        $categories = '';
        $tmp_categories = '';
        $tmp2_cat = [];
        $child_categories = $this->selectAll(
            "SELECT virtuemart_category_id FROM `products_db`.`MA_virtuemart_product_categories` 
            WHERE virtuemart_product_id = " . $this->product->parent_id .""
          );  

          foreach($child_categories as $child_category) {

            $child_category = $child_category->virtuemart_category_id;
            $tmp_categories = $child_category;
            while($child_category != '0' && $child_category != '3452') {
                $parent_category = $this->select(
                    "SELECT category_parent_id FROM `products_db`.`MA_virtuemart_category_categories` 
                    WHERE category_child_id  = " . $child_category .""
                );  

                $child_category = $parent_category->category_parent_id;
                $tmp_categories = $child_category . ',' . $tmp_categories;
            }

            if(!empty($child_category) && $child_category != 0 ) {

                // $tmp_array_cat = [];
                $cat_array = explode(',', $tmp_categories);
                $cat_p = $cat_array[1];
                $cat_c = $cat_array[2];

                $cat_p = $this->select("SELECT category_name FROM `products_db`.`MA_virtuemart_categories_fr_fr` WHERE virtuemart_category_id = " . $cat_array[1] ."")->category_name;

                $cat_c = $this->select("SELECT category_name FROM `products_db`.`MA_virtuemart_categories_fr_fr` WHERE virtuemart_category_id = " . $cat_array[2] ."")->category_name;

                $tmp2_cat[$cat_p] = $cat_c;
            }  

          }
          $categories = '';
          foreach($tmp2_cat as $key => $cat)
            if($key != 'HASHTAGS' && $key != 'Techniques de Personnalisation')
            $categories .= $key . ' ' . $cat . ' - ';
          
            $categories = substr($categories, 0, -3);
        return $categories;
    }

    public function DuplicateMockup() {
		$this->execute(
			"INSERT INTO `templateDB`.`1_designed` (user_id, json, zonesjson, date) 
			SELECT user_id, json, zonesjson, NOW() FROM `templateDB`.`1_designed` WHERE rowid = :id",
			array(
				'id' => $this->source_id
			)
		);
		$id_duplicate = $this->pdo->lastInsertId();

		$this->execute(
			"INSERT INTO `templateDB`.`1_gestion_bat` (nom, reference, idclient, options, date_creation, date_modif, date_genthumbnails, product, product_name, template, designed, statut) 
			SELECT nom, reference, idclient, options, NOW(), NOW(), date_genthumbnails, product, product_name, template, $id_duplicate, statut  FROM `templateDB`.`1_gestion_bat` WHERE designed = :id",
			array(
				'id' => $this->source_id
			)
		);

		$id_gestion_duplicate = $this->pdo->lastInsertId();

		$old_id_gestion_duplicate = $this->select(
			"SELECT rowid FROM `templateDB`.`1_gestion_bat` WHERE designed = :id",
			array(
				'id' => $this->source_id
			)
		)->rowid;

		$this->execute(
			"INSERT INTO `templateDB`.`1_element_element` (`source`, `source_id`, `target`, `target_id`, `created_at`) 
			SELECT source, source_id, target, $id_gestion_duplicate, NOW() FROM `templateDB`.`1_element_element` WHERE source = 'user' AND target = 'bat' AND target_id = :id",
			array(
				'id' => $old_id_gestion_duplicate
			)
		);

		return $id_gestion_duplicate;
	}
}