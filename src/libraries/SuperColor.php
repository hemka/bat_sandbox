<?php 

if (!class_exists('SuperClass'))
  require_once "SuperClass.php";

class SuperColor extends SuperClass {

    private $id;
    public $type = "";
    public $collection = "";
    public $name = "";
    public $ref = "";
    public $hexa = "";
    public $rgb = "";
    public $r = "";
    public $g = "";
    public $b = "";

    public function __construct($color_id) {
        $this->autoAuth("sp");
        $this->id = $color_id;
        $query = "SELECT * FROM `colors` WHERE id = $this->id LIMIT 1";
        $infos = $this->select($query);

        foreach($infos as $key => $info) {
            $this->{$key} = $info;
        }

        $css = 'rgb(%d , %d , %d)';
        $this->rgb = sprintf($css, $this->r, $this->g, $this->b);
        
    }

}

?>