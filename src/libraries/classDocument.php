<?php 

// ini_set("display_errors", 1);
// ini_set("display_startup_errors", 1);
// error_reporting(E_ALL);

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
// require_once  $root."/templates/superpictor/css/scssphp-1.1.0/scss.inc.php";
require $root."/src/style/scssphp-1.1.0/scss.inc.php";
// require_once ($root.'/libraries/simplehtmldom/simple_html_dom.php');

// $root = realpath($_SERVER["DOCUMENT_ROOT"]);
/*require_once($root.'/designer/simple_html_dom.php');
 require_once ($root.'/libraries/phpwkhtmltopdf/vendor/autoload.php');
use ScssPhp\ScssPhp\Compiler;
use \mikehaertl\wkhtmlto\Pdf;
use \mikehaertl\wkhtmlto\Image;  */
require_once($root.'/src/libraries/simple_html_dom.php');
require($root.'/src/vendor/autoload.php');
// require $root."/src/style/scssphp-1.1.0/scss.inc.php";
use ScssPhp\ScssPhp\Compiler;
use \mikehaertl\wkhtmlto\Pdf;
use \mikehaertl\wkhtmlto\Image;
require_once ("SuperSource.php");


class SuperDocument extends SuperSource {
	public $id;
    public $user_id;

    public $ext;
    public $name;
    public $download;
    public $date_creation;    
    public $ref;
    public $editedFields;

    public $html = array();
	
    public $io =  array(); 
	
    public $slug;
    public $theme;
    public $config;
    public $type;
    public $orientation;
    public $published;
    public $template_id;
    public $source;
    public $source_id;
	public $context = 'client'; //? to set mode client or server 
	public $root_path;
  
    public function __construct($id = null, $context = 'client'  ) {
		$this->autoAuth("sp");
		$this->id			    = $id;
		$this->io["statics"]    = array();
		$this->io["stacks"] 	= array();

        if ($this->id != null) {
            $document            = $this->select("SELECT * FROM 0_document WHERE id = ".$this->id);
            $this->user_id       = $document->user_id;
            $this->type          = $document->type;
            $this->template_id   = $document->template;
            $this->source        = $document->source;
            $this->source_id     = $document->source_id;
            $this->date_creation = DateTime::createFromFormat('Y-m-d H:i:s', $document->date_creation);
            $this->editedFields  = json_decode($document->fields);
			
            $template          = $this->select("SELECT * FROM `0_document_templates` WHERE id = " . $this->template_id);
            $this->name        = $template->nom;
            $this->slug        = $template->slug;
            $this->config      = json_decode($template->config);
            $this->type        = $template->type;
            $this->orientation = $template->orientation;
            $this->published   = $template->published;
			$this->setContext($context);
			
            foreach ($this->config->pages as $page) {
				$path = $_SERVER['DOCUMENT_ROOT'] . '/templates/'.$this->slug.'/'.$page->fichier;
                $this->html["modele"][$page->nom] = file_get_contents($path);
            }
            $root = $_SERVER['DOCUMENT_ROOT'];
            if (!empty($this->config->scss->variables)) {
				$text_css = "";
                foreach ($this->config->scss->variables as $variable) {
					$text_css .= "$".$variable->nom.":\t".$variable->value.";\n";
                    if (!is_dir($root.'/templates/'.$this->slug.'/css'))
                    mkdir($root.'/templates/'.$this->slug.'/css');
                    file_put_contents($root.'/templates/'.$this->slug.'/css/config_'.$this->template_id.'.scss', $text_css);
                }
            }
		}
    }

	public function setContext($context) {
		$this->context = $context;
		$this->root_path = ($this->context != 'client') ? realpath($_SERVER["DOCUMENT_ROOT"]) : "";
	}

	public function initInfos($json) {
		if(!json_decode($json)) //Verifie si c'est au format json
			return;

		$this->execute(
		  "UPDATE `templateDB`.`0_document`
		  SET fields = :fields
		  WHERE id = :id",
		  array(
			'fields' => $json,
			'id' => $this->id
		  )
		);
	}

	public function getDocInfos() {
		if ($this->source == "mockup") {
			if (!class_exists("SuperEntity")) 
				require_once ($_SERVER['DOCUMENT_ROOT'] . "/src/libraries/SuperEntity.php");
			$this->doc_infos =  SuperEntity::get("mockup",  $this->source_id);

		} else if ($this->source == "product") {
			if (!class_exists("SuperLang")) 
				require_once ($_SERVER['DOCUMENT_ROOT'] . "/src/libraries/SuperLang.php");
			$lang = new SuperLang();

			$code = $this->source_id;
			if(!class_exists("SuperProduct"))
				require_once ($_SERVER['DOCUMENT_ROOT'] . "/src/libraries/ProductFinder/SuperProduct.php");

			$this->doc_infos          = new stdClass();
			$this->doc_infos->product = new SuperProduct($code);
			$this->doc_infos->user = new SuperClient($this->user_id, SP_INFOS);
			$this->doc_infos->views = reset($this->doc_infos->product->getColorsPictures(false, 1, false)["pictures"]);
			$this->doc_infos->idbat = md5("FICHE_".$this->id);
			$this->doc_infos->product->setProps('brand', $lang->l('br_'.$this->doc_infos->product->getProps('brand')));
		}

		foreach(["sizes", "colors"] as $prop)
			if($this->doc_infos->product instanceof SuperProduct) {
				$new_tab = implode(' ', array_map(function ($entry) {
					return $entry['name'];
				}, $this->doc_infos->product->getProps($prop)));

				$this->doc_infos->product->setProps($prop, $new_tab);
			}

		return (!empty($this->doc_infos)) ? $this->doc_infos : null;
	}
   
    public function setnbpages(){
        $pageadefinir = 1;
        for($i = 0; $i < ($pageadefinir); $i++)
            foreach ($this->config->pages as $page) {
                $this->html["generated"][$page->nom][$i] =  $this->html["modele"][$page->nom];            
            }
    }
 
	public function io_addStatic($category, $attribute, $value) {
		if (!isset($this->io["statics"][$category]))
			$this->io["statics"][$category] = array();

		if (is_string($value)) {
			$tmpvalue          = new stdClass();
			$tmpvalue->content = $value;
			$value             = $tmpvalue;
		}
		$this->io["statics"][$category][$attribute] = $value;
	}

	public function io_addStackable($attribute, $value, $reset = false) {
		if (!isset($this->io["stacks"][$attribute]) || $reset)
			$this->io["stacks"][$attribute] = array();

		if (is_string($value)) {
			$tmpvalue          = new stdClass();
			$tmpvalue->content = $value;
			$value             = $tmpvalue;
		}

		$this->io["stacks"][$attribute][] = $value;
	}

    public function io_load() {
		foreach ($this->io["stacks"] as $stack_category => $stack_values) { 
            foreach ( $this->html["generated"] as $page_name => $pages_html) 
				foreach ( $pages_html as $nbpages => $htmlpage) {
					$simpleHtml = str_get_html($htmlpage);
					foreach ($simpleHtml->find('[sp-stackable='.$stack_category.']') as $field) {
						if (!isset($this->io["stacks"][$stack_category]["template"])) {
							$this->io["stacks"][$stack_category]["template"] = $field->outertext;
						}
						$field->outertext = "";

						for ($is=0; $is < $field->getAttribute("sp-repeat"); $is++) { 
							foreach($stack_values as $stack_value) {
								if (isset($stack_value->used) && $stack_value->used)
									continue;
								$stack_value->used = true;

								$modeletmpl = str_get_html($this->io["stacks"][$stack_category]["template"]);
								foreach ($stack_value as $attr => $attrval) {
									foreach ($modeletmpl->find('[sp-type='.$attr.']') as $subfield) {
										if ($subfield) {
											if ($subfield->tag == "img" && !empty($attrval)) {
												$path = (strpos($attrval, 'http://') !== false || strpos($attrval, 'https://') !== false) ? $attrval : $this->root_path.$attrval;
												$subfield->setAttribute("src", $path);
											} else if ($subfield->tag == "table") {
												$this->fillArray($subfield, $attrval);
											} else {
												if (isset($attrval)) {
													$subfield->innertext = $attrval;
												}
											}
										}
									}
								}
								$field->outertext .= $modeletmpl->save();

								break;
							}
						}

					}

 					

					$this->html["generated"][$page_name][$nbpages] = $simpleHtml->save();
				}

        }  

		
        foreach ($this->io["statics"] as $toreplace_category => $toreplace_values) { 
            foreach ( $this->html["generated"] as $page_name => $pages_html) 
				foreach ( $pages_html as $nbpages => $htmlpage) {
					$simpleHtml = str_get_html($htmlpage);
					foreach ($simpleHtml->find('[sp-category='.$toreplace_category.']') as $field) {
						if ($field) {
							$edit = $field->getAttribute('sp-edit');
							if ($field->tag == "img" && !empty($toreplace_values[$edit])) {
								$field->setAttribute("src", $toreplace_values[$edit]->content);
							} else if ($field->tag == "table") {
								$this->fillArray($field, $toreplace_values);
							} else {
								if (isset($toreplace_values[$edit])) {
									$field->innertext = $toreplace_values[$edit]->content;
								}
							}
						}
					}
					$this->html["generated"][$page_name][$nbpages] = $simpleHtml->save();
				}

        }   
    }

	public function fillArray($mark,$matrix) {
		$html = '<tbody>';
		foreach($matrix["matrice"] as $key => $values) {
			$html .= "<tr id='$key'>";
			foreach($values as $idx => $col)
				$html .= "<td id='$key-$idx'>$col</td>";

			$html .= "</tr>";
		}

		$html .= '</tbody>';
		$mark->innertext = $html;
	}

	public function getStyleSheetPath() {
		$root = realpath($_SERVER["DOCUMENT_ROOT"]) ;
		if (file_exists($root.'/templates/'.$this->slug.'/style.scss')) {
			$scss = new Compiler();
			$scss->setImportPaths("$root/templates/".$this->slug."/");			
			$scss_texte = "";
			if (file_exists($root.'/templates/'.$this->slug.'/css/config_'.$this->template_id.'.scss')){ 
				$scss_texte .= '@import "css/config_'.$this->template_id.'.scss";';
				$scss_texte .= '@import "style.scss";';
				file_put_contents($root.'/templates/'.$this->slug.'/css/style_'.$this->template_id.'.css', $scss->compile($scss_texte));	

				return '/templates/'.$this->slug.'/css/style_'.$this->template_id.'.css'; 
			} else			
				file_put_contents($root.'/templates/'.$this->slug.'/style.css', $scss->compile('@import "style.scss";'));
		}

		return '/templates/'.$this->slug.'/style.css';
    }

    public function displayPage(){		
        foreach ($this->html["generated"] as $page_name => $pages_html)
			foreach ( $pages_html as $nbpages => $htmlpage)
				echo $this->html["generated"][$page_name][$nbpages];
	}

    public function getTemplateEditableFields() {
        $editables = [];
		foreach ($this->html["modele"] as $page_name => $page) {
			$html = str_get_html($page);

			$categories = array();
			foreach ($html->find("[sp-edit][sp-category]") as $category_element) {
				$category = $category_element->getAttribute('sp-category');

				if (!in_array($category, $categories)) {
					$categories[] = $category;
				}
			}

			foreach ($categories as $category) {
				$tmp = [];

				foreach ($html->find("[sp-edit][sp-category=$category]") as $field) {
					if ($field) {
						$edit = $field->getAttribute('sp-edit');

						$obj = new stdClass();
						$obj->label = $field->getAttribute('sp-label');
						$obj->value = trim($field->plaintext);
						$obj->order = intval($field->getAttribute('sp-order'));
						$obj->type = $field->getAttribute('sp-type');
						$obj->hiddable = $field->getAttribute('sp-hiddable');
						$obj->permission = intval($field->getAttribute('sp-permission'));
						$obj->infoField = $field->getAttribute('sp-infofield');
						$obj->isImg = $field->getAttribute('sp-img');
						$obj->isStatic = $field->getAttribute('sp-static');
						$obj->selectOptions = array();

						if ($obj->type == 'select') {
							switch ($edit) {
								case 'commercial':
									$sc = new SuperClient($this->user_id, 0);

									if ($current_userinfos = $sc->select(
										"SELECT virtuemart_user_id, first_name, last_name
										FROM `digime_virtuemart_userinfos`
										WHERE virtuemart_user_id = $this->user_id"
									)) {
										array_push($obj->selectOptions, array(
											'value' => $current_userinfos->virtuemart_user_id,
											'text'  => "$current_userinfos->first_name $current_userinfos->last_name"
										));
									}

									if ($contacts = $sc->getRootUserContacts()) {
										foreach ($contacts as $contact) {
											array_push($obj->selectOptions, array(
												'value' => $contact->virtuemart_user_id,
												'text'  => "$contact->first_name $contact->last_name"
											));
										}
									}
								break;
								default: break;
							}
						}

						$tmp[$edit] = $obj;
					}
				}
				uasort($tmp, 'callBackSort');
				if (!isset($editables[$category]))
					$editables[$category] = $tmp;
			}
		}

		return $editables;
    }

    public function setTemplateEditableFields() {		
        $root = realpath($_SERVER["DOCUMENT_ROOT"]) ;

		foreach ($this->html["generated"] as $page_name => $pages_html ) {
			foreach ($pages_html as $nbpages => $html) {
				$html = str_get_html($html);
				$layout_number = count($this->doc_infos->views);
				if($layout_number > 0 && $layout_number <= 1) {
					$layout_number = 1;
				} else if ($layout_number > 0 && $layout_number <= 2) {
					$layout_number = 2;
				} else {
					$layout_number = 4;
				}

				$yrd = $html->find(".contenaire_produit", 0);
				$yrd->setAttribute('layout', $layout_number);
				foreach ($html->find('[sp-edit][sp-category=infoproduct]') as $field) {
					if ($field) {
						$edit = $field->getAttribute('sp-edit');
						
						if (isset($this->editedFields->$edit) || $field->tag == 'img') {
							
							if ($field->tag == 'img') {
								$imgSrc = '';

								if (isset($this->editedFields->$edit->value))
									$imgSrc = $root.$this->editedFields->$edit->value.'?'.time();

								if ($imgSrc)
									$field->src = $imgSrc;
								else {
									if ($field->getAttribute('sp-contained')) {
										$contained = $html->find('.'.$edit.'_container', 0);
										$contained->outertext = '';
									}
									else
										$field->outertext = '';
								}
							}
							else
								$field->innertext = preg_replace("/(&lt;)[a-zA-Z][a-zA-Z0-9](&gt;)/", '. ', $this->editedFields->$edit->value);

							if (isset($this->editedFields->$edit) && $this->editedFields->$edit->hidden) {
								if ($field->getAttribute('sp-contained')) {
									$contained = $html->find('.'.$edit.'_container', 0);
									$contained->setAttribute('hidden-element', '');
								}
								else
									$field->setAttribute('hidden-element', '');
							}
						}
					}
				}							
				
                $this->html["generated"][$page_name][$nbpages] = $html->save();
			}
		}
	}
	
	public function sortiePDF($orientation = "Landscape", $sendtoout = false) {
		$root = realpath($_SERVER["DOCUMENT_ROOT"]);
		//$this->displayPage();
		if (!empty($this->orientation))
			$orientation = $this->orientation;

			
		$pdf = new Pdf(array(
		'no-outline',         // Make Chrome not complain
		'encoding'      => 'UTF-8',
		'margin-top'    => 0,
		'margin-right'  => 0,
		'margin-bottom' => 0,
		'orientation'   => $orientation,
		'margin-left'   => 0,

		// Default page options
		'disable-smart-shrinking',
		'print-media-type',
		'user-style-sheet' => $root.$this->getStyleSheetPath()
		));
		
		$starthtml =
			'<!DOCTYPE html>
				 <html>
				 <head>
					 <style>
					 	body {
							margin: 0;
							padding: 0;';
							if ($orientation == "Portrait")
							$starthtml .= 'height: 297mm; width: 210mm;';
								else
							$starthtml .= 'height: 210mm; width: 297mm;';
		$starthtml .='}
					 </style>
				 </head>
				 <body>
				 ';
		$endhtml = '</body></html>' ;

		foreach ($this->html["generated"] as $key => $html) {
			if ($html == $this->html[$key])
				continue; // temporaire senator
		
			if ($key != '')
				foreach($html as $nb=>$htmlpage)
					$pdf->addPage($starthtml.$htmlpage.$endhtml);
		
		}

		$name = md5("FICHE_".$this->doc_infos->idbat);
		$target = $root.'/pdf/'.$name.'.pdf' ;

		if (file_exists($target)) {
			chmod($target, 0777);
			unlink($target);
		}

		if ($sendtoout) {
			if (!$pdf->send())
				throw new Exception('Could not create PDF: '.$pdf->getError());

			exit(1);
		}


		$pdf->saveAs($target);

		return '/pdf/'.$name.'.pdf';
	}

}

function callBackSort($a, $b) {
	if ($a->order == $b->order) return 0;
	return ($a->order < $b->order) ? -1 : 1;
}

?>