<?php

// require_once ("SuperSource.php");
// require_once ("SuperItem.php");
// require_once ("SuperSourceBat.php");

foreach (scandir($_SERVER['DOCUMENT_ROOT'] . "/src/libraries/") as $filename)
    if (strpos($path = $_SERVER['DOCUMENT_ROOT'] . "/src/libraries/".$filename, "Super") !== false)
        require_once ($path);

abstract class SuperEntity  extends SuperClass {
     public static function get($source, $source_id) {
        if ($source && $source_id) {
                switch (strtolower($source)) {
                    case 'item':
                        return new SuperItem($source, $source_id);
                        break;
                    case 'mockup':
                        return new SuperDesign($source, $source_id);
                        break;
                    case 'bat':
                        return new SuperSourceBat($source, $source_id);
                        break;
                    case 'document':
                        return new SuperProductSheet($source_id);
                        break;
                    case 'superlogo':
                        return new SuperLogo($source_id, 0);
                        break;
                    default:
                        return new SuperSource($source, $source_id);
                        break;
                }
        }
    }
  /*   public function getSourceInfo($source, $user_id, $client_id ){
        $sql = "SELECT * FROM 1_element_element WHERE source = '$source' AND target = 'client'  AND target_id = '$user_id::societe::$client_id' " ;
        $sourceInfo = $this->selectAll($sql);
        return $sourceInfo ;

    } */

}


