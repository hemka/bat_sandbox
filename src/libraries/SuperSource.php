<?php

require_once ("SuperClass.php");


const SP_ELEMENT_PATTERN_SOCIETE = "%::societe::%";

class SuperSource extends SuperClass {
    public $files;
    public $client = "";
    public $admin_infos = [];
    public $contact = [];
    public $source;
    public $source_id;
    public $user_id;
    public $css = [];

    public $name;
    public $ref;
    public $link_single;
    public $created_on ;
    public $status;
    public $status_code;
    public $status_class;
    public $type;

    public $tags = [];

    public $user_tags = [];
    public $smart_tags = [];
    public $g_tags = [];
    public $user_clients = [];
    

    public function __construct($source = null, $source_id = null) {
        $this->autoAuth("sp");
        if ($source && $source_id) {
            $this->source    = $source;
            $this->source_id = intval($source_id);
            $this->created_on = new Datetime();

            $this->getTags();
            $this->getSourceContact();
        }
    }

    public function setUser($user_id) {
        $this->user_id = $user_id;

        if (!class_exists('SuperClient'))
            require_once "SuperClient.php";

        $this->user = new SuperClient($this->user_id, SP_NONE);

        $this->user->loadERP();
        $this->getUserClients();
    }

    private function getUserClients() {
        $this->user_clients = $this->getAttachedElements("client", SP_ELEMENT_PATTERN_SOCIETE);

        foreach ($this->user_clients as $itclient => $client) {
                list($user_id,, $client_id) = explode('::', $client->target_id);
                $this->user_clients[$itclient] = $this->user->erp->getClient($client_id);
        }
    }

    public function promotePDO($local = false) {
        if ($local)
            $this->pdo = $this->setPDO('mysql:host=127.0.0.1', 'root', '');
        else {
            if (!class_exists('spConfig'))
                require_once $_SERVER['DOCUMENT_ROOT'] . "/spconfig.php";
            // $this->pdo = new PDO('mysql:host='.spConfig::getHost("full").';dbname='.spConfig::getDbname("full"), spConfig::getUser("full"), spConfig::getPwd("full"));
            $this->pdo = new PDO('mysql:host='.spConfig::getHost("sp").';dbname='.spConfig::getDbname("sp"), spConfig::getUser("sp"), spConfig::getPwd("sp"));
        }
    }

    public function getTags() { 
        $resfiles = $this->selectAll(sprintf(
            "SELECT * FROM `0_tags_relations` as relation JOIN `0_tags` as tags ON tags.rowid = relation.tag_id WHERE relation.source = %s AND source_id = %s",
            $this->quote($this->source), $this->quote($this->source_id)
        ));

        foreach ($resfiles as $key => $tag) {
            $this->tags[$tag->type][] = $tag;
            if ($tag->type == "user")
                $this->user_tags[] = $tag->tag_id;
            else if ($tag->type == "smart")
                $this->smart_tags[] = $tag->tag_id;
        }
        return $this->tags;
    }

    public function getSourceContact() {
        $ret = $this->select(sprintf("SELECT * FROM `1_element_element` WHERE source = 'user' AND  `target` = %s AND `target_id` = %s",  
                                            $this->quote($this->source), $this->quote($this->source_id)
                                        ));
        if ($ret) {
            // ? Tableau pour futur attr contact
            $this->contact["id"] = $ret->source_id ;
            $this->contact["userinfos"] = $this->getOwnerInfo($ret->source_id) ;

        }
        
        return $this->contact;
    }

    public function getHtmlStatus($statuscode, $type = "label") {
        return "";
      }

    public function getOwnerInfo($user_id) {
        

        $sql = " SELECT * FROM `digime_virtuemart_userinfos` WHERE virtuemart_user_id = ".intval($user_id);
        $owner = $this->select($sql);
        if(empty($owner))
            $this->client = "Cette utilisateur n'existe plus";
        else
            $this->client = $owner->company . " " . $owner->name;

        return $owner;
    }

    public function addTag($tag_name, $parent_id = null, $relation = "user", $user_id = 0) {
        $upsertinfos = array();
        $upsertinfos["tag_name"]             =  [ "value" => $tag_name, "key" => 1 ];
        if ($relation == "smart")
            $upsertinfos["smart"]             =  [ "value" => 1];
        $tag_id = $this->dxupsert("0_tags", $upsertinfos);
        
        $upsertinfos              = array();
        $upsertinfos["tag_id"]    = [ "value" => $tag_id, "key" => 1 ];
        $upsertinfos["type"]      = [ "value" => $relation, "key" => 1 ];
        $upsertinfos["source"]    = [ "value" => $this->source, "key" => 1 ];
        $upsertinfos["source_id"] = [ "value" => $this->source_id, "key" => 1 ];
        $relation_tag_id                   = $this->dxupsert("0_tags_relations", $upsertinfos);

        if ($relation_tag_id)
            $this->execute("DELETE FROM `0_tags_predictions` WHERE source = '".$this->source."' AND source_id = ".intval($this->source_id) . " AND tag_id = " . intval($tag_id));
        if ($parent_id !== null)
            $this->dxupsert('0_tag_tag', array(
                'parent_tag_id' => ['value' => intval($parent_id)],
                'child_tag_id'  => ['value' => $tag_id, 'key' => 1],
                'user_id'       => ['value' => $user_id, 'key' => 1]
            ));
    }

    

    public function excludeTag($tag_id) {
        $upsertinfos              = array();
        $upsertinfos["tag_id"]    = [ "value" => $tag_id, "key" => 1 ];
        $upsertinfos["exclude"]   = [ "value" => 1 ];
        $upsertinfos["score"]     = [ "value" => 0 ];
        $upsertinfos["source"]    = [ "value" => $this->source, "key" => 1 ];
        $upsertinfos["source_id"] = [ "value" => $this->source_id, "key" => 1 ];
        return $this->dxupsert("0_tags_predictions", $upsertinfos);
    }

    public function editTag($tag, $user_id) {
        $this->dxupsert('0_tags', array(
            'rowid'    => ['value' => $tag['rowid'], 'key' => 1],
            'tag_name' => ['value' => $tag['tag_name']]
        ));

        $this->dxupsert('0_tag_tag', array(
            'parent_tag_id' => ['value' => $tag['parent_tag_id']],
            'child_tag_id'  => ['value' => $tag['rowid'], 'key' => 1],
            'user_id'       => ['value' => $user_id, 'key' => 1]
        ));
    }

    public function removeTag($tag_name, $relation = "user") {
        $upsertinfos             = array();
        $upsertinfos["tag_name"] = [ "value" => $tag_name, "key" => 1 ];
        $tag_id                  = $this->dxupsert("0_tags", $upsertinfos, 0, true);

        $upsertinfos              = array();
        $upsertinfos["tag_id"]    = [ "value" => $tag_id, "key" => 1 ];
        $upsertinfos["type"]      = [ "value" => $relation, "key" => 1 ];
        $upsertinfos["source"]    = [ "value" => $this->source, "key" => 1 ];
        $upsertinfos["source_id"] = [ "value" => $this->source_id, "key" => 1 ];
        $relation_tag_id          = $this->dxupsert("0_tags_relations", $upsertinfos, 0, true);

        if ($relation_tag_id)
            $this->execute("DELETE FROM `0_tags_relations` WHERE rowid = " . intval($relation_tag_id));
    }
    public function isPredicted($source,$source_id) {
        $sql = "SELECT * FROM 0_tags_predictions 
                JOIN 0_tags ON 0_tags.rowid = 0_tags_predictions.tag_id
                WHERE source = '".$source."' AND exclude = 0 AND source_id = ".intval($source_id);
        $result = $this->pdo->prepare($sql);
        $result->execute();
        return $result->fetchAll(PDO::FETCH_OBJ); 
    


    }

    public function getNumDuplicate($source, $source_id) {
        return $this->select("SELECT * FROM `1_num_duplicate` WHERE source_item_id  = $source_id");
    }

    public function getGroup($create = false, $infos = "", $relation = "") {
        $groupe = $this->select("SELECT target_id FROM `1_duplicate_association` WHERE source_id = $this->source_id AND source = '$this->source' ");
        if ($groupe) 
            $id_groupe =  $groupe->target_id;

        if (!$groupe && $create) {
            $id_groupe = $this->createGroup();
            $this->execute(sprintf("INSERT INTO `1_duplicate_association` (source, source_id, target, target_id, details, created_at, relation)
                                    VALUES ('%s' , %d ,'duplicate_group' , %d , %s, NOW(), %s)", 
                                            $this->source,  $this->source_id, $id_groupe, $this->pdo->quote($infos), $this->pdo->quote($relation)));
        }

        return $id_groupe;
    }

    public function getGroupDeclinate() {
        if (!$groupId = $this->getGroup())
            return false;
        $selectGroupDec = $this->select("SELECT target_id FROM `1_duplicate_association` WHERE source_id = $groupId AND source = 'duplicate_group'");
        return $selectGroupDec;
    }

    public function createGroup() {
        $this->execute("INSERT INTO `1_duplicate_group` (source, created_at) VALUES ('duplicate_group', NOW())");
        return $this->pdo->lastInsertId();
    }
    
    public function insertInGroup($groupId, $infos, $relation) {
        $infos = $this->pdo->quote($infos);
        $relation = $this->pdo->quote($relation);
        $this->execute("INSERT INTO `1_duplicate_association` (source, source_id, target, target_id, details, created_at, relation)
                        VALUES ('$this->source' ,$this->source_id ,'duplicate_group' ,$groupId ,$infos, NOW(), $relation)"); 
    }

    public function insertDeclinate($groupId, $groupDec, $infos) {
        $infos = $this->pdo->quote($infos);
        $this->execute("INSERT INTO `1_duplicate_association` (source, source_id, target, target_id, details, created_at, relation)
                        VALUES ('duplicate_group' ,$groupId ,'duplicate_group' ,$groupDec ,$infos, NOW(), 'DECLINATE')");
    }

    public function updateDeclinate($groupDoublon) {
        $this->execute(sprintf("UPDATE `1_duplicate_association` SET target_id = %d WHERE target_id = %d AND relation = 'DECLINATE'",
                                $this->getGroupDeclinate(), $groupDoublon));
    }

    public function updateGroup($groupDoublon) {
        $this->execute(sprintf("UPDATE `1_duplicate_association` SET target_id = %d WHERE target_id = %d AND relation = 'DUPLICATE'",
                                $this->getGroup(), $groupDoublon));
    }
    public function getSourceInfo($source, $user_id, $client_id ){
        $sql = "SELECT * FROM 1_element_element WHERE source = '$source' AND target = 'client'  AND target_id = '$user_id::societe::$client_id' " ;
        $sourceInfo = $this->selectAll($sql);
        return $sourceInfo ;

    }

    public function getAttachedElements($target, $target_pattern = "") {
        if ($this->source == "BAT") 
        {
            $sql = sprintf("SELECT ee.* FROM `1_element_element`  AS ee INNER JOIN `1_gestion_bat` AS gb ON ee.source_id = gb.designed 
            WHERE gb.rowid = %s AND ee.source = 'mockup' AND ee.target = %s %s ",
            $this->quote($this->source_id),
            $this->quote($target),
            (!empty($target_pattern)) ? " AND ee.target_id LIKE ". $this->quote($target_pattern) : ""
            );        
        } else 
        $sql = sprintf("SELECT * FROM 1_element_element WHERE source = %s AND source_id = %s AND target = %s %s ", 
                        $this->quote($this->source),
                        $this->quote($this->source_id),
                        $this->quote($target),
                        (!empty($target_pattern)) ? " AND target_id LIKE ". $this->quote($target_pattern) : ""
                        ) ;
        $sourceInfo = $this->selectAll($sql);
        return $sourceInfo ;
    }
    // ajoute fonction pour creer le document dans gestion de documents
    public function createDocument($type) {
        $sql1 = "SELECT * FROM 0_document WHERE mockup_id = ".$this->source_id. " AND type = ".$this->quote($type);
        $ficheProd = $this->select($sql1);
        $idficheProd = intval($ficheProd->id);
        if  ($idficheProd == 0) {
         
            $sql = "INSERT INTO `0_document`(type, template, date_creation, user_id, status, reference, fields, mockup_id) 
                    VALUES(".$this->quote($type).", 1, NOW(), ". $this->quote($this->user_id).",0,'','',". $this->quote($this->source_id).")";
        
            $this->execute($sql);
            $lastInsertId = $this->pdo->lastInsertId();
            
            return $lastInsertId;
        }
        else
        return $idficheProd;

    }
    
    public function getIdFicheProduit($type) {
        $sql1 = "SELECT * FROM 0_document WHERE mockup_id = ".$this->source_id. " AND type = ".$this->quote($type);
        $ficheProd = $this->select($sql1);
        $idficheProd = intval($ficheProd->id);
        return $idficheProd;
    }
    public function createDocsforMockup($type, $mockup_id, $user_id) {         
            $sql = "INSERT INTO `0_document`(type, template, date_creation, user_id, status, reference, fields, mockup_id) 
                    VALUES(".$this->quote($type).", 1, NOW(), ". $this->quote($user_id).",0,'','',". $this->quote($mockup_id).")";
        
            $this->execute($sql);
            $lastInsertId = $this->pdo->lastInsertId();
            
            return $lastInsertId;
    }

    /*
    ** Dan layout system
    **
    */
    public function display($tmpl, $options = []) {
        ob_start();
            foreach ($options as $n => $v) 
                ${$n} = $v;
            include $_SERVER['DOCUMENT_ROOT'] . '/libraries/layout/'.$tmpl.'.php';
            $data		= ob_get_contents();
        ob_end_clean();
  
        return $data;
      } 
      public function getOrderItemId($order_id) {        

        $sql = " SELECT virtuemart_order_item_id FROM `digime_virtuemart_order_items` WHERE virtuemart_order_id = ".$order_id;
        return  $this->select($sql);    
        }

}
