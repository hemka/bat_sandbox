<?php

require_once 'SuperClass.php';

/* 
** Rappel des identifiers
UID::societe::rowid
UID::socpeople::rowid
UID::user::rowid
**
*/

class LDAPSP extends SuperClass {
    private $user_id;
    private $tables;

    public function __construct($user_id = 0) {
        $this->autoAuth("sp");

        $this->tables = array(
            'user', 'user_extrafields', 'usergroup', 'usergroup_user',
            'societe', 'socpeople'
        );

        $this->fetch($user_id);
    }

    public function fetch($user_id) {
        if ($user_id) {
            $this->setUserID($user_id);
            $this->initTables();
        }
    }

    public function setUserID($user_id) {
        $this->user_id = $user_id;
    }

    public function tablesInitialized() {
        $query = $this->execute("SHOW TABLES LIKE '" . $this->user_id . "_%'");
        return (count($query->fetchAll()) === count($this->tables));
    }

    private function initTables() {
        $created_tables = 0;
        if (!$this->tablesInitialized()) {
            foreach ($this->tables as $table) {
                $created_tables += intval(!!$this->pdo->query(
                    "CREATE TABLE IF NOT EXISTS `" . $this->user_id . "_$table` LIKE `master_$table`"
                ));
                
            }
        }

        return ($created_tables == count($this->tables));
    }
  
    public function deleteTables() {
        if ($this->tablesInitialized()) {
            foreach ($this->tables as $table) {
                $this->pdo->exec(
                    "DROP TABLE IF EXISTS `" . $this->user_id . "_$table`"
                );
            }
        }
    }

    public function getSpUser($dol_user_id) {
        return $this->select(
            "SELECT sp_id FROM `" . $this->user_id . "_user_extrafields`
            WHERE `fk_object` = :dol_user_id",
            array('dol_user_id' => $dol_user_id)
        );
    }

    public function getUserFromSP($sp_user_id) {
        $user = $this->select(
            "SELECT u.* FROM `" . $this->user_id . "_user_extrafields` AS ue
            INNER JOIN `" . $this->user_id . "_user` AS u
            ON ue.fk_object = u.rowid
            WHERE ue.sp_id = :sp_user_id",
            compact('sp_user_id')
        );

        return $user;
    }

    public function getUser($user_id) {
        $user = $this->select(
            "SELECT * FROM `" . $this->user_id . "_user` 
            WHERE `rowid` = :user_id", 
            array('user_id' => $user_id)
        );

        if ($user) {
            $sp_user = $this->getSpUser($user_id);
            $user->sp_id = ($sp_user) ? $sp_user->sp_id : 0;
        }

        return $user;
    }

    public function getUsers() {
        $users = $this->selectAll("SELECT * FROM `" . $this->user_id . "_user` ORDER BY `rowid`");

        if (!empty($users)) {
            foreach ($users as &$user) {
                $sp_user = $this->getSpUser($user->rowid);
                $user->sp_id = ($sp_user) ? $sp_user->sp_id : 0;
            }
        }

        return $users;
    }

    public function addUser($user_infos) {
        return $this->editUser($user_infos);
    }

    public function editUser($user_infos) {
        $upserts = array();

        foreach ($user_infos as $field => $info) {
            if ($field !== 'sp_id' && strpos($field, "perm") !== 0) {
                $upsert = array('value' => $info);

                if ($field == 'rowid') {
                    $upsert['key'] = true;
                }

                $upserts[$field] = $upsert;
            }
        }

        $user_id = $this->dxupsert($this->user_id . '_user', $upserts);

        if (!empty($user_infos['sp_id']) && $user_id) {
            $this->dxupsert($this->user_id . '_user_extrafields', array(
                'fk_object' => ['value' => $user_id, 'key' => true],
                'sp_id'     => ['value' => $user_infos['sp_id']]
            ));
        }

        return $user_id;
    }

    public function deleteUser($user_id) {
        if ($this->getUser($user_id)) {
            $success = true;
            if ($this->getSpUser($user_id))
                $success = $this->execute("DELETE FROM `" . $this->user_id . "_user_extrafields` WHERE `fk_object` = $user_id");

            return $success && $this->execute("DELETE FROM `" . $this->user_id . "_user` WHERE `rowid` = $user_id");
        }

        return false;
    }

    public function getUserGroups() {
        return $this->selectAll("SELECT * FROM `" . $this->user_id . "_usergroup` ORDER BY `nom`");
    }

    public function addUserGroup($usergroup) {
        return $this->editUserGroup($usergroup);
    }

    public function editUserGroup($usergroup) {
        $upserts = array();
        //! TODO
    }


    public function addClientContact($contact_infos) {
        return $this->editClientContact($contact_infos);
    }

    public function editClientContact($contact_infos) {
        $upserts = array();

        foreach ($contact_infos as $field => $info) {
            $upsert = array('value' => $info);

            /* if ($field == 'rowid' || preg_match('/fk_.+/', $field)) {
                $upsert['key'] = true;
            } */
            if ($field == 'rowid') {
                $upsert['key'] = true;
            }

            $upserts[$field] = $upsert;
        }

        return $this->dxupsert($this->user_id . '_socpeople', $upserts);
    }

    public function addClient($client_infos) {
        return $this->editClient($client_infos);
    }

    public function editClient($client_infos) {
        $upserts = array();
        foreach ($client_infos as $field => $info) {
            $upsert = array('value' => $info);

            if ($field == 'rowid') {
                $upsert['key'] = true;
            }

            $upserts[$field] = $upsert;
        }

        return $this->dxupsert($this->user_id . '_societe', $upserts);
    }

    public function deleteClient($client_id) {
        if ($client = $this->getClient($client_id, false)) {
            $success = true;

            if (!empty($client['socpeople'])) {
                $success = $success && $this->execute("DELETE FROM `" . $this->user_id . "_socpeople` WHERE `fk_soc` = $client_id");
            }

            return $success && $this->execute("DELETE FROM `" . $this->user_id . "_societe` WHERE `rowid` = $client_id");
        }

        return false;
    }

    public function getClient($client_id, $joined = true) {
        if ($joined) {
            $client = $this->select(
                "SELECT * 
                FROM `" . $this->user_id . "_societe` AS s
                LEFT JOIN `" . $this->user_id . "_socpeople` AS p
                ON s.rowid = p.fk_soc WHERE s.rowid = $client_id"
            );
        }
        else {
            $client =  array(
                'societe' => $this->select("SELECT * FROM `" . $this->user_id . "_societe` WHERE `rowid` = $client_id"),
                'socpeople' => $this->select(
                        "SELECT p.* FROM `" . $this->user_id . "_societe` AS s
                        LEFT JOIN `" . $this->user_id . "_socpeople` AS p
                        ON s.rowid = p.fk_soc WHERE s.rowid = $client_id"
                    )
            );

        }


        if (!empty($client->logo))
            $client->logo_url = "/images/clients/" . $this->DanXEncrypt($client_id)."/".$client->logo;
        $client->single_url = "/index.php?option=com_superpictor&view=infoclient&ucid=".$this->encrypt($this->user_id."::societe::$client_id",true);
        return $client;


    }

    public function getClientByIdentifier($identifier) {
        list($user_id,, $client_id) = explode('::', $identifier);

        if (intval($user_id) && intval($client_id)) {
            return $this->select("SELECT * FROM `" . $user_id . "_societe` WHERE `rowid` = $client_id");
        }

        return false;
    }

    public function getClients($client_id = 0, $dol_subuser_id = 0) {
        if (!$client_id)
            $client_id = $this->user_id;
            
        return $this->selectAll(
            "SELECT 
        s.rowid, s.nom, s.address, s.zip, s.town, s.fk_user_creat,
        p.firstname, p.lastname, p.email, p.poste, p.phone, CONCAT('$client_id', '::societe::', s.rowid) as identifier
        FROM `" . $client_id . "_societe` AS s
        LEFT JOIN `" . $client_id . "_socpeople` AS p
        ON s.rowid = p.fk_soc ". ( (!$dol_subuser_id) ? '' : 'WHERE s.fk_user_creat = '.$dol_subuser_id ) ." GROUP BY s.nom"
        ); 
    }
   
    public function getLastClientContactByIdentifier($identifier) {
        list($user_id,, $client_id) = explode('::', $identifier);

        if(intval($user_id) && intval($client_id)) {
            if (!$user_id)
            $user_id = $this->user_id;

            $sql = "SELECT * FROM `" . $user_id . "_socpeople` AS sp WHERE sp.fk_soc = " . $client_id . " ORDER BY `rowid` DESC LIMIT 1";
            
            return $this->select($sql);
        }

        return false;
    }
   
    public function getInfoClientContacts($client_id, $user_id = 0) {
        if (!$user_id)
            $user_id = $this->user_id;
        $sql =  "SELECT 
                sp.rowid, sp.firstname, sp.address, sp.zip, sp.town, sp.poste, sp.phone,
                sp.email, sp.lastname, sp.fk_soc as client_id, CONCAT('$user_id', '::societe::','$client_id', '::socpeople::', sp.rowid) as identifier
                FROM `" . $user_id . "_socpeople` AS sp          
                WHERE sp.fk_soc = $client_id";
        
       return $this->selectAll($sql);
       
    }
    public function getInfoUpdateContact($contact_id, $user_id = 0) {
        if (!$user_id)
            $user_id = $this->user_id;
        $sql =  "SELECT 
                sp.rowid, sp.firstname, sp.address, sp.zip, sp.town,
                sp.email, sp.lastname, sp.phone, sp.poste
                FROM `" . $user_id . "_socpeople` AS sp          
                WHERE sp.rowid = $contact_id"; 
       
       return $this->selectAll($sql);
       
    }
    
    public function addInfoClientContact($contact_infos) {
        $upserts = array();
        foreach ($contact_infos as $field => $info) {
            $upsert = array('value' => $info);

           /*  if ($field == 'rowid' || preg_match('/fk_.+/', $field)) {
                $upsert['key'] = true;
            } */
            if ($field == 'rowid' ) {
                $upsert['key'] = true;
            }

            $upserts[$field] = $upsert;
        }
        return $this->dxupsert($this->user_id . '_socpeople', $upserts);
    }
    public function deleteInfoClientContact($contact_id) {
                // return (true && $this->execute("DELETE FROM `" . $this->user_id . "_socpeople` WHERE `rowid` = $contact_id"));
                return  ($this->execute("DELETE FROM `" . $this->user_id . "_socpeople` WHERE `rowid` = $contact_id")) ? true : false;
    }
    public function getDolUserId($subUserID) {
        $sql = "SELECT fk_object FROM `" . $this->user_id . "_user_extrafields` AS ue WHERE ue.sp_id = " . $subUserID;
        return $this->select($sql);
    }    
}
