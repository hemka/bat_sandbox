<?php 
//Global PDO array
$allSuperDb = array();


if (!class_exists('spConfig'))
  require_once $_SERVER['DOCUMENT_ROOT'] . "/spconfig.php";


abstract class SuperClass {
    protected $pdo;
    private $method = 'AES-128-CTR';
    private $key    = "DN8-(LSC)-8AX";

    protected $lastQueries = [];
 
    protected function setKey($key) {
       if(ctype_print($key))
          $this->key = openssl_digest($key, 'SHA256', TRUE);
       else
          $this->key = $key;
    }
 
    private function iv_bytes() {
       return openssl_cipher_iv_length($this->method);
    }
 
    public  function encrypt( $data, $base64_encode = false) {
       $iv      = openssl_random_pseudo_bytes($this->iv_bytes());
       $crypted = bin2hex($iv) . openssl_encrypt($data, $this->method, $this->key, 0, $iv);
       return  ($base64_encode) ? base64_encode($crypted) : $crypted ;
    }
 
    public  function decrypt($data, $base64_decode = false) {
      if ($base64_decode)
        $data = base64_decode($data);
      $iv_strlen = 2 * $this->iv_bytes();

       if (preg_match("/^(.{" . $iv_strlen . "})(.+)$/", $data, $regs)) {
          list(, $iv, $crypted_string) = $regs;
 
          if(ctype_xdigit($iv) && strlen($iv) % 2 == 0)
             return openssl_decrypt($crypted_string, $this->method, $this->key, 0, hex2bin($iv));
       }
 
       return false;
    }
 
    public function urlEncrypt($key, $data) {
      $this->setKey($key);

      return base64_encode($this->encrypt($data));
    }
 
    public function urlDecrypt($key, $data) {
      $this->setKey($key);

      return $this->decrypt(base64_decode($data));
    }

    protected function setPDO($dsn, $usr, $pwd, $standalone = false) {

        $allPDO = $GLOBALS["allSuperDb"] = (isset($GLOBALS["allSuperDb"]) && is_array($GLOBALS["allSuperDb"])) ? $GLOBALS["allSuperDb"] : array() ;
        if(array_key_exists($dsn.$usr.$pwd, $allPDO)) {
          $pdo = $allPDO[$dsn.$usr.$pwd];
          $this->pdo = $allPDO[$dsn.$usr.$pwd];
        } else {
          $pdo = new \PDO($dsn, $usr, $pwd);
          $pdo->exec("SET NAMES utf8");
          $pdo->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);
          if (!$standalone)
            $this->pdo = $pdo;

          $GLOBALS["allSuperDb"][$dsn.$usr.$pwd] = $pdo;
        }

        return $pdo;
     }

    public function autoAuth($base_code) {
      $this->setPDO('mysql:host='.spConfig::getHost($base_code).';dbname='.spConfig::getDbname($base_code), spConfig::getUser($base_code), spConfig::getPwd($base_code));
    }

    public function selectAll($query, $params = []) {
        $result = $this->pdo->prepare($query);
        $result->execute($params);
        $this->logQuery($query, $params);
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

    private function logQuery($query, $params) {
      $this->lastQueries[] = [$query, $params];
    }

    public function dumpQueries($howmuch = 5, $return = false, $str =  "") {
      foreach ( array_slice($this->lastQueries, $howmuch * -1)  as  $value)
        $str  .= "<strong>Query from ".get_class($this)." instance:</strong> ". $value[0] . "\n" . (!empty($value[1]) ? "\n<strong>Params:</strong> ".$value[1]."\n\n" : "");

      if ($return)  return $str;
      else          printf("<pre>%s</pre>", $str);
    }

    public function select($query, $params = []) {
        $result = $this->pdo->prepare($query);
        $result->execute($params);
        $this->logQuery($query, $params);
        return $result->fetch(PDO::FETCH_OBJ);
    }

    public function execute($query, $params = []) {
        $result = $this->pdo->prepare($query);
        $result->execute($params);
        $this->logQuery($query, $params);
        return $result;
    }
    public function quote($str) {
        return $this->pdo->quote($str);
    }

    public static  function DanXEncrypt($pass, $it = 16) { // $it = le nombre de tour de cryptage
    for ($i = 0; $i < $it ; $i++) {
      if ($i % 3 == 0 || $i % 12 == 0)
        $pass = strrev($pass);
      else if ($i % 5 == 0) {
        $Tableau = explode("W", $pass);
        $Tableau = array_reverse($Tableau);
        $pass = implode("W", $Tableau);
      }
      else
        $pass = base64_encode($pass);
    }

    return $pass;
  }

    public static function DanXDecrypt($pass, $it = 16) {
      for ($i = $it - 1; $i >= 0 ; $i--) {
        if ($i % 3 == 0 || $i % 12 == 0)
          $pass = strrev($pass);
        else if ($i % 5 == 0) {
          $Tableau = explode("W", $pass);
          $Tableau = array_reverse($Tableau);
          $pass = implode("W", $Tableau);
        }
        else
          $pass = base64_decode($pass);
      }
      return $pass;
    }

    public function dxupsert($tablename, $upsertinfos, $verbose = 0, $checkonly = false) {
        $keywhere    = "";
        $coltab      = array();
        $valtab      = array();
        $combicolval = array();

        if (!function_exists("dxupsertlog")) {function dxupsertlog($var, $verbose = 0)  {if ($verbose)echo $var;}}

        foreach ($upsertinfos as $colname => $colinfo) {
            if (!is_object($colinfo))
              $colinfo = (object) $colinfo;
            $coltab[] = "`".$colname."`";
            $valtab[] = $this->pdo->quote($colinfo->value);
            if (!isset($colinfo->key) || !intval($colinfo->key))
              $combicolval[] = "`".$colname."` = " . $this->pdo->quote($colinfo->value);
            if ( isset($colinfo->key) && intval($colinfo->key) )
              $keywhere .= sprintf(" AND `$colname` = %s ", $this->pdo->quote($colinfo->value));
        }

        $resPrimary = $this->pdo->prepare("SELECT key_column_usage.column_name
                                      FROM   information_schema.key_column_usage
                                      WHERE  table_schema = schema()
                                      AND    constraint_name = 'PRIMARY'
                                      AND    table_name = '$tablename'");
                                      $resPrimary->execute();
        $primaryKeyColumnName =  $resPrimary->fetchColumn();

        $reqcheck = "SELECT $primaryKeyColumnName FROM `$tablename` WHERE 1";
        $result   = $this->pdo->prepare($reqcheck . $keywhere);

        dxupsertlog("dxupsert ---- requesting ".$reqcheck . $keywhere." \n", $verbose);
        $result->execute();
        if ($checkonly)
            return $result->fetchColumn();

        if (($primaryKey = $result->fetchColumn()) && !empty($keywhere)) {
          if (!empty($combicolval)) {
            $requpdate = sprintf("UPDATE `$tablename` SET %s WHERE 1 %s", implode(", ", $combicolval), $keywhere);
            dxupsertlog("dxupsert --FOUND on primary key $primaryKey -- requesting $requpdate \n", $verbose);
            $resup = $this->pdo->prepare($requpdate);
            $resup->execute();
          }

          return $primaryKey;
        } else {
          $reqinsert = sprintf("INSERT INTO `$tablename` (%s)  VALUES (%s)", implode(", ", $coltab), implode(", ", $valtab));

          $resins = $this->pdo->prepare($reqinsert);
          $resins->execute();
          $lastInsertId = $this->pdo->lastInsertId();
          dxupsertlog("dxupsert --NOT FOUND created on primary key $lastInsertId -- requesting $reqinsert \n", $verbose);
          return $lastInsertId;
      }
    }

    public function isLocalhost($superpictorOfficeWAN = false) {
      $whitelistAddr = ['127.0.0.1', '::1'];
      if ($superpictorOfficeWAN)
        $whitelistAddr[] = "109.190.128.0";
      return (in_array($_SERVER['REMOTE_ADDR'], $whitelistAddr)) ? true : false ;
    }

    public function sanitizeStr($str) {
       $accents = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ');
 
       $replacements = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
 
       return str_replace($accents, $replacements, $str);
    }
}