<?php
require_once $_SERVER["DOCUMENT_ROOT"]."/src/libraries/SuperClass.php";

class SuperLang extends SuperClass{

    const DEFAULT_LANG = 'fr_FR';
    const LANGS = ['fr_FR', 'en_US'];

    const REG_ARG = '/%arg-[0-9]*%/';

    public $default_sentences = [];
    public $sentences = [];
    public $request = [];
    public $lang;

    public function __construct($lang = self::DEFAULT_LANG){
        $this->lang = $lang;
        $this->sentences = $this->loadSentenceFromFile($this->lang);
        $this->request = $this->loadSentenceFromFile('data_bdd');
        if($this->lang != self::DEFAULT_LANG){     
            $sentence_default = $this->loadSentenceFromFile(self::DEFAULT_LANG);
            $this->sentences = array_unique(array_merge($sentence_default, $this->sentences), SORT_REGULAR);
        }
        
    }
    /**
     * Traduire la phrase numero X de "$this->sentences" avec les arguments $args ou X = 0 par defaut
     * @param array $args arguments qui vont remplacer les %arg-X% ou X est un nombre
     * @param int $sentencePosition position de la phrase a traduire dans le tableau $this->sentences
     * @return string Phrase avec les %arg-X% remplaces ou X est un nombre
    **/
    public function load($name){
        if(isset($this->sentences[$name])) {
            $sP = $this->sentences[$name];
            $args = func_get_args();
            $args = (sizeof($args) > 1) ? array_splice($args, 1) : [];
            return $this->replaceArgsInSentence($sP, $args);
        } else {
            $infos = explode('_', $name);
            $prefix = $infos[0];
            unset($infos[0]);
            $infos = implode('_', $infos);
            if (isset($this->request[$prefix])) {
                $this->autoAuth('products');
                $result = $this->select($this->request[$prefix][0], [":code" => $infos], true);
                return (isset($result->name) ? $result->name : $infos);
            } else
                return explode('_',$name)[1];
        }
    }

    private function replaceArgsInSentence($sentence, $args)
    {
        foreach($args as $key => $arg)
            $sentence = str_replace('%arg-'.($key+1).'%', $arg, $sentence);
        return preg_replace(self::REG_ARG, '', $sentence);
    }

    public function loadSentenceFromFile($lang){
        $filename = $_SERVER['DOCUMENT_ROOT'].'/locale/'.$lang.'.json';
        $file = fopen($filename, 'r');
        $contents = fread($file, filesize($filename));
        return json_decode($contents, true);
    }

    public function l(){
        return call_user_func_array(array($this, 'load'), func_get_args());
    }

}