<?php

Trait DB {
	private $db;
	private $isConnected;
	private $table_prefix;

	/**
	 * Connexion à la base de données Superpictor
	 *
	 * @param string $table_prefix, le préfixe de table à utiliser durant l'instance en cours
	 */
	public function connect($table_prefix) {
		if (!$this->isConnected) {
			try {
				$this->table_prefix = htmlspecialchars($table_prefix);
				if (!class_exists('spConfig'))
					require_once $_SERVER['DOCUMENT_ROOT'] . "/spconfig.php";
				$this->db = new PDO('mysql:host='.spConfig::getHost("sp").';dbname='.spConfig::getDbname("sp"), spConfig::getUser("sp"), spConfig::getPwd("sp"));
				$this->db->exec("SET CHARACTER SET utf8");
				$this->db->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_OBJ);

				$this->isConnected = true;
			} catch (\Exception $e) {
				$this->isConnected = false;
			}
		}
	}

	/**
	 * Sélectionne et retourne un jeu de données en fonction d'une table et d'une condition WHERE
	 *
	 * @param string $table, le nom de la table
	 * @param string $where, la condition WHERE
	 * @param array  $attributes, les attributs de la requête
	 * @param bool   $getAll, si true, va exécuter un fetchAll (sinon fetch)
	 *
	 * @return Array|Object
	 */
	public function get($table, $where, $attributes = [], $getAll = false) {
		return $this->query("SELECT * FROM `" . $this->table_prefix . "$table` WHERE $where", $attributes, $getAll);
	}

	/**
	 * Insert ou met à jour une entrée en base de données
	 *
	 * Si une clé "id" est présente dans le tableau $values, une requête UPDATE sera alors exécutée, sinon ce sera un INSERT
	 *
	 * @param string $table, le nom de la table
	 * @param array  $values, les valeurs relatives à la requête
	 *
	 * @return int le résultat de la méthode query, lastInsertId si la requête est un INSERT, sinon rowCount
	 */
	public function set($table, $values) {
		$cond = '';

		if (!empty($values['id'])) {
			$sql = "UPDATE `" . $this->table_prefix . "$table` SET ";

			$cond = " WHERE id = ". $values['id'];
			unset($values['id']);

			$i = 1;
			foreach ($values as $field => $value) {
				$sql .= "`$field` = " . $this->db->quote($value) . (($i < count($values)) ? ', ' : '');
				$i++;
			}
		}
		else {
			$sql = "INSERT INTO `" . $this->table_prefix . "$table` ";
			$sql .= '(' . implode(', ', array_keys($values)) . ')';
			$sql .= " VALUES ";

			$sql .= '(';
			foreach (array_values($values) as $key => $value)
			$sql .= $this->db->quote($value) . (($key + 1 < count($values)) ? ', ' : '');
			$sql .= ')';
		}

		$sql .= $cond;

		return $this->query($sql);
	}

	/**
	 * Supprimer une entrée de la base de données
	 *
	 * @param string $table, le nom de la table
	 * @param array  $id, la valeur du champ "id" utilisé dans la condition WHERE
	 *
	 * @return int le résultat de la méthode rowCount
	 */
	public function delete($table, $id) {
		return $this->query("DELETE FROM `" . $this->table_prefix . "$table` WHERE id = :id", compact('id'));
	}

	/**
	 * Exécute une requête en base de données
	 *
	 * @param string     $query, la requête préparée
	 * @param array      $attributes, le tableau d'arguments relatifs à la requête préparée
	 * @param bool       $getAll, si true, exécuteras la méthode fetchAll
	 * @param int|null   $fetchMode, constante PDO pour définier la méthode de fetch (par défaut PDO::FETCH_OBJ)
	 */
	private function query($query, $attributes = array(), $getAll = true, $fetchMode = null) {
		if (!$this->isConnected)
			return false;

		$isSelect = strpos($query, 'SELECT') !== false;
		$isInsert = strpos($query, 'INSERT INTO') !== false;
		$query = $this->db->prepare($query);

		if ($fetchMode)
			$query->setFetchMode($fetchMode);

		$query->execute($attributes);

		if ($isSelect) {
			if ($getAll)
				$result = $query->fetchAll();
			else
				$result = $query->fetch();
		}

		return (($isSelect) ? ($result ?: false) : (($isInsert) ? $this->db->lastInsertId() : !!$query->rowCount()));
	}
}