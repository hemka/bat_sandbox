<?php

require_once __DIR__ . "/DB.trait.php";

class Permission {
   use DB;

   /**
    * Constructeur
    */
   public function __construct() {
      $this->connect('1_perm_');
   }

   /**
    * Sélectionne et retourne les permissions
    *
    * @return array
    */
   public function getPermissions() {
      return $this->query("SELECT * FROM `1_perm_permission` ORDER BY name");
   }

   /**
    * Sélectionne et retourne les catégorie de permissions, ainsi que les permissions relatives à chaque catégorie
    *
    * @return array
    */
   public function getPermissionsWithCategory() {
      $categories = $this->get("category", "1 ORDER BY name", [], true);

      if ($categories) {
         foreach ($categories as &$category) {
            $permissions = $this->get('permission', 'category_id = :cid ORDER BY code', array('cid' => $category->id), true);

            $category->permissions = $permissions;
         }
      }

      return $categories;
   }

   /**
    * Sélectionne et retourne une permission en fonction de son code
    *
    * @param string $code, le code de la permission
    *
    * @return Object
    */
   public function getPermissionByCode($code) {
      return $this->get("permission", "code = :code", compact('code'));
   }

   /**
    * Sélectionne et retourne les compteur
    *
    * @return array
    */
    public function getCounters() {
      return $this->query("SELECT * FROM `1_perm_counter` ORDER BY name");
   }

   /**
    * Sélectionne et retourne les compteurs, ainsi que les permissions relatives à chaque compteur
    *
    * @return array
    */
   public function getCountersWithPermissions() {
      $counters = $this->get("counter", "1 ORDER BY name", [], true);

      if ($counters) {
         foreach ($counters as &$counter) {
            $permissions = array();
            $c_perms = $this->get('counter_permission', 'counter_id = :cid', array('cid' => $counter->id), true);

            if ($c_perms) {
               foreach ($c_perms as $cp) {
                  $permission = $this->get('permission', 'id = :id', array('id' => $cp->permission_id));

                  if ($permission) {
                     $permission->counter_permission_id = $cp->id;

                     array_push($permissions, $permission);
                  }
               }
            }

            $counter->permissions = $permissions;
         }
      }

      return $counters;
   }

   /**
    * Sélectionne et retourne un compteur en fonction de son code
    *
    * @param string $code, le code du compteur
    *
    * @return Object
    */
   public function getCounterByCode($code) {
      return $this->get("counter", "code = :code", compact('code'));
   }

   /**
    * Sélectionne et retourne un compteur en fonction d'une permission
    *
    * @param int $p, l'id de la permission
    *
    * @return Object
    */
   public function getCounterByPermission($p) {
      return $this->get("counter_permission", "permission_id = :p", compact('p'));
   }

   /**
    * Vérifie si une permission existe en fonction de son code
    *
    * @param string $code, le code de la permission
    *
    * @return bool
    */
   public function permissionExists($code) {
      return !!$this->getPermissionByCode($code);
   }

   /**
    * Vérifie si un groupe d'utilisateurs a accès à une permission
    *
    * @param int $ug, l'id du groupe d'utilisateurs
    * @param int $p, l'id de la permission
    *
    * @return bool
    */
   public function userGroupHasPermission($ug, $p) {
      return !!$this->get('usergroup_permission', 'usergroup_id = :ug AND permission_id = :p', compact('ug', 'p'));
   }

   /**
    * Sélectionne et retourne le ou les groupes d'utilisateurs en fonction, ou non, d'un id de groupe d'utilisateurs
    *
    * @param int|null $id, l'id du groupe d'utilisateurs
    *
    * @return Array|Object
    */
   public function getUserGroups($id = null) {
      $sql =
         "SELECT virtuemart_shoppergroup_id AS id,
         shopper_group_name AS name,
         shopper_group_desc AS description
         FROM `digime_virtuemart_shoppergroups`"
      ;

      $sql .= ($id) ? " HAVING id = $id" : '';
      $sql .= " ORDER BY name";

      return $this->query($sql, [], ($id ? false : true));
   }

   /**
    * Sélectionne et retourne le ou les utilisateurs en fonction, ou non, d'un id d'utilisateur
    *
    * @param int|null $id, l'id de l'utilisateur
    *
    * @return Array|Object
    */
   public function getUsers($id = null) {
      $sql =
         "SELECT virtuemart_user_id AS id, company, name
         FROM `digime_virtuemart_userinfos`"
      ;

      $sql .= ($id) ? " HAVING id = $id" : '';
      $sql .= " ORDER BY name";

      return $this->query($sql, [], ($id ? false : true));
   }

   /**
    * Sélectionne et retourne les permissions d'un groupes d'utilisateurs
    *
    * @param int $ug, l'id du groupe d'utilisateurs
    *
    * @return Array
    */
   public function getUserGroupPermissions($ug) {
      return $this->get('usergroup_permission', "usergroup_id = :ug GROUP BY permission_id", compact('ug'), true);
   }

   /**
    * Sélectionne et retourne une permission d'un utilisateur
    *
    * @param int  $u, l'id de l'utilisateur
    * @param int  $p, l'id de la permission
    *
    * @return Object
    */
   public function getUserPermission($u, $p) {
      return $this->get('user_permission', 'user_id = :u AND permission_id = :p', compact('u', 'p'));
   }
   /**
    * Sélectionne et retourne une permission d'un utilisateur en fonction d'un compteur
    *
    * @param int  $u, l'id de l'utilisateur
    * @param int  $p, l'id du compteur
    *
    * @return Object
    */
   public function getUserPermissionByCounter($u, $c) {
      $query = 
         "SELECT 
            up.id, up.user_id, up.permission_id, cp.counter_id, 
            up.user_actual_counter, up.user_counter_allowed, up.reset_date, up.allowed
         FROM `1_perm_user_permission` AS up
         INNER JOIN `1_perm_counter_permission` AS cp
         ON up.permission_id = cp.permission_id
         WHERE up.user_id = :u AND cp.counter_id = :c"
      ;

      $u_perm = $this->query($query, compact('u', 'c'), false);

      if (!$u_perm) {
         $ugs = $this->getUserUserGroups($u);
         $counter = $this->get('counter', 'id = :c', compact('c'));

         foreach ($ugs as $ug) {
            $ugp = $this->get('usergroup_permission', 'usergroup_id = :ugid', ['ugid' => $ug->id]);

            if ($ugp) {
               $counter = $this->getCounterByPermission($ugp->permission_id);

               if ($counter && $counter->counter_id == $c) {
                  $this->checkCounterPermissionForUser($u, $ugp->permission_id);
                  break;
               }
            }
         }

         $u_perm = $this->query($query, compact('u', 'c'), false);
      }

      return $u_perm;
   }

   /**
    * Sélectionne et retourne les permissions d'un utilisateur
    *
    * @param int  $u, l'id de l'utilisateur
    *
    * @return Array
    */
   public function getUserPermissions($u) {
      return $this->get('user_permission', 'user_id = :u', compact('u'), true);
   }

   /**
    * Affecte des permissions à un groupe d'utilisateurs
    *
    * @param int   $ug, l'id du groupe d'utilisateurs
    * @param array $ps, le tableau d'id des permissions
    *
    * @return void
    */
   public function setUserGroupPermissions($ug, $ps) {
      $perms = $this->getUserGroupPermissions($ug);

      foreach ($perms as $perm) {
         if (!in_array($perm->permission_id, array_keys($ps))) {
            $this->delete('usergroup_permission', $perm->id);
            unset($ps[$perm->permission_id]);
         }
      }

      foreach ($ps as $real_p => $allowed) {
         if (!$this->userGroupHasPermission($ug, $real_p)) {
            $this->set('usergroup_permission', array(
               'usergroup_id' => $ug,
               'permission_id' => $real_p
            ));
         }
      }
   }

   /**
    * Autorise ou non un utilisateur l'accès à une permission
    *
    * @param int  $u, l'id de l'utilisateur
    * @param int  $p, l'id de la permission
    * @param bool $p, si true, autorise l'accès à la permission
    *
    * @return Array|Object
    */
   public function allowPermissionForUser($u, $p, $allowed = true) {
      $ugs = $this->getUserUserGroups($u);
      $userPerm = $this->getUserPermission($u, $p);
      $fields = array(
         'user_id'        => $u,
         'permission_id'  => $p,
         'allowed'        => $allowed
      );

      if ($userPerm)
         $fields['id'] = $userPerm->id;

      if ($ugs)
         foreach ($ugs as $ug)
            if ($this->userGroupHasPermission($ug->id, $p))
               if ($userPerm && $allowed)
                  return $this->delete('user_permission', $userPerm->id);

      return $this->set('user_permission', $fields);
   }

   /**
    * Affecte des permissions à un utilisateur
    *
    * @param int   $u, l'id de l'utilisateur
    * @param array $ps, le tableau d'id des permissions
    *
    * @return void
    */
   public function setUserPermissions($u, $ps) {
      $ugs = $this->getUserUserGroups($u);

      $fields = array(
         'user_id'       => $u,
         'permission_id' => 0,
         'allowed'       => false
      );

      if (($ups = $this->getUserPermissions($u))) {
         foreach ($ups as $up) {
            if (!in_array($up->permission_id, array_keys($ps))) {
               foreach ($ugs as $ug) {
                  if (!$this->userGroupHasPermission($ug->id, $up->permission_id)) {
                     $this->delete('user_permission', $up->id);
                     unset($ps[$up->permission_id]);
                  }
               }
            }
         }
      }

      if ($ugs) {
         foreach ($ugs as $ug) {
            if ($ugps = $this->getUserGroupPermissions($ug->id)) {
               foreach ($ugps as $ugp) {
                  $fields['permission_id'] = $ugp->permission_id;
                  $fields['allowed'] = false;

                  $up = $this->getUserPermission($u, $ugp->permission_id);
                  $perm = $this->get('permission', 'id = :id', array('id' => $ugp->permission_id));

                  if ($up) $fields['id'] = $up->id;
                  else unset($fields['id']);

                  if (!$perm->has_counter && !in_array($ugp->permission_id, array_keys($ps)))
                     $this->set('user_permission', $fields);
                  else {
                     if ($up && $up->allowed == 0) {
                        $fields['allowed'] = true;

                        $this->set('user_permission', $fields);
                     }
                  }

                  // if (!$perm->has_counter)
                  //    unset($ps[$ugp->permission_id]);
               }
            }
         }
      }

      foreach ($ps as $p => $value) {
         $fields = array(
            'user_id'       => $u,
            'permission_id' => $p,
         );

         if (is_array($value))
            $fields = array_merge($fields, $value);
         else
            $fields['allowed'] = !!$value;

         $up = $this->getUserPermission($u, $p);
         if ($up) $fields['id'] = $up->id;
         else unset($fields['id']);

         $this->set('user_permission', $fields);
      }
   }

   /**
    * Vérifie si un utilisateur est autorisé à accéder à une permission
    *
    * Cette fonction retourne un booléen en fonction de son entrée en base de données, ou null si aucune entrée n'a été détectée
    *
    * @param int  $u, l'id de l'utilisateur
    * @param int  $p, l'id de la permission
    * @param bool $check_counter, dans le cas d'une permission limitée par un compteur, spécifie s'il faut vérifier le compteur de l'utilisateur
    *
    * @return bool|null
    */
   public function userIsPermitted($u, $p, $check_counter = true) {
      $u_perm = $this->getUserPermission($u, $p);

      if ($check_counter) {
         if ($perm = $this->get('permission', 'id = :p', compact('p'))) {
            if ($perm->has_counter) {
               return $this->checkCounterPermissionForUser($u, $p);
            }
         }
      }

      return ($u_perm) ? ($u_perm->allowed == '1') : null;
   }

   /**
    * Vérifie qu'un utilisateur a accès à une permission limitée par un compteur
    *
    * Si un compteur n'est pas encore initialisé pour un utilisateur, ce dernier le sera.
    * Réinitialise la date de renouvellement si celle-ci est dépassée
    *
    * @param int  $u, l'id de l'utilisateur
    * @param int  $p, l'id de la permission
    *
    * @return bool
    */
   public function checkCounterPermissionForUser($u, $p) {
      $u_perm = $this->get('user_permission', 'user_id = :u AND permission_id = :p', compact('u', 'p'));
      $counter = $this->get('counter_permission', 'permission_id = :p', compact('p'));

      if ($counter) {
         preg_match('/(\d{1})(J|M|A){1}/i', $counter->duration_pattern, $matches);
         list(, $nb, $type) = $matches;
         $nb = intval($nb);
         $type = strtoupper($type);

         $fields = array(
            'user_id'              => $u,
            'permission_id'        => $p,
            'user_actual_counter'  => 0,
            'user_counter_allowed' => $counter->counter,
            'reset_date'           => $this->getResetDate($nb, $type),
            'allowed'              => ($u_perm) ? !!$u_perm->allowed : true
         );
         if (!$u_perm || ($u_perm && !$u_perm->reset_date)) {
            if ($this->userHasPermission($u, $p, false)) {
               if ($u_perm)
                  $fields['id'] = $u_perm->id;

               $q_return = $this->set('user_permission', $fields);
               $fields['id'] = ($u_perm) ? $u_perm->id : $q_return;

               $u_perm = (object)$fields;
            }
         }

         if ($u_perm) {
            $now           = date_create(date('Y-m-d'));
            $reset_date    = date_create($u_perm->reset_date);
            $date_interval = date_diff($now, $reset_date);

            if (intval($date_interval->format('%R%a')) >= 0) {
               if ($counter->counter > $u_perm->user_counter_allowed) {
                  $u_perm->user_counter_allowed = $counter->counter;

                  $this->set('user_permission', (array)$u_perm);
               }
               
               if ($u_perm->user_actual_counter >= $u_perm->user_counter_allowed)
                  $u_perm->allowed = false;
            }
            else {
               $new_reset_date = $this->getResetDate($nb, $type, $reset_date);

               $u_perm->user_actual_counter = 0;
               $u_perm->reset_date = $new_reset_date;

               $this->set('user_permission', (array)$u_perm);
            }
         }
      }

      return ($u_perm) ? !!$u_perm->allowed : null;
   }

   /**
    * Obtient la prochaine date de réinitialisation pour les permissions limitée par un compteur
    *
    * Si aucune valeur n'est spécifiée pour le paramètre $from_date, la date d'aujourd'hui sera utilisée
    *
    * @param int        $nb, la valeure numérique du pattern de durée
    * @param string     $type, le type du pattern de durée
    * @param DateTime   $from_date, la date à partir de laquelle calculer l'interval
    *
    * @return string
    */
   private function getResetDate($nb, $type, $from_date = null) {
      $from_day   = ($from_date) ? $from_date->format('d') : date('d');
      $from_month = ($from_date) ? $from_date->format('m') : date('m');
      $from_year  = ($from_date) ? $from_date->format('Y') : date('Y');
      
      $day   = intval($from_day)    + (($type == 'J') ? $nb : 0);
      $month = intval($from_month)  + (($type == 'M') ? $nb : 0);
      $year  = intval($from_year)   + (($type == 'A') ? $nb : 0);

      return date('Y-m-d', mktime(0, 0, 0, $month, $day, $year));
   }

   /**
    * Sélectionne et retourne les groupes d'utilisateurs relatifs à un utilisateur
    * 
    * @param int $u, l'id de l'utilisateur
    * 
    * @return array
    */
   public function getUserUserGroups($u) {
      $ugs = $this->query(
         "SELECT virtuemart_shoppergroup_id AS id
         FROM `digime_virtuemart_vmuser_shoppergroups`
         WHERE virtuemart_user_id = $u"
      );

      if ($ugs) {
         foreach ($ugs as &$ug) {
            $uug = $this->getUserGroups($ug->id);
            $ug->name = $uug->name;
         }
      }

      return $ugs;
   }

   /**
    * Vérifie si un utilisateur est autorisé à accéder à une permission
    *
    * Cette fonction, contrairement à userIsPermitted, vérifie aussi les droits d'accès du groupe auquel l'utilisteur appartient
    *
    * @param int $u, l'id de l'utilisateur
    * @param int $p, l'id de la permission
    * @param bool $check_counter, dans le cas d'une permission limitée par un compteur, spécifie s'il faut vérifier le compteur de l'utilisateur
    *
    * @return bool
    */
   public function userHasPermission($u, $p, $check_counter = true) {
      $isPermitted = $this->userIsPermitted($u, $p, $check_counter);
      if ($isPermitted !== null)
         return $isPermitted;

      $hasPermission = false;
      $ugs = $this->getUserUserGroups($u);
      if ($ugs)
         foreach ($ugs as $ug)
            $hasPermission = $hasPermission || $this->userGroupHasPermission($ug->id, $p);

      return $hasPermission;
   }

   /**
    * Vérifie qu'une permission liée à un utilisateur est héritée du groupe auquel appartient l'utilisateur
    *
    * @param int $u, l'id de l'utilisateur
    * @param int $p, l'id de la permission
    *
    * @return bool
    */
   public function isPermissionInherited($u, $p) {
      $isPermitted = $this->userIsPermitted($u, $p, false);
      if ($isPermitted !== null)
         return false;

      return true;
   }
}
