<?php 
require_once ("classDocument.php");
if (!class_exists('SuperClient'))
  require_once "$root/src/libraries/SuperClient.php";

if (!class_exists('SuperLang'))
  require_once "$root/src/libraries/SuperLang.php";

class SuperProductSheet extends SuperDocument { 
    // public  $id;
    // public  $type;
    /* public  $user_id;
    public  $ref;*/
    public  $doc_infos;
    public  $name;  
    public  $fields = array(); 
    public function __construct($idProdSheet = null)
    {
        parent::__construct($idProdSheet); 
        $this->promotePDO();
        $this->execute('USE templateDB');
        if ($this->id != null) 
            $this->init();         
        
    }
    public function init() {
        $IO_props = [ "name", "desc" => "description", "size" => "sizes", "category" => "categories", "color" => "colors", "current_color" => "model", "brand" ];
        $IO_spec_chars = ["brand", "description"];
        $this->doc_infos = $this->getDocInfos();
        $doc_pdt = $this->doc_infos->product;
        $user = new SuperClient($this->doc_infos->user->owner_userid, SP_INFOS);
        if (!empty($this->doc_infos)) {
          foreach ($this->doc_infos->views as $it_view => $viewurl) {
            $view = new stdClass();
            $view->photo_produit = ($doc_pdt instanceof SuperProduct) ? "$viewurl" : "/src/photoProduits/" . $viewurl;
            $view->{"picture-label"} = (!isset($this->doc_infos->views_infos)) ? "" : $this->doc_infos->views_infos[$it_view]->name;
            $this->io_addStackable("produit", $view);
          }

          foreach($IO_props as $key => $prop) {
            $IO_name = (!empty($key) && !is_int($key)) ? $key : $prop;
            $IO_value = ($doc_pdt instanceof SuperProduct) ? $doc_pdt->getProps($prop) : $doc_pdt->{$prop};
            $IO_value = (array_search($prop, $IO_spec_chars) !== false) ? htmlspecialchars($IO_value, ENT_QUOTES) : $IO_value;
            $this->io_addStatic("infoproduct", "product_$IO_name", $IO_value);
          }

          $this->io_addStatic("infoclient", "client_company", $user->infos->company);
          $this->io_addStatic("infoclient", "client_name", $user->infos->last_name . ' ' . $user->infos->first_name);
          $this->io_addStatic("infoclient", "client_logo", 'https://www.superpictor.com' . $user->user_logo->file_path);
        }

        $editables = $this->getTemplateEditableFields();
        $infosFP = $this->getInfosFP();
        $oldinfosFP = $this->getInfosFP();
        $infosFP->fields = json_decode($infosFP->fields);
        foreach ($editables as $category => $editable) {
          if($category === 'infoclient') continue;
          foreach($editable as $key => $content) {
              if(!$infosFP->fields->$key) {
                // if($content->type !== 'text') continue;
                if(empty($infosFP->fields)) $infosFP->fields = new stdClass();
                $infosFP->fields->$key = new stdClass();
                $infosFP->fields->$key->value = $this->io['statics'][$category][$key]->content;
                $infosFP->fields->$key->hidden = false;
              }
          }
        }
        
        if($oldinfosFP->fields != $infosFP->fields) {
          $this->initInfos(json_encode($infosFP->fields));
        }
    }
    public function getInfosFP()
    {
        return $this->select(
            "SELECT id, d.reference, gb.product_name, d.fields, d.status FROM `templateDB`.`0_document` AS d
            JOIN `templateDB`.`1_gestion_bat` AS gb
            ON d.source_id = gb.designed
            WHERE id = ".$this->id
        );     
        
    }

    

    public function updateInfoFP($values) {
        $jsonFields = json_encode($values);

    $this->execute(
      "UPDATE `templateDB`.`0_document`
      SET fields = :fields
      WHERE id = :id",
      array(
        'fields' => $jsonFields,
        'id' => $this->id
      )
    );
    $this->execute(
      "UPDATE `templateDB`.`1_gestion_bat`
      SET product_name = :product_name
      WHERE designed = :mockup_id",
      array(        
        'product_name' => $values->product_name['value'],
        'mockup_id' => $this->mockup_id
      )
    );
    }
    public function getInfoBat() {
      return $this->select(
        "SELECT rowid FROM `templateDB`.`1_gestion_bat` 
        WHERE designed = ".$this->mockup_id
    );
    }

    public function updateTemplate($template) {
      return $this->execute(
        "UPDATE `templateDB`.`0_document`
        SET template = :template
        WHERE id = :id",
        array(
          'template' => $template,
          'id' => $this->id
        )
        );
    }
}
?>