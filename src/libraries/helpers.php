<?php

// require $_SERVER['DOCUMENT_ROOT'].'/components/com_bat/helpers/db.php';

if (!function_exists("DanXEncrypt")) {
  function DanXEncrypt ($pass, $it = 16) { // $it = le nombre de tour de cryptage
    for ($i = 0; $i < $it ; $i++) {
      if ($i % 3 == 0 || $i % 12 == 0)
        $pass = strrev($pass);
      else if ($i % 5 == 0) {
        $Tableau = explode("W", $pass);
        $Tableau = array_reverse($Tableau);
        $pass = implode("W", $Tableau);
      }
      else
        $pass = base64_encode($pass);
    }

    return $pass;
  }
}

if (!function_exists("DanXDecrypt")) {
  function DanXDecrypt ($pass, $it = 16) {
    for ($i = $it - 1; $i >= 0 ; $i--) {
      if ($i % 3 == 0 || $i % 12 == 0)
        $pass = strrev($pass);
      else if ($i % 5 == 0) {
        $Tableau = explode("W", $pass);
        $Tableau = array_reverse($Tableau);
        $pass = implode("W", $Tableau);
      }
      else
        $pass = base64_decode($pass);
    }

    return $pass;
  }
}

function getBATPreview($user, $id_bat, $no = null) {
  require $_SERVER['DOCUMENT_ROOT'].'/designer/classMaquetor.php';
  $maquetor= new Maquetor($id_bat, $pdo, $user);

	$idbat = DanXDecrypt($_GET['idbat']);
	$infos = $maquetor->getInfos() ;
	$maquetor->loadTemplate('template1') ;

	return	$maquetor->displayMiniature($no);
}

function nameToSlug($name) {
  $search  = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,e,i,ø,u");
  $replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,e,i,o,u");

  $slug = str_replace($search, $replace, $name);
  $slug = str_replace(' ', '', $slug);

  return strtolower($slug);
}

?>
