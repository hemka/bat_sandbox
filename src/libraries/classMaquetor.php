<?php
// error_reporting(E_ALL); // Error engine
// ini_set('display_errors', TRUE); // Error display
// ini_set('log_errors', TRUE); // Error logging
// ini_set("xdebug.var_display_max_children", '-1');
// ini_set("xdebug.var_display_max_data", '-1');
// ini_set("xdebug.var_display_max_depth", '-1');

// !  attention a changer, valeur augmentee uniquement pour les ancienne creations
ini_set('memory_limit','512M');

$root = realpath($_SERVER["DOCUMENT_ROOT"]);
require($root.'/src/libraries/simple_html_dom.php');

require($root.'/src/vendor/autoload.php');
require $root."/src/style/scssphp-1.1.0/scss.inc.php";
use ScssPhp\ScssPhp\Compiler;
use \mikehaertl\wkhtmlto\Pdf;
use \mikehaertl\wkhtmlto\Image;

if (!class_exists('SuperClient'))
	require_once "$root/src/libraries/SuperClient.php";
if (!class_exists('LDAPSP'))
	require_once "$root/src/libraries/LDAPSP.php";
if (!class_exists('SuperColor'))
	require_once "$root/src/libraries/SuperColor.php";

require_once $_SERVER['DOCUMENT_ROOT'].'/spconfig.php';

class Maquetor
{
	public $infos = array()  ;
	public $templateSlug ;
	public $templateId;
	public $idbat ;
	public $userid ;
	public $ref_bat = "";
	private $base ;
	public $hiddenelmts  ;
	public $userimg ;
	public $miniatures = array() ;
	public $nbpages ;
	public $context = 'client';
	public $editedFields;

	public $io_in;
	public $io_variables = array();
	public $io_imagepath = "";

	function __construct($idbat, $base = 0, $userid = 0, $slug = ''){
		$this->idbat = $idbat ;
		$this->base = ($base) ? $base :  new PDO('mysql:host='.spConfig::getHost('sp').';dbname='.spConfig::getDbname('sp').';', spConfig::getUser('sp'), spConfig::getPwd('sp'));
		$this->templateSlug = 'ls_superpictor'; // Template par defaut du systeme
		$this->templateId = 9;
		$this->templateOrientation = 'Landscape';
		$this->userid = $userid;


		$this->initTemplateChoice();

		if ($idbat)
			$this->initSuperPictorInfos();
	}

	public function initTemplateChoice() {
		$template_bat_q = $this->execute(
			"SELECT
			bt.id as t_id,
			bt.slug as t_slug,
			bt.orientation as t_orientation, 
			gb.template
			FROM `1_gestion_bat` AS gb
			INNER JOIN `1_bat_templates` AS bt
			ON gb.template = bt.id
			WHERE gb.rowid = ".$this->idbat
		);
		if ($template = $template_bat_q->fetch(PDO::FETCH_OBJ)) {
			// $this->templateSlug = $template->t_slug;
			$this->templateId = $template->t_id;
			$this->templateOrientation = $template->t_orientation;
		}
		else {
			$user_template_q = $this->execute(
				"SELECT bt.id, bt.slug, bt.orientation, gb.template
				FROM `1_bat_templates` AS bt
				INNER JOIN `1_gestion_bat` as gb
				INNER JOIN `1_user_template` AS ut
				ON bt.id = ut.template
				WHERE ut.user = ".$this->userid." AND ut.user != 0 AND gb.rowid = $this->idbat
				ORDER BY bt.id"
			);

			$id_template = null;
			if ($user_template = $user_template_q->fetch(PDO::FETCH_OBJ)) {
				$id_template = $user_template->id;
				$this->templateId = $user_template->id;
				// $this->templateSlug = $user_template->slug;
				$this->templateOrientation = $user_template->orientation;
			}
			else {
				$default_template_q = $this->execute("SELECT id FROM `1_bat_templates` WHERE slug = '$this->templateSlug'");
				if ($default_template = $default_template_q->fetch(PDO::FETCH_OBJ)) {
					$id_template = $default_template->id;
				}
			}

			if ($id_template) {
				$this->execute("UPDATE `1_gestion_bat` SET template = $id_template WHERE rowid = $this->idbat");
			}
		}
	}

	public  function initSuperPictorInfos() {
		$infostemplate = $this->SelectMoiCaOBJ(
			"SELECT * FROM 1_gestion_bat as bat
			JOIN 1_designed as design ON bat.designed = design.rowid
			WHERE bat.rowid = ".$this->idbat
		);
		$this->userid             = ($this->userid) ?: $infostemplate[0]->idclient;
		$this->date_edited        = $infostemplate[0]->date_modif;
		$this->date_genthumbnails = $infostemplate[0]->date_genthumbnails;
		$this->userid             = ($this->userid) ?: $infostemplate[0]->idclient;
		$optionsdecode            = json_decode($infostemplate[0]->options) ;
		$this->ref_bat            = $infostemplate[0]->reference;
		$this->hiddenelmts       =  (isset($optionsdecode->hiddenelmts)) ? $optionsdecode->hiddenelmts : []  ;
		$this->userimgs          = (isset($optionsdecode->uploadimg)) ? $optionsdecode->uploadimg : [] ;

		$this->io_in = json_decode($infostemplate[0]->zonesjson);


		$fields = $this->SelectMoiCaOBJ(
			"SELECT fields FROM 1_infos_bat WHERE idbat = ".$this->idbat
		);
		$this->editedFields = json_decode($fields[0]->fields);


		$revendeurInfos = null;
		if ($this->userid) {
			$revendeurInfos = $this->SelectMoiCaOBJ('SELECT * FROM `digime_users` as du
													JOIN `digime_virtuemart_userinfos` as dvu
													ON dvu.virtuemart_user_id = du.id
													LEFT JOIN `1_user_image` as img
													ON img.user = du.id
													WHERE du.id = ' . $this->userid . '
													ORDER BY du.`id`  ASC');
			if (!empty($revendeurInfos)) {
				$revendeurInfos = $revendeurInfos[0];
				if (!empty($revendeurInfos->file_path))
					$revendeurInfos->user_logo = "https://www.superpictor.com/".$revendeurInfos->file_path;
			}
		}
		$this->io_variables["inforevendeur"] = $revendeurInfos;

		//? AFFECTATION COMMERCIAL
		$user_q = $this->execute(
			"SELECT ui.virtuemart_user_id AS id, ui.first_name, ui.last_name
			FROM `1_element_element` AS ee
			INNER JOIN `digime_virtuemart_userinfos` AS ui 
			ON ee.source_id = ui.virtuemart_user_id
			WHERE `source` = 'user' AND `target` = 'bat' AND `target_id` = $this->idbat"
		);
		if ($user = $user_q->fetch(PDO::FETCH_OBJ)) {
			$contact = new stdClass();
			$contact->commercial = "$user->first_name $user->last_name";

			$this->io_variables['contact'] = $contact;
		}

		//? AFFECTATION CLIENT

		$sql_idmc = $this->execute("SELECT `designed` AS id_mockup FROM `1_gestion_bat` WHERE rowid = $this->idbat");



		if ($id_mockup = $sql_idmc->fetch(PDO::FETCH_OBJ)) {
			$client_q = $this->execute(
				"SELECT `target_id` AS identifier FROM `1_element_element`
				WHERE `source` = 'mockup' AND `source_id` = " . intval($id_mockup->id_mockup) . " AND `target` = 'client'"
			);

			if ($client = $client_q->fetch(PDO::FETCH_OBJ)) {
				$ldap = new LDAPSP();
				$client_ldap = $ldap->getClientByIdentifier($client->identifier);
				$client_contact_ldap = $ldap->getLastClientContactByIdentifier($client->identifier);

				if ($client_ldap) {
					$io_client = new stdClass();
					$io_client->name = $client_ldap->nom;

					if($client_contact_ldap) {
						$io_client->lastname = $client_contact_ldap->lastname;
						$io_client->firstname = $client_contact_ldap->firstname;
						$io_client->phone = $client_contact_ldap->phone;
						$io_client->email = $client_contact_ldap->email;

						if($client_contact_ldap->address && $client_contact_ldap->town && $client_contact_ldap->zip) {
							$io_client->address = $client_contact_ldap->address;
							$io_client->town = $client_contact_ldap->town;
							$io_client->zip = $client_contact_ldap->zip;
						} else {
							$io_client->address = $client_ldap->address;
							$io_client->town = $client_ldap->town;
							$io_client->zip = $client_ldap->zip;
						}



					}

					$this->io_variables['client'] = $io_client;
				}
			}
		}
	}

	public function setContext($context) {
		$this->context = $context;
	}

	public function displayPage($numpage){
		$sizeproducts = sizeof($this->template->htmlgen["page_produits"]) ;

		if( $numpage < ($sizeproducts + 1) )
			$page = $this->template->htmlgen["page_produits"][intval($numpage - 1)] ;
		else
			$page= $this->template->htmlgen["page_emplacements"][intval($numpage - intval($sizeproducts) - 1  )];
		$page = $this->clearHTML($page) ;

		return $page;
	}

	public function numOfPage() {
		$nbPages = 0;
		foreach ($this->template->htmlgen as $key => $html) {
			$nbPages += sizeof($html);
		}		
		return $nbPages;
	}

	public function updateDate($type = "edit") {
		$requpdate =  "UPDATE `1_gestion_bat` SET ".(($type == "edit") ? "date_modif" : "date_genthumbnails" )." =  NOW() WHERE rowid = ". $this->idbat;
		$res = $this->execute($requpdate);
	}

	public function genMiniatures($width, $height) {
		for($i = 1; $i <= $this->nbpages; $i++) {
			$this->displayMiniature($i, $width, $height, false);
		}
		$this->updateDate("thumbnails");
	}

	public function needToBeUpdated() {
		return $this->date_edited > $this->date_genthumbnails;
	}

	public function displayMiniature($numpage, $width, $height, $caching = false) {
		$name = md5("img_".$this->idbat.'_'.$width.'_'.$height.'_'.$numpage.'_'.$this->templateSlug);
		$root = ($this->context != 'client') ? realpath($_SERVER["DOCUMENT_ROOT"]) : "";
		$fimg = $root.'/designer/tmp/'.$name.'.jpg' ;


		if ($caching && file_exists($fimg) && is_readable($fimg)) {
			echo  readfile($fimg);
			exit(1);
		}


		$sizeproducts = sizeof($this->template->htmlgen["page_produits"]) ;

		if( $numpage < ($sizeproducts + 1) )
			$page = $this->template->htmlgen["page_produits"][intval($numpage - 1)] ;
		else
			$page= $this->template->htmlgen["page_emplacements"][intval($numpage - intval($sizeproducts) - 1  )];

		$html = str_get_html($page) ;
		$starthtml = ' <!DOCTYPE html>
								<html>
								<head>
					        <meta charset="utf-8" />
								</head>
								<body>' ;
		$endhtml = '</body></html>' ;

		$image = new Image($starthtml.$html.$endhtml);

		$options['quality']='85';

		$options['user-style-sheet'] =  $root.$this->getStyleSheetPath();

		$options['width']=$width;
		$image->setOptions($options);
		$image->saveAs($fimg);

		//Resize de la miniature avec imagick
		// $imagick = new Imagick($fimg) ;
		// $imagick->resizeImage($width, $height, Imagick::FILTER_LANCZOS, 1);
		// $image->writeImage($fimg);
		//    header("Content-Type: image/jpg");
		// $arr = array('name' => $name, 'url' => '/designer/user_img/'.$this->userid.'/'.$this->idbat.'/miniatures/'.$name.'.jpg') ;
		// $this->miniatures[] = json_encode($arr) ;
		//unlink($fimg) ;
		//	unlink($fimg) ; // supprimer la miniature
		return $image->send();

	}

	public function getStyleSheetPath() {
		$root = ($this->context != 'client') ? realpath($_SERVER["DOCUMENT_ROOT"]) : "";
		if (file_exists($root.'/templates/'.$this->templateSlug.'/style.scss')) {
			$scss = new Compiler();
			$scss->setImportPaths("$root/templates/".$this->templateSlug."/");
			$scss_texte = "";
			if (file_exists($root.'/templates/'.$this->templateSlug.'/css/config_'.$this->templateId.'.scss')){
				$scss_texte .= '@import "css/config_'.$this->templateId.'.scss";';
				$scss_texte .= '@import "style.scss";';
				// $scss_texte .= "";
				file_put_contents($root.'/templates/'.$this->templateSlug.'/css/style_'.$this->templateId.'.css', $scss->compile($scss_texte));
				return '/templates/'.$this->templateSlug.'/css/style_'.$this->templateId.'.css';
			} else {
				file_put_contents($root.'/templates/'.$this->templateSlug.'/style.css', $scss->compile('@import "style.scss";'));
			}

		}
		return '/templates/'.$this->templateSlug.'/style.css';

	}

	public function clearHTML($page){
		$html = str_get_html($page);

		if ($html) {
			$SPvisibles = $html->find('[sp-visible="false"]');

			foreach ($SPvisibles as $SPvisible)
				$SPvisible->innertext = "";

			$page = $html->save() ;
		}

		return $page ;
	}

	// ? PHOTOS DU PRODUITS

	public function setPhotos() {
		$nbimg = 0 ;
		$count = 0;
		$root = ($this->context != 'client') ? realpath($_SERVER["DOCUMENT_ROOT"]) : "";
		if ( empty( $this->io_in->views ) )
			return -1 ;
		foreach($this->template->htmlgen["page_produits"] as $nb=>$htmlpage){
			$html = str_get_html($htmlpage);

			$infos_layout = $html->find(".infos_layout", 0);
			if($infos_layout) {
				$count_zone = 0;
				foreach($this->io_in->zones as $obj) {
					$count_zone += ($obj->miniature_file) ? 1 : 0;
				}
				if($count_zone > 6) {
					$count_zone = 6;
				}
				$infos_layout->setAttribute("nb_logo", $count_zone);
				$infos_layout->setAttribute("nb_product_img", count($this->io_in->views));
			}

			$productcontainer = $html->find(".contenaire_produit", 0);
			if (!empty($productcontainer)) {
				$Htmlproduct = "";
				$tmpl = $productcontainer->find('[sp-type="produit"]', 0);
				$nbrepeatmax = ($tmpl) ? $tmpl->getAttribute('sp-repeat') : 0;
				$picture_label = $tmpl->find('[sp-type="picture-label"]', 0);

				$layout_mode = 'p';
				foreach ( $this->io_in->views as $i => $designinfo) {
					// if ($nbrepeatmax && $nbimg >= $nbrepeatmax)
					if (!($i >= $count && $i < $count + $nbrepeatmax))
						continue;

					if ($this->io_imagepath != "")
						$urlimagephoto = $this->io_imagepath.$designinfo  ;
					else if (strpos($designinfo, "http") !== false)
						$urlimagephoto = $designinfo  ;
					else
						$urlimagephoto = $root.'/src/photoProduits/'.$designinfo  ;

					if(!empty($urlimagephoto)) {

						if (!empty($this->io_in->views_infos[$i])) {
							if ($picture_label)
								$picture_label->innertext = !empty($this->io_in->views_infos[$i]->name) ? $this->io_in->views_infos[$i]->name : '';

							if (!empty($this->io_in->views_infos[$i]->image_ratio) && $this->io_in->views_infos[$i]->image_ratio > 1.7)
								if ($layout_mode == 'p')
									$layout_mode = 'ls';
						}

						$img = $tmpl->find('[sp-type="photo_produit"]', 0);
						$img->setAttribute('src',$urlimagephoto."?".time());
						$nbimg++;

					}
					$Htmlproduct .= $tmpl->outertext;

				}
				
				$count += $nbrepeatmax;

				$views_count = count($this->io_in->views);
				/* if ($views_count && $views_count <= 2)
					$layout_nb = '2';
				else if ($views_count > 2 && $views_count < 6) {
					if ($layout_mode == 'p')
						$layout_nb = '3';
					else
						$layout_nb = '4';
				}
				else
					$layout_nb = '6'; */
				if (intval($this->templateId) > 0 && $this->templateSlug != 'ls_superpictor' && intval($this->templateId) <= 28) {
					if ($views_count && $views_count <= 2)
						$layout_nb = '2';
					else if ($views_count > 2 && $views_count <= 6) 
						$layout_nb = '3';											

				} elseif (intval($this->templateId) > 28 || $this->templateSlug == 'ls_superpictor' ) {
					if ($views_count > 0) {
						$layout_last_page = ($nbrepeatmax > 0) ? $views_count % $nbrepeatmax : 0;
						$npage = ($nbrepeatmax > 0) ? intval($views_count / $nbrepeatmax) : 0;
						if ($nb < $npage) 
							$layout_nb = $nbrepeatmax ;
						elseif ($nb == $npage)
							$layout_nb = $layout_last_page;
					}
				}	

				$productcontainer->setAttribute('layout', "$layout_nb-$layout_mode");
				$productcontainer->innertext = $Htmlproduct;
				$this->template->htmlgen["page_produits"][$nb] = $html->save() ;
				continue;
			}

			$img = $html->find('[sp-type="photo_produit"]');
			$nbimg++ ;
			if( !sizeof($img) ) return -1;

			foreach ( $this->io_in->views as $i => $designinfo) {
				if ($this->io_imagepath != "")
					$urlimagephoto = $this->io_imagepath.$designinfo  ;
				else if (strpos($designinfo, "http") !== false)
					$urlimagephoto = $designinfo  ;
				else
					$urlimagephoto = $root.'/src/photoProduits/'.$designinfo  ;
				if(isset($urlimagephoto) && !empty($urlimagephoto) && isset($img[$i]))
					$img[$i]->setAttribute('src',$urlimagephoto."?".time());
				$nbimg++ ;
			}

			foreach ($img as $imgk => $imgi) {
				if($imgk > ($nbimg-2))
					$img[$imgk]->outertext = "" ;
			}
			$this->template->htmlgen["page_produits"][$nb] = $html->save() ;

		}

	}

	public function setIOvariables() {
		$it = 0;
		foreach ($this->io_variables as $toreplace_category => $toreplace_values) {
			foreach ($this->template->htmlgen as $key => $html)
				foreach($html as $nb => $htmlpage) {
					$simpleHtml = str_get_html($htmlpage);

					foreach ($simpleHtml->find('[sp-category='.$toreplace_category.']') as $field) {
						if ($field) {
							$edit = $field->getAttribute('sp-edit');

							if ($field->tag == "img" && !empty($toreplace_values->$edit)) {
								$field->setAttribute("src", $toreplace_values->$edit);
							} else {
								if (isset($toreplace_values->$edit)){
									$field->innertext = ($toreplace_values->$edit != '') ? $toreplace_values->$edit : '';
									if ($toreplace_values->$edit != '' && ($edit == 'address_1' || $edit == 'zip'))
										$field->innertext .= ',';
								}
							}
						}
					}
					$this->template->htmlgen[$key][$nb]= $simpleHtml->save();
				}
		}
	}

	public function newPage($page, $nbpage) {
		//	var_dump($this->template->html[$page]) ;
		if($page == 'produits')
			$page = 'page_produits' ;
		else
			$page = 'page_emplacements' ;

		$i = sizeof($this->template->htmlgen[$page]) ;
		$this->template->htmlgen[$page][$i] = $this->template->html[$page] ;
	}

	public function SelectMoiCaOBJ($query) {
		$result = $this->base->exec("SET CHARACTER SET utf8");
		$result = $this->base->prepare($query);
		$result->execute();
		return $result->fetchAll(PDO::FETCH_OBJ);
	}

	public function SelectMoiCaARRAY($query) {
		$result = $this->base->exec("SET CHARACTER SET utf8");
		$result = $this->base->prepare($query);
		$result->execute();
		return $result->fetchAll(PDO::FETCH_ASSOC);
	}

	public function execute($query) {
		$result = $this->base->prepare($query);
		$result->execute();
		return $result;
	}


	public function getInfos() {
		$infos = array() ;
		$infos['1_gestion_bat']['nom'] = 'nombat' ;
		$infos['1_gestion_bat']['reference'] = 'ref_commande' ;
		$infos['1_infos_client']['nom_client'] = 'client_bat' ;
		$infos['1_infos_client']['entreprise_client'] = 'entreprise_client' ;
		$infos['1_infos_client']['commercial_client'] = 'commercial' ;
		$infos['1_infos_client']['commande_client'] = 'commande' ;
		$infos['1_infos_revendeur']['entreprise'] = 'company_name' ;
		$infos['1_infos_revendeur']['adresse'] = 'adresse' ;
		$infos['1_infos_revendeur']['zip'] = 'zip' ;
		$infos['1_infos_revendeur']['ville'] = 'ville' ;
		$infos['1_infos_revendeur']['tel'] = 'telephone' ;
		$infos['1_infos_revendeur']['email'] = 'email' ;
		$infos['1_infos_revendeur']['website'] = 'website' ;
		$sql_colonnes = array() ;

		foreach($infos as $table_bdd=>$ligne){

			foreach($ligne as $col_bdd=>$matchelement){

				if(isset($matchelement) && !empty($matchelement))
					$alias = ' as '.$matchelement ;
				else
					$alias = '';

				$sql_colonnes[]= ' `'.$table_bdd.'`.'.$col_bdd.$alias ;
			}
		}
		$select = implode(', ',$sql_colonnes) ;
		// 			echo '<pre>' ;
		// echo $select ;
		// echo '</pre>' ;
		//	$select2 = preg_replace(", "," ", $select, 1);
		$query = 'SELECT '.$select.' FROM `1_gestion_bat`
						LEFT JOIN `1_infos_client` ON `1_gestion_bat`.rowid = `1_infos_client`.idbat
						LEFT JOIN `1_infos_revendeur` ON `1_gestion_bat`.rowid = `1_infos_revendeur`.idbat
						WHERE `1_gestion_bat`.rowid ='.$this->idbat ;

		$tableau = $this->SelectMoiCaOBJ($query) ;

		$this->infos = $tableau[0] ;
		return $tableau[0] ;
	}


	public function loadTemplate($template = null, $templateId = null) {

		$this->template = new stdClass(); // Création de l'objet
		$this->template->config = new stdClass();
		$this->template->html = array();
		$this->template->htmlgen = array();
		$this->template->models = array();
		if (!empty($template) && ($templateId && intval($templateId)) > 0) {
			$this->templateSlug = $template;
			$this->templateId = $templateId;

		}
		$this->template->name = $this->templateSlug;
		$root = realpath($_SERVER["DOCUMENT_ROOT"]);

		if (intval($this->templateId)) {
			// $text_css = "";
			// get config from database for template
			$query = "SELECT config FROM 1_bat_templates WHERE id = $this->templateId";
			$config = $this->SelectMoiCaOBJ($query);
			if (!empty($config) && !empty($config[0]->config)) {
				$this->template->params = json_decode($config[0]->config);

				if(!empty($this->template->params->scss)){
					$text_css = "";
					foreach($this->template->params->scss->variables as $variable)
						$text_css .= "$".$variable->nom.":\t".$variable->value.";\n";
					if (!is_dir($root.'/templates/'.$this->templateSlug.'/css'))
						mkdir($root.'/templates/'.$this->templateSlug.'/css');
					file_put_contents($root.'/templates/'.$this->templateSlug.'/css/config_'.$this->templateId.'.scss', $text_css);
				}


			}

		}

		$chemin = $root.'/templates/'.$this->template->name.'/pageProduit.php' ;
		$this->template->html["page_produits"] = file_get_contents($chemin);
		$chemin = $root.'/templates/'.$this->template->name.'/pageEmplacements.php' ;
		$this->template->html["page_emplacements"] = file_get_contents($chemin);
		$this->setnbpages() ; // Definis le nombre de page necessaire en fonction du nombre de vue dans le produit et d'emplacements
		$this->setBatPagination() ; // Met la pagination
		$this->setPhotos() ; // Ajoute les vue produits
		$this->setTemplateEditableFields();
		$this->setIOvariables() ;
		$this->nbpages = count($this->template->htmlgen["page_emplacements"]) + count($this->template->htmlgen["page_produits"]) ; // nombre total de page
	}



	public function setTemplateEditableFields() {
		$root = ($this->context != 'client') ? realpath($_SERVER["DOCUMENT_ROOT"]) : "";
		$nbrepeatmax_elmt = str_get_html($this->template->html['page_emplacements'])->find('[sp-type=emplacements]', 0);
		$nbrepeatmax_elmt = (!$nbrepeatmax_elmt) ? str_get_html($this->template->html['page_produits'])->find('[sp-type="emplacements"]', 0) : $nbrepeatmax_elmt;
		$nbrepeatmax = ($nbrepeatmax_elmt) ? $nbrepeatmax_elmt->getAttribute('sp-repeat') : 0;
		$displayedEmp = [];

		$sc = new SuperClient($this->userid, 0);
		$notPermission = (!$sc->hasPermissionFor("TEMPLATE_CUSTOM") && !$sc->hasPermissionFor("TEMPLATE_NO_BRANDING") && !$sc->hasPermissionFor("TEMPLATE_LOW_BRANDING") && !$sc->hasPermissionFor("TEMPLATE_HIGH_BRANDING")) ? true : false;
		foreach ($this->template->htmlgen as $page => $pageHtml) {
			foreach ($pageHtml as $nb => $html) {
				$html = str_get_html($html);

				foreach($html->find('[spBranding]') as $branding) {
					foreach($html->find('[high-branding]') as $highBrand) {
						if($sc->hasPermissionFor("TEMPLATE_NO_BRANDING") || $sc->hasPermissionFor("TEMPLATE_LOW_BRANDING") || $sc->hasPermissionFor("TEMPLATE_CUSTOM"))
							$highBrand->setAttribute('hidden-element', '');
					}

					foreach($html->find('[low-branding]') as $lowBrand) {
						if($sc->hasPermissionFor("TEMPLATE_NO_BRANDING") || $sc->hasPermissionFor("TEMPLATE_CUSTOM"))
							$lowBrand->setAttribute('hidden-element', '');
					}

					foreach($html->find('[no-branding]') as $noBrand) {
						if($sc->hasPermissionFor("TEMPLATE_HIGH_BRANDING") || $notPermission)
							$noBrand->setAttribute('hidden-element', '');
					}
				}

				foreach ($html->find('[sp-edit][sp-category=infobat]') as $field) {
					if ($field) {
						$edit = $field->getAttribute('sp-edit');


						if (isset($this->editedFields->$edit) || $field->tag == 'img') {
							if ($field->tag == 'img') {
								$imgSrc = '';

								if (isset($this->editedFields->$edit->value))
									$imgSrc = $root.$this->editedFields->$edit->value.'?'.time();

								if ($imgSrc)
									$field->src = $imgSrc;
								else {
									if ($field->getAttribute('sp-contained')) {
										$contained = $html->find('.'.$edit.'_container', 0);
										$contained->outertext = '';
									}
									else
										$field->outertext = '';
								}
							}
							else
								$field->innertext = nl2br($this->editedFields->$edit->value);

							if (isset($this->editedFields->$edit) && $this->editedFields->$edit->hidden) {
								if ($field->getAttribute('sp-contained')) {
									$contained = $html->find('.'.$edit.'_container', 0);
									$contained->setAttribute('hidden-element', '');
								}
								else
									$field->setAttribute('hidden-element', '');
							}
						}
					}
				}
				if (isset($this->template->params))
				$display_background = $this->template->params->display_background;
				// $html = str_get_html($html);
				$container = $html->find('.contenaire_emplacements', 0);
				$tmpl = null;

				if ($container) {
					$tmpl = $container->find('[sp-type=emplacements]', 0);
					$container->innertext = "";

					$zones_count = 0;
					$iterator = 0;
					$fonts = [];
					foreach ($this->io_in->zones as $emplacement => $obj) {
						if (intval($obj->has_miniature) && $iterator < $nbrepeatmax && !in_array($emplacement, $displayedEmp)) {
							$zones_count++;
							foreach($tmpl->find('[sp-edit]') as $edit) {
								$edit->innertext = '';
							}
							$tmpl->setAttribute('container', str_replace(' ', '+', $emplacement));
							$tmpl->find('[sp-edit=emplacement]', 0)->innertext = $emplacement;
							if (empty($obj->ref) && !empty($obj->spref))
								$obj->ref = $obj->spref;
							foreach ($obj as $prop => $value) {

								if ($prop == 'miniature_file' && ($elmt = $tmpl->find('img[sp-edit=miniature_file]', 0))) {

									if ($this->io_imagepath != "")
										$urlImageFile = $this->io_imagepath.$obj->miniature_file  ;
									else if (strpos($obj->miniature_file, "http") !== false)
										$urlImageFile = $obj->miniature_file  ;
									else
										$urlImageFile = $root.'/src/photoProduits/'.$obj->miniature_file  ;

									$elmt->src = (!empty($urlImageFile)) ? $urlImageFile : "";

								} else if ($elmt = $tmpl->find("[sp-edit=$prop]", 0)) {

									if(!empty($value))
										$elmt->innertext = $value;

								} else if ((strpos($prop, "hidden") === 0) ) {
									$elmt = $tmpl->find("[sp-edit=$prop]", 0);
									$pr = substr($prop, 7);
									if(($elmt_child = $tmpl->find("[sp-edit=$pr]", 0)) && $elmt_child->getAttribute('sp-contained')) {
										if ($contained = $tmpl->find('.'.$pr.'_container', 0))
											$contained->setAttribute('hidden-element', (($value) ? '' : null ));
										else
											$elmt_child->setAttribute('hidden-element', (($value) ? '' : null ));
									}
									if(!empty($value))
										$elmt->innertext = $value;
								}

								if ($prop == 'background') {
									$product_color = $value;
									if (!empty($display_background) && $elmt = $tmpl->find('[sp-style=background]', 0))
									$elmt->setAttribute('style', 'background-image:none;background-color:'.$value.';');
								} 
							}

							$html_font        = '';
							$html_color       = '';
							$colors_container = '';
							$arr = [];
							$tmp_colors = [];


							foreach($obj->user_objects as $user_object) {
								$style = json_decode(json_encode($user_object->object->styles),true);
								if($user_object->type == 'texte'){
									if($html_font == "")
										$html_font = "<p class='fonts_use'>Polices utilisées</p>";
				
									$html_font.="<div class='box_fonts'>";
									if(!isset($style[0]))
										$fonts[$user_object->object->text][] = 'Serif'; 
									else
										foreach($style[0] as $font)
											$fonts[$user_object->object->text][] = ($font['fontFamily']==null) ? 'Serif' : $font['fontFamily'];

									$fonts[$user_object->object->text] = array_unique($fonts[$user_object->object->text]);
									$html_font.= "<div class='font_text'>".$user_object->object->text . ' : </div>';
									$html_font.= "<div class='fonts_list'>";
									$fonts_list = [];
									foreach($fonts[$user_object->object->text] as $key => $font)
										array_push($fonts_list, $font);
			
									$html_font.= implode(', ', $fonts_list);
									$html_font.= "</div></div>";
								}

								if (!empty($filecolors = $user_object->object->des_colors)) {
									$number_colors = 0;

									if($colors_container = $tmpl->find('[sp-type=colors]', 0)) {
										$type_display = $colors_container->attr['type_display'];
									}

									foreach ($filecolors as $color) {
										$number_colors++;
										$duplicate = true;
										if(!empty($color->to)) {
											$r          = $color->to->r;
											$g          = $color->to->g;
											$b          = $color->to->b;
										} else {
											$r          = $color->from->r;
											$g          = $color->from->g;
											$b          = $color->from->b;
										}
										$rgb        = sprintf('rgb(%d , %d , %d)', $r, $g, $b);
										$ref_color = sprintf("#%02x%02x%02x", $r, $g, $b);
										$collection_color = "Hexadécimal";
										$type_color = "";

										if ($color->to->id) {
											$color_class      = new SuperColor($color->to->id);
											$rgb              = $color_class->rgb;
											$ref_color        = $color_class->ref;
											$collection_color = $color_class->collection;
											$type_color       = $color_class->type;
										}

										if($type_color === "pantone") {
											$ref_color        = $color_class->ref;
											$collection_color = "Pantone";
										}

										if(in_array($ref_color, $tmp_colors)) {
											$duplicate = false;
											$number_colors--;
										}

										if($duplicate) {
											if($type_display === '0') {
												$html_color .= "
												<div class='squares_color'>
												<span style='background-color: $rgb' class='color_square'></span>
												<span class='color_text' >$ref_color - $collection_color</span>
												</div>
												";
											} else if ($type_display === '1') {
												$json = new stdClass();
												$json->ref = $ref_color;
												$json->rgb = $rgb;
												$arr[$collection_color][] = $json;
											}
											$tmp_colors[] = $ref_color;
										}
									}

									if($nb_colors = $tmpl->find('[sp-type=nb_colors]', 0)) {
										$nb_colors->innertext = $number_colors;
									}
								}
							}

							if ($type_display === '1') {
								foreach($arr as $col => $vla) {
									$content = "";
									if (sizeof($vla) <= 6) {
										foreach($vla as $vl) {
											$content .= "<div class='squares_color'>
											<span style='background-color: ". $vl->rgb ."' class='color_square'></span>
											<span class='color_text'>". $vl->ref ."</span>
											</div>";
										}
									} else {
										// count number of rows
										$modNumber = sizeof($vla) % 6 ;
										$divNumber = (int) (sizeof($vla) / 6) ;
										$rowNumber = ($modNumber > 0) ? $divNumber + 1 :  $divNumber ;
										$index = 0;
										for ($i=1; $i <= $rowNumber; $i++) {
											$content .= "<div>";
											if (($i*6) < sizeof($vla))
												$maxIndex = ($i*6) ;
											else
												$maxIndex = sizeof($vla) ;
											for ($j = $index ; $j < $maxIndex; $j++) {
												$content .= "<div class='squares_color'>
												<span style='background-color: ". $vla[$j]->rgb ."' class='color_square'></span>
												<span class='color_text'>". $vla[$j]->ref ."</span>
												</div>";

											}
											$index = $j;
											$content .= "</div>";
										}
									}
									$html_color .= "
									<div class='box_collection'>
										<p class='collection_name'>$col</p>
										$content
									</div>
									";
								}
							}

							if($colors_container = $tmpl->find('[sp-type=colors]', 0)) {
								$colors_container->layout = ($number_colors > 10) ? 2 : 1;
								$colors_container->innertext = $html_color;
							}

							if($fonts_container = $tmpl->find('[sp-type=fonts]', 0)) {
								$fonts_container->innertext = $html_font;
							}

							$iterator++;
							array_push($displayedEmp, $emplacement);
							$container->innertext .= $tmpl->outertext;
						}

						if ($iterator == $nbrepeatmax)
							break;
					}

					if ($zones_count > 0 && $zones_count <= 2)
						$layout_zones = $zones_count;
					else if ($zones_count <= 4)
						$layout_zones = 4;
					else
						$layout_zones = 6;

					$container->setAttribute('layout', $layout_zones);
				}
				// get name of color product from model
				$model_product = (isset($this->io_in->model)) ? $this->io_in->model : '';			
				if ($elmt_model_product = $html->find('[sp-style=model_product]', 0))
					$elmt_model_product->innertext = $model_product;
				if (($elmt_background = $html->find('[sp-style=background]', 0)) && !empty($display_background)) {
					if (!empty($product_color))
					$elmt_background->setAttribute('style', 'background-image:none;background-color:'.$product_color.';');
				}
				$this->template->htmlgen[$page][$nb] = $html->save();
			}
		}

	}

	public function getTemplateEditableFields() {
		$this->loadTemplate();

		$editables = [];
		foreach ($this->template->html as $page) {
			$html = str_get_html($page);

			$categories = array();
			foreach ($html->find("[sp-edit][sp-category]") as $category_element) {
				$category = $category_element->getAttribute('sp-category');

				if (!in_array($category, $categories)) {
					$categories[] = $category;
				}
			}

			foreach ($categories as $category) {


				$tmp = [];

				foreach ($html->find("[sp-edit][sp-category=$category]") as $field) {
					if ($field) {
						$edit = $field->getAttribute('sp-edit');

						$obj = new stdClass();
						$obj->label = $field->getAttribute('sp-label');
						$obj->value = trim($field->plaintext);
						$obj->order = intval($field->getAttribute('sp-order'));
						$obj->type = $field->getAttribute('sp-type');
						$obj->hiddable = $field->getAttribute('sp-hiddable');
						$obj->permission = intval($field->getAttribute('sp-permission'));
						$obj->infoField = $field->getAttribute('sp-infofield');
						$obj->isImg = $field->getAttribute('sp-img');
						$obj->isStatic = $field->getAttribute('sp-static');
						$obj->selectOptions = array();

						if ($obj->type == 'select') {
							switch ($edit) {
								case 'commercial':
									$sc = new SuperClient($this->userid, 0);

									if ($current_userinfos = $sc->select(
										"SELECT virtuemart_user_id, first_name, last_name
										FROM `digime_virtuemart_userinfos`
										WHERE virtuemart_user_id = $this->userid"
									)) {
										array_push($obj->selectOptions, array(
											'value' => $current_userinfos->virtuemart_user_id,
											'text'  => "$current_userinfos->first_name $current_userinfos->last_name"
										));
									}

									if ($contacts = $sc->getRootUserContacts()) {
										foreach ($contacts as $contact) {
											array_push($obj->selectOptions, array(
												'value' => $contact->virtuemart_user_id,
												'text'  => "$contact->first_name $contact->last_name"
											));
										}
									}
									break;
								default: break;
							}
						}

						$tmp[$edit] = $obj;
					}
				}

				$reorder = [];
				for ($i = 0; $i < count($tmp); $i++)
					foreach ($tmp as $key => $obj)
						if ($obj->order == $i)
							$reorder[$key] = $obj;

				if (!isset($editables[$category]))
					$editables[$category] = $reorder;
			}
		}

		return $editables;
	}


	public function setnbpages(){
		foreach ($this->template->html as $page => $content) {
			$this->template->htmlgen[$page]= array( $this->template->html[$page]) ;
		}

		$nbemplacements = 0;
		foreach ($this->io_in->zones as $emplacement => $obj) {
			if (!intval($obj->has_miniature))
				continue;
				$nbemplacements++;
		}

		if(!$nbemplacements)
			return -1 ;
		
		$nbrepeatmax_emp = str_get_html($this->template->html['page_emplacements'])->find('[sp-type="emplacements"]', 0);
		$nbrepeatmax = ($nbrepeatmax_emp) ? $nbrepeatmax_emp->getAttribute('sp-repeat') : 0;			

		$pageadefinir = 1;		
		if ($nbrepeatmax) {
			while ($nbemplacements > $nbrepeatmax) {
				$pageadefinir++;
				$nbemplacements -= $nbrepeatmax;
			}			
		}
		$nbViews = sizeof($this->io_in->views);
		$pageProducts = 1;
		$prod = str_get_html($this->template->html['page_produits'])->find('[sp-type="produit"]', 0);
		if (!empty($prod)) {
			$nbrepeatprod = $prod->getAttribute('sp-repeat') ;
			if ($nbrepeatprod) {
				while ($nbViews > $nbrepeatprod) {
					$pageProducts++;
					$nbViews -= $nbrepeatprod;
				}
			}
		}
		for($i = 0; $i < ($pageadefinir); $i++)
			$this->template->htmlgen['page_emplacements'][$i] = $this->template->html['page_emplacements'];
		for($i = 0; $i < $pageProducts; $i++)
		$this->template->htmlgen['page_produits'][$i] = $this->template->html['page_produits'];
	}

	public function setBatPagination(){

		$htmlgen = $this->template->htmlgen ;
		$pagetotal = 0;
		foreach ($htmlgen as $key => $html) {
			$pagetotal = $pagetotal + sizeof($html) ;

		}
		$nbpage = 0 ;

		foreach ($htmlgen as $key => $html) {
			foreach($html as $nb=>$htmlpage){
				$nbpage++ ;

				$htmlpage = str_replace('{pactive}', $nbpage, $htmlpage) ;
				$htmlpage = str_replace('{pagetotal}', $pagetotal, $htmlpage) ;
				$this->template->htmlgen[$key][$nb]= $htmlpage ;
			}

		}
	}

	public function showBAT(){
		$i = 1 ;
		foreach ($this->template->htmlgen as $key => $htmlgen)
		{
			foreach ($htmlgen as $nb =>$showhtml) {
				echo'<div class="showtemplate" id="'.$key.'-'.$nb.'" relation="miniature-'.$i.'">' ;
				echo $showhtml ;
				echo '</div>' ;
				$i++ ;
			}

		}

	}

	public function DanXEncrypt($pass, $it = 16) { // $it = le nombre de tour de cryptage
		for ($i = 0; $i < $it ; $i++) {
			if ($i % 3 == 0 || $i % 12 == 0)
				$pass = strrev($pass);
			else if ($i % 5 == 0) {
				$Tableau = explode("W", $pass);
				$Tableau = array_reverse($Tableau);
				$pass = implode("W", $Tableau);
			}
			else
				$pass = base64_encode($pass);
		}

		return $pass;
	}

	public function getMiniatures() {
		for($i = 1; $i <= $this->nbpages; $i++) {
			$tmp = new stdClass();
			$tmp->images = array();
			$tmp->images["low"] = "/designer/miniature.php?idbat=" . $this->DanXEncrypt($this->idbat) . "&tmpl=" . $this->templateSlug ."&tmplid=" . $this->templateId . "&no=$i&def=low&t=".time();
			// $tmp->images["low"] = "/designer/miniature.php?idbat=" . $this->DanXEncrypt($this->idbat) . "&tmpl=" . $this->templateSlug ."&tmplid=9" . "&no=$i&def=low&t=".time();
			$tmp->images["high"] = "/designer/miniature.php?idbat=" . $this->DanXEncrypt($this->idbat) . "&tmpl=" . $this->templateSlug ."&tmplid=" . $this->templateId . "&no=$i&def=high&t=".time();
			$tmp->page = $i;
			$this->miniatures[] = $tmp;
		}
		return $this->miniatures;
	}

	function getMiniaturesHTML($def = "low", $activeid = 0, $limit = null, $caching=false, $lazy=false) {
		if (!isset($this->miniatures) || !sizeof($this->miniatures))
			$this->getMiniatures();
		$minHTML = "";
		foreach ($this->miniatures as $key => $min) {
			$minHTML .=  "<img class='miniature " .  ( ($key == $activeid) ? 'active' : '' ) . (($lazy) ? "'data-src='" : "'src=" ) . $min->images[$def] . (($caching) ? "&fromcache=true" : "" ) . "' data-miniature-id='" . $min->page . "'>";
			if ($limit && $key >= intval($limit - 1))
				return $minHTML;
		}
		return $minHTML;
	}

	function getMiniaturesDeditionHTML($def = "low", $activeid = 0) {
		if (!isset($this->miniatures) || !sizeof($this->miniatures))
			$this->getMiniatures();
		$minHTML = "";
		foreach ($this->miniatures as $key => $min)
			$minHTML .=  "<div class='mindedition'><img class='miniature " .  ( ($key == $activeid) ? 'active' : '' ) . "' src='"  . $min->images[$def] . "' data-miniature-id='" . $min->page . "'>Page " . $min->page . "</div>";

		return $minHTML;
	}


	public function showMiniatures(){
		if(!empty($this->miniatures)){
			foreach ($this->miniatures as $key => $miniature) {
				$miniature = json_decode($miniature);

				echo '<img src="'.$miniature->url.'" name="'.$miniature->name.'" class="miniatures">' ;
			}
		}
	}

	private function changeSRC(){


		foreach ($this->template->htmlgen as $key => $html) {
			foreach($html as $nb=>$htmlpage){
				$html = str_get_html($htmlpage);
				//var_dump($this->template->htmlgen);

				$img = $html->find('[sp-type="photo_produit"]');
				if(sizeof($img))
					$img[0]->setAttribute('src','https://www.superpictor.com/'.$urlimagephoto);
				$this->template->htmlgen[$key][$nb] = $html->save() ;
			}
		}
	}



	public function sortiePDF($orientation = "Portrait", $sendtoout = false) {
		$root = realpath($_SERVER["DOCUMENT_ROOT"]);
		$this->setContext('server');
		$this->loadTemplate();
		if (!empty($this->templateOrientation))
			$orientation = $this->templateOrientation;


		// var_dump($this->getStyleSheetPath()); exit();
		$pdf = new Pdf(array(
			'no-outline',         // Make Chrome not complain
			'margin-top'    => 0,
			'margin-right'  => 0,
			'margin-bottom' => 0,
			'orientation' => $orientation,
			'margin-left'   => 0,
			'encoding' 			=> 'UTF-8',
			//'enable-local-file-access',
			// Default page options
			'disable-smart-shrinking',
			'print-media-type',
			'user-style-sheet' => $root.$this->getStyleSheetPath()
		));
		$starthtml =
			'<!DOCTYPE html>
				 <html>
				 <head>
					 <style>
					 	body {
							margin: 0;
							padding: 0;';
		if ($orientation == "Portrait")
			$starthtml .= 'height: 297mm; width: 210mm;';
		else
			$starthtml .= 'height: 210mm; width: 297mm;';
		$starthtml .='}
					 </style>
				 </head>
				 <body>';
		$endhtml = '</body></html>' ;

		foreach ($this->template->htmlgen as $key => $html) {
			if ($html == $this->template->html[$key])
				continue; // temporaire senator

			$hashParDefaut = str_replace("\n", "", $this->template->html[$key]);
			$hashParDefaut = str_replace("\t", "", $hashParDefaut);
			$hashParDefaut = str_replace(" ", "", $hashParDefaut);
			$hashParDefaut = filter_var($hashParDefaut, FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);
			if ($key != '')
				foreach($html as $nb=>$htmlpage) {
					$hash = str_replace("\n", "", $htmlpage);
					$hash = str_replace("\t", "", $hash);
					$hash = str_replace(" ", "", $hash);
					$hash = filter_var($hash, FILTER_UNSAFE_RAW, FILTER_FLAG_STRIP_LOW|FILTER_FLAG_STRIP_HIGH);

					if ($hash == $hashParDefaut)
						continue;
					$pdf->addPage($starthtml.$htmlpage.$endhtml);
				}


		}



		$name = md5("BAT_".$this->idbat);
		$target = $root.'/pdf/'.$name.'.pdf' ;

		if (file_exists($target)) {
			chmod($target, 0777);
			unlink($target);
		}


		if ($sendtoout) {
			// header('Content-type: application/pdf');
			// echo "heho";
			if (!$pdf->send()) {
				throw new Exception('Could not create PDF: '.$pdf->getError());
			}
			// var_dump($this->template->htmlgen);
			exit(1);
		}

		if (!$pdf->saveAs($target)) {
			throw new Exception('Could not save PDF: '.$pdf->getError());
		}

		return '/pdf/'.$name.'.pdf';
	}
}

?>
