<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/src/libraries/classMaquetor.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/src/libraries/SuperClass.php';

class SuperBAT extends SuperClass {
  public  $id;
  public  $user_id;
  public  $designed;
  public  $logos;
  public  $name;
  public  $ref;
  public  $product_name;
  public  $created_on;
  public  $modified_on;
  public  $template;
  public  $templateId;
  public  $maquetor;

  public function __construct($id_bat, $mode = 'bat') {
    $this->id = $id_bat;
    $this->autoAuth("sp");

    if ($mode == 'bat') {
      $this->initInfos();
      $this->initInfoBat();
      $this->designed = $this->getDesigned();
    }
    else {
      $this->designed = $this->select("SELECT * FROM `1_designed` WHERE rowid = $id_bat");

      if ($this->designed) {
        $zones_json = json_decode($this->designed->zonesjson);

        if ($zones_json) {
          $this->name = $zones_json->name;
          $this->ref = '';
          $this->product_name = $zones_json->name;
          $this->user_id = $this->designed->user_id;
          $this->views = $zones_json->views;
        }
      }
    }

    $this->logos = $this->getLogos();
  }

  public function initMaquetor($user_id) {
      $this->maquetor = new Maquetor($this->id, $this->pdo, (intval($user_id)) ? $user_id : $this->user_id); // userid ??
      $this->maquetor->loadTemplate();
      $this->maquetor->getInfos();
  }

  public function initInfos() {
      $infos = $this->select(
        "SELECT
        gb.nom as nom,
        gb.reference as reference,
        gb.idclient as user_id,
        gb.product_name as product_name,
        gb.date_creation as date_creation,
        gb.date_modif as date_modif,
        gb.statut as status,
        bt.id as t_id,
        bt.slug as t_slug
        FROM `1_gestion_bat` AS gb
        LEFT JOIN `1_bat_templates` AS bt
        ON gb.template = bt.id
        WHERE gb.rowid = ".$this->id
      );
      $this->name         = $infos->nom;
      $this->ref          = $infos->reference;
      $this->user_id      = $infos->user_id;
      $this->product_name = $infos->product_name;
      $this->statuscode   = $infos->status;

      $this->status      = $this->tradStatus($infos->status);
      $this->warnclass   = $this->tradStatus($infos->status, "warn");
      $this->created_on  = DateTime::createFromFormat('Y-m-d H:i:s', $infos->date_creation);
      $this->modified_on = DateTime::createFromFormat('Y-m-d H:i:s', $infos->date_modif);
      $this->template    = ($infos->t_id && intval($infos->t_id) > 0) ? $infos->t_slug : 'ls_superpictor';
      $this->templateId  = ($infos->t_id && intval($infos->t_id) > 0) ? intval($infos->t_id) : 9 ;
  }

  public function updateOneInfo($table, $field, $value) {
    if (property_exists($this, $field)) {
      if ($this->execute(
        "UPDATE $table SET $field = :value WHERE rowid = :id",
        array("value" => $value, "id" => $this->id)) != false)
          return true;
    }

    return false;
  }

  public function setTemplateSlug($tmplid) {
    $templateNb = $this->select(
			"SELECT
			bt.id as t_id,
			bt.slug as t_slug
			FROM `1_gestion_bat` AS gb
			LEFT JOIN `1_bat_templates` AS bt
			ON gb.template = bt.id
			WHERE gb.rowid = :id",
      array("id", $this->id)
		);

    $this->template = ($templateNb[0]->t_id && intval($templateNb[0]->t_id) > 0) ? $templateNb[0]->t_slug : 'ls_superpictor';
    // $this->templateId = ($templateNb[0]->t_id && intval($templateNb[0]->t_id) > 0) ? $templateNb[0]->t_id : '9';
  }

  private function tradStatus($statuscode, $type = "label") {
      if (intval($statuscode) == 0)
        return ($type == "label") ? "Brouillon" : "warngray" ;
      if (intval($statuscode) == 1)
          return ($type == "label") ? "Enregistré" : "warnblue" ;
      if (intval($statuscode) == 2)
          return ($type == "label") ? "Validé" : "warngreen" ;
      if (intval($statuscode) == 10)
          return ($type == "label") ? "Supprimé" : "warnred" ;
      return "Aucun";
  }

  public function changeStatus($status) {
    $this->execute(
      "UPDATE `1_gestion_bat`
      SET statut = :status
      WHERE rowid = :id",
      array(
        'status' => $status,
        'id' => $this->id
      )
    );
  }

  public function initInfoBat() {
    $infosBat = $this->select("SELECT * FROM 1_infos_bat WHERE idbat = ".$this->id);

    if (!$infosBat) {
      $infos = $this->getInfos();

      $fields = new stdClass();
      $fields->nom = [
        'value' => $infos->nom ?: null,
        'hidden' => false
      ];
      $fields->reference = [
        'value' => $infos->reference ?: null,
        'hidden' => false
      ];
      $fields->product_name = [
        'value' => $infos->product_name ?: null,
        'hidden' => false
      ];

      $this->execute(
        "INSERT INTO `1_infos_bat` (fields, idbat)
        VALUES (:fields, :idbat)",
        array(
          'fields' => json_encode($fields),
          'idbat' => $this->id
        )
      );
    }
  }

  public function updateInfoBat($values) {
    $jsonFields = json_encode($values);

    $this->execute(
      "UPDATE `1_infos_bat`
      SET fields = :fields
      WHERE idbat = :id",
      array(
        'fields' => $jsonFields,
        'id' => $this->id
      )
    );
    $this->execute(
      "UPDATE `1_gestion_bat`
      SET reference = :reference, product_name = :product_name
      WHERE rowid = :id",
      array(        
        'reference' => $values->reference['value'],
        'product_name' => $values->product_name['value'],
        'id' => $this->id
      )
      );
  }

  public function getLogos() {
    if (!$this->designed)
      $this->getDesigned();

    $des_json = json_decode($this->designed->zonesjson);
    $logos = [];


    $logos["photo_url"] = $des_json->views;
    foreach ($logos["photo_url"] as $key => $url)
        $logos["photo_url"][$key] = ((strpos($url, "http") !== false) ? $url : "/designer/".$url);
    foreach ($des_json->zones as $zoneName => $zoneObj) {
       if ($zoneObj->has_miniature) {
          $zoneObj->miniature_file = ((strpos($zoneObj->miniature_file, "http") !== false) ? $zoneObj->miniature_file : "/designer/".$zoneObj->miniature_file);
          $logos['zones'][$zoneName] = $zoneObj;
      }
    }

    return $logos;
  }

   public function getOrderByRef($spref) {
      if ($spref) {
         $order = $this->select(
            "SELECT voi.virtuemart_order_item_id, vo.virtuemart_order_id, voi.order_status, vo.order_number, vo.order_pass, voi.product_attribute
            FROM `digime_virtuemart_order_items` AS voi
            INNER JOIN `digime_virtuemart_orders` AS vo
            ON voi.virtuemart_order_id = vo.virtuemart_order_id
            WHERE vo.virtuemart_user_id = $this->user_id
            AND voi.product_attribute LIKE '%\"$spref\"%'"
         );

         return $order;
      }

      return null;
   }

   public function setInfoLogo($values, $hidden = array(), $spref = null) {
      $designed = $this->getDesigned();
      $zones = json_decode($designed->zonesjson);
      foreach ($zones->zones as $emp => &$zone) {
         if (!empty($values[$emp])) {
            if (!$spref)
               $spref = (!empty($values[$emp]['spref'])) ? $values[$emp]['spref'] : null;

            foreach ($values[$emp] as $key => $value) {
               $zone->$key = $value;

               $hidden_name = "hidden_$key";
               $zone->$hidden_name = (!empty($hidden[$emp]) && in_array($key, $hidden[$emp]));

               if ($key == 'ref' && $spref) {
                  $order = $this->getOrderByRef($spref);

                  if ($order) {
                     $p_attribute = json_decode($order->product_attribute, true);

                     foreach ($p_attribute['33'] as &$ref)
                        $ref['comment'] = $value;

                     $this->execute(
                        "UPDATE `digime_virtuemart_order_items`
                        SET product_attribute = " . $this->quote(json_encode($p_attribute)) .
                        "WHERE virtuemart_order_item_id = " . $order->virtuemart_order_item_id
                     );
                  }
               }
            }
         }
      }
      // die;

      $this->execute(
         "UPDATE `1_designed` AS d
         INNER JOIN `1_gestion_bat` AS gb
         ON d.rowid = gb.designed
         SET d.zonesjson = :zonesjson
         WHERE gb.rowid = :id",
         array(
            'zonesjson' => json_encode($zones),
            'id' => $this->id
         )
      );
   }

  public function getInfosBat() {
    return $this->select(
      "SELECT rowid, nom, reference, product_name, fields, statut
      FROM `1_gestion_bat` AS gb
      JOIN `1_infos_bat` AS ib ON gb.rowid = ib.idbat
      WHERE gb.rowid = ".$this->id
    );
  }

  public function getInfos() {
    return $this->select("SELECT * FROM `1_gestion_bat` WHERE rowid = ".$this->id);
  }

  public function getDesigned() {
    return $this->select(
      "SELECT * FROM `1_gestion_bat` as bat
      JOIN `1_designed` as design ON bat.designed = design.rowid
      WHERE bat.rowid = ".$this->id
    );
  }
}

 ?>
