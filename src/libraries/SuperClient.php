<?php
// error_reporting(E_ALL); // Error engine
// ini_set('display_errors', TRUE); // Error display
// ini_set('log_errors', TRUE); // Error logging
//

require_once("SuperBAT.php");
if (!class_exists('Permission'))
  require_once "Permission.php";
if (!class_exists('SuperSourceBat'))
  require_once "SuperSourceBat.php";
if (!class_exists('SuperClass'))
  require_once "SuperClass.php";
if (!class_exists('LDAPSP'))
  require_once "LDAPSP.php";

const SP_ALL = 32767;
const SP_NONE = 0;
const SP_INFOS = 1; // 001
const SP_CREDITS = 2; //010
const SP_LOGOS = 4; //100
const SP_ERP = 8; //100
const SP_STRIPE = 16; //100

if (!function_exists("cmpmission")) {
  function cmpmission($a, $b) {
      return $a->created_on > $b->created_on ? -1 : 1;
  }
}


if (!class_exists("SuperClient")) {
  class SuperClient extends SuperClass {
      private $mode_demo = null;
      public  $owner_userid;
      public  $guest = 1;
      public  $crypted_id;
      public  $supercredits;
      public  $logos = [];
      public  $user_logo = "";
      public  $bats = [];
      public  $orders;
      public  $infos;
      public  $missions;
      private $permission;
      private $saved_mode;

      public $stripe;
      public $plan;

      public function __construct($ownerid, $mode = SP_ALL) {
        $this->owner_userid = $ownerid;
        $this->saved_mode = $mode;
        $this->fetch($ownerid, $mode);
      }

      private function fetch($ownerid, $mode) {
        $this->owner_userid = $ownerid;
        $this->guest = ($ownerid) ? 0 : 1 ;

        $this->crypted_id = $this->DanXEncrypt($ownerid);
        // $this->setKey($this->owner_userid.strrev((string)$this->owner_userid).$this->owner_userid);
        $this->autoAuth("sp");

        if (!intval($ownerid) || ($mode & SP_NONE))
          return;
      }

      public function loadERP() {

        $userID = $this->owner_userid;

        if ($root_user = $this->getRootUserInfos())
            $userID = $root_user->id;

        $this->erp = new LDAPSP($userID);
      }

      public function isRootUser() {
        $ret = $this->select("SELECT * FROM `1_element_element` WHERE source = 'root_user' AND  `target` = 'contact_user' AND `target_id` = $this->owner_userid");
        $this->root_userid =    ($ret) ? $ret->source_id : false ;
        return ($this->rootUser = (!$ret) ? true : false);
      }

      public function getRootUserInfos() {
        if ($this->isRootUser()) return null;

        $root_user_infos = $this->select(
          "SELECT 
            u.id, u.email, ui.company, ui.first_name, ui.last_name, 
            ui.address_1, ui.zip, ui.city, ui.virtuemart_country_id, ui.Siteweb
          FROM `digime_virtuemart_userinfos` AS ui
          INNER JOIN `digime_users` AS u ON u.id = ui.virtuemart_user_id
          INNER JOIN `1_element_element` AS ee ON u.id = ee.source_id
          WHERE ee.target = 'contact_user' AND ee.target_id = $this->owner_userid"
        );

        return $root_user_infos;
      }

      public function getParentUserContacts($parent_id) {

        $root_user_contacts = $this->selectAll(
          "SELECT ui.* FROM `digime_virtuemart_userinfos` AS ui
          INNER JOIN `1_element_element` AS ee ON ui.virtuemart_user_id = ee.target_id
          LEFT JOIN `1_user_image` as uimg ON uimg.user = ui.virtuemart_user_id
          WHERE ee.source = 'root_user' AND ee.source_id = $parent_id"
        );

        return $root_user_contacts;
      }

      public function getRootUserContacts() {
        if (!$this->isRootUser()) return null;

        $root_user_contacts = $this->selectAll(
          "SELECT ui.* FROM `digime_virtuemart_userinfos` AS ui
          INNER JOIN `1_element_element` AS ee ON ui.virtuemart_user_id = ee.target_id
          LEFT JOIN `1_user_image` as uimg ON uimg.user = ui.virtuemart_user_id
          WHERE ee.source = 'root_user' AND ee.source_id = $this->owner_userid"
        );

        return $root_user_contacts;
      }

      public function add_user_logo($file_path, $file_title) {
        $upsertinfos = array();
        //? $upsertinfos["nom de la colonne"]   =  [ "value" => "Nouvelle valeur a changer", "key" => /* 1 ou 0, definie si la colonne fait partie des conditions de mise a jour */];
        $upsertinfos["user"]             =  [ "value" => $this->owner_userid, "key" => 1 ];
        $upsertinfos["file_path"]        =  [ "value" => $file_path ];
        $upsertinfos["file_title"]       =  [ "value" => $file_title];
        $this->dxupsert("1_user_image", $upsertinfos);
      }

      public function datePlagePlus($myDate){
        $arrayDate = array();

        $arrayDate['D1'] = "1 day" ;
        $arrayDate['D3'] = "3 day" ;
        $arrayDate['D7'] = "7 day" ;
        $arrayDate['D14'] = "2 week" ;
        $arrayDate['M1'] = "1 month" ;
        $arrayDate['M3'] = "3 month" ;
        $arrayDate['M6'] = "6 month" ;
        $arrayDate['M12'] = "12 month" ;

        foreach ($arrayDate as $keyDate => $valueDate) {
          if($myDate == $keyDate)
            return $valueDate;
        }
      }
      
      public function datePlageMoins($myDate){
        $arrayDate = array();
        
        $arrayDate['D1'] = "-1 day" ;
        $arrayDate['D3'] = "-3 day" ;
        $arrayDate['D7'] = "-7 day" ;
        $arrayDate['D14'] = "-2 week" ;
        $arrayDate['M1'] = "-1 month" ;
        $arrayDate['M3'] = "-3 month" ;
        $arrayDate['M6'] = "-6 month" ;
        $arrayDate['M12'] = "-12 month" ;

        foreach ($arrayDate as $keyDate => $valueDate) {
          if($myDate == $keyDate)
            return $valueDate;
        }
      }

      public function getBats($page = 1, $PlageDate = null, $DateForPlage = null, $search = null, $filtreMission = null, $returntotal = false) {
        $bats = array();
        $ownerid = $this->owner_userid;

        if (in_array($filtreMission, [null, 'BAT']) && !$this->mode_demo) {
          if ($PlageDate && $DateForPlage) {
            $DateForPlage = str_replace('/', '-', $DateForPlage );
            $newDate = date("Y-m-d", strtotime($DateForPlage));
            $datePlagePlus = $this->datePlagePlus($PlageDate);
            $datePlageMoins = $this->datePlageMoins($PlageDate);
            $dateDiffPlus = date('Y-m-d', strtotime($datePlagePlus, strtotime($DateForPlage)));
            $dateDiffMoins = date('Y-m-d', strtotime($datePlageMoins, strtotime($DateForPlage)));
            $TotalDateDiff = "AND date_creation BETWEEN '".$dateDiffMoins."' AND '".$dateDiffPlus."'";
          } else
            $TotalDateDiff = '';
    
          if ($search) {
            $search = str_replace("\n"," ", $search);
            $words_search = explode(" ",strtolower($search));
            foreach ($words_search as $key => $word)
              $words_search[$key] = "CONCAT(nom, reference) LIKE ".$this->pdo->quote("%".$word."%");
    
            $searchUsersconcat = " AND " . implode(" AND ", $words_search);
          } else
            $searchUsersconcat = '';
    
          $limit = 24;
          $offset = ($page - 1) * $limit;
          if ($offset <= 0)
            $offset = 0;
          if ($returntotal)
            return count($this->selectAll("SELECT * FROM `1_gestion_bat` WHERE idclient = ".$ownerid." ".$TotalDateDiff." ".$searchUsersconcat." ORDER BY `date_creation` "));

          $batssql = $this->selectAll("SELECT * FROM `1_gestion_bat` WHERE idclient = ".$ownerid." ".$TotalDateDiff." ".$searchUsersconcat." ORDER BY `date_creation` DESC LIMIT ".$limit." OFFSET ".$offset);
          foreach ($batssql as $key => $bat) {
              $tmpBat = new stdClass();
              $tmpBat->created_on = DateTime::createFromFormat('Y-m-d H:i:s', $bat->date_creation);
              $tmpBat->superbat = new SuperBat($bat->rowid);
              if ($this->mode_demo) {
                $tmpBat->name = "";
                $tmpBat->product_name = "";
              }
              $tmpBat->superbat->initMaquetor();
              $tmpBat->idbatcrypted = $this->DanXEncrypt($bat->rowid);
              $tmpBat->cardtype = "bat";
              $bats[] = $tmpBat;
          }
        }

        return ($returntotal) ? $returntotal: $bats;
      }

      public function getPermissionByCode($code) {
        $id = $this->permission->getPermissionByCode($code);

        if($id)
          return $id->id;
        return false;
      }

      public function hasPermissionByUserId($code, $id) {
        if(empty($id)) {
          return false;
        }
        if (!isset($this->permission))
          $this->permission = new Permission();
        if ($perm = $this->permission->getPermissionByCode($code))
          return $this->permission->userHasPermission($id, $perm->id);

        return false;
      }

      public function getUserChild($login_child) {
        $child_id = $this->select(
            "SELECT id FROM `digime_users` WHERE `email` = :email", array('email' => $login_child)
        );

        return $child_id->id;
      }

      public function hasPermissionFor($code) {
        if (!isset($this->permission))
          $this->permission = new Permission();
        if ($perm = $this->permission->getPermissionByCode($code))
          return $this->permission->userHasPermission($this->owner_userid, $perm->id);

        return false;
    }
  }
}
?>