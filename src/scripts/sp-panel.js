function customEvent(types, opt, elmt, callback) {
    var e = $.Event(types.before, opt);
    if (e.firstTarget !== undefined)
        elmt.trigger(e);

    if (!e.isDefaultPrevented()) {
       if (typeof callback === 'function') callback();

       e = $.Event(types.after, opt);
       elmt.trigger(e);
    }

    return e;
}

function openPanel(panel, e) {
    if (typeof panel == 'string') panel = $(panel);
    if (e === undefined) e = {};

    if (!panel.hasClass('panel-opened')) {
        customEvent({
            before: 'panel:opened:before',
            after: 'panel:opened:after'
        }, {
            cancelable: true,
            firstTarget: e.currentTarget
        }, panel, function() {
            panel.css('opacity', 1);
            
            panel.animate({
                width: '100%'
            }, 200, function() {
                $(this).addClass('panel-opened');
            });
        });
    }
}

function closePanel(panel, e) {
    if (typeof panel == 'string') panel = $(panel);
    if (e === undefined) e = {};

    if (panel.hasClass('panel-opened')) {
        customEvent({
            before: 'panel:closed:before',
            after: 'panel:closed:after'
        }, {
            cancelable: true,
            firstTarget: e.currentTarget
        }, panel, function() {
            let panelOpener = panel.find('.panel-opener');
            let widthToGo = (panelOpener.length) ? `${panelOpener.width()}px` : '0%';

            panel.animate({
                width: widthToGo
            }, 200, function() {
                if (!panelOpener.length)
                    $(this).css('opacity', 0);

                $(this).removeClass('panel-opened');
            });
        });
    }
}

var documentHeight = $( document ).height();
var panelParentOffset = 0;

function initPanel(panel) {
    if (panel.length) {
        let panelParent = panel.parent();
        let top = ($(window).scrollTop() >= 0) ? $(window).scrollTop() : panelParent.offset().top;
        let panelHeight = Math.round($(window).height());
        documentHeight = $( document ).height() ;
        panelParentOffset = panelParent.offset().top;
    
        if (panelHeight > panelParent.outerHeight())
            panelHeight = panelParent.outerHeight();
    
        panel.css({
            height: `calc(${panelHeight}px - 120px)`,
            top: `${top}px`
        });
    }
}

$(function() {
    documentHeight = $( document ).height();
    $('.panel.panel-standalone').each(function() {
        initPanel($(this));
        openPanel($(this));
    });
    
    $(document).on('click', '[panel]', function(e) {
        let panelId = $(this).attr('panel');

        if (panelId != '') {
            closePanel('.panel.panel-opened:not(.panel-standalone)');

            if (panelId.indexOf('#') == 1)
                panelId = panelId.substring(1);

            let panel = $(`#${panelId}`);
            initPanel(panel);
            openPanel(panel, e);
        }
    });

    $(document).on('click', '.close-panel', function(e) {
        let panel = $(this).closest('.panel.panel-opened');

        if (panel.length) {
            closePanel(panel, e)
        }
    });
    
    $(window)
        .on('scroll', function() {
            let top = $(window).scrollTop();
            if (top > documentHeight - panelParentOffset - $('.panel').height() )
                top = documentHeight - panelParentOffset - $('.panel').height();
            $('.panel').css('top', `${top}px`);
        })
        .on('resize', function() {
            initPanel($('.panel'));
        })
    ;
})