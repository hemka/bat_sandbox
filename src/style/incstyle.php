<?php
header("Content-type: text/css");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-Requested-With");
require "scssphp-1.1.0/scss.inc.php";
use ScssPhp\ScssPhp\Compiler;
$scss = new Compiler();
$scss->setImportPaths("");

// $stfile = filter_var ( $_GET["st"], FILTER_SANITIZE_STRING);
$stfile =  preg_replace('/[^\w]/', '', $_GET["st"]);;

echo $scss->compile('@import "'.$stfile.'.scss"');
